<?php



/**

 * This is the model class for table "tb_eleitor".

 *

 * The followings are the available columns in table 'tb_eleitor':

 * @property string $cpf

 * @property string $nome

 * @property string $rg

 * @property string $codigo

 * @property string $data_nasc

 * @property string $sexo

 * @property string $nome_mae

 * @property string $wathsapp

 * @property string $telefone

 * @property string $email

 * @property string $coordenador

 * @property string $lider

 * @property string $data_visita

 * @property string $hora_visita

 * @property string $indicado_por

 * @property string $facebook

 * @property string $instagram

 * @property string $engajamento

 * @property string $endereco

 * @property string $numero

 * @property integer $estado

 * @property integer $municipio

 * @property integer $bairro

 * @property string $cep

 * @property string $zona

 * @property string $secao

 * @property string $numero_pesssoas_familia

 * @property string $lider_comunitario

 * @property string $tipo_residencia

 * @property string $possui_veiculo
 * 
 * @property string $qtdeDependetes
 * 
 * @property string $nome_dep_1
 * @property string $faixa_dep_1

 * @property string $nome_dep_2
 * @property string $faixa_dep_2

 * @property string $nome_dep_3
 * @property string $faixa_dep_3

 * @property string $nome_dep_4
 * @property string $faixa_dep_4
 * 
 * @property string $nome_pai
   
 * @property string $endereco_votacao
 * @property string $bairro_votacao
 * @property string $municipio_votacao
 * @property string $twitter
 * @property string $telefone_recado
 * @property string $cel_recado

 * @property string $tipo_veiculo

 * @property string $titulo_eleitor

 * @property string $situacao_titulo

 * @property integer $estimativa_votos

 * @property string $qualbairro

 */

class Eleitor extends CActiveRecord

{

	public $codigos_escolhidos;

	/**

	 * @return string the associated database table name

	 */

	public function tableName()

	{

		return 'tb_eleitor';

	}



	/**

	 * @return array validation rules for model attributes.

	 */

	public function rules()

	{

		// NOTE: you should only define rules for those attributes that

		// will receive user inputs.

		return array(

			array('cpf, nome, rg, codigo, data_nasc, nome_mae, coordenador, lider, endereco, numero, estado, municipio, bairro, cep, secao, titulo_eleitor, situacao_titulo, zona_moradia, local_votacao', 'required'),

			array('estimativa_votos', 'numerical', 'integerOnly'=>true),

			array('cpf, wathsapp, telefone, coordenador, lider', 'length', 'max'=>14),

			array('cartao_sus', 'length', 'max'=>19),

			array('cpf', 'verificaDuplicidade', 'length', 'max'=>14),

			array('nome, nome_mae, endereco_votacao', 'length', 'max'=>200),

			array('estado', 'length', 'max'=>2),

			array('municipio', 'length', 'max'=>30),

			array('telefone_recado, cel_recado', 'length', 'max'=>30),
			

			array('bairro_votacao, municipio_votacao,  ', 'length', 'max'=>50),


			array('bairro, profissao, local_votacao', 'length', 'max'=>45),

			array('rg', 'length', 'max'=>15),

			array('zona_moradia', 'length', 'max'=>2),

			array('faixa_dep_1, faixa_dep_2, faixa_dep_3, faixa_dep_4, sexo, lider_comunitario, tipo_residencia, qtdeDependetes, possui_veiculo, situacao_titulo, falecido, estado_civil', 'length', 'max'=>1),

			array( 'nome_dep_1, nome_dep_2, nome_dep_3, nome_dep_4, email, endereco, bairroconjunto, domicilio_eleitoral', 'length', 'max'=>100),

			array('hora_visita', 'length', 'max'=>5),

			array('indicado_por, nome_pai', 'length', 'max'=>120),

			array('facebook, instagram, twitter', 'length', 'max'=>250),

			array('numero', 'length', 'max'=>8),

			array('cep', 'length', 'max'=>9),

			array('secao, zona, numero_pesssoas_familia, engajamento', 'length', 'max'=>4),

			array('titulo_eleitor', 'length', 'max'=>30),

			array('tipo_veiculo', 'verificaVeiculo', 'length', 'max'=>30),

			array('observacao', 'length', 'max'=>256),

			array('qualbairro', 'verificaQualbairro', 'length', 'max'=>30),

			array('tipo_veiculo, lider, data_visita', 'safe'),

			// The following rule is used by search().

			// @todo Please remove those attributes that should not be searched.

			array('cpf, nome, rg, codigo, data_nasc, sexo, nome_mae, wathsapp, telefone, email, coordenador, lider, data_visita, hora_visita, indicado_por, facebook, instagram, engajamento, endereco, numero, estado, municipio, bairro, cep, zona, secao, numero_pesssoas_familia, lider_comunitario, tipo_residencia, possui_veiculo,  qtdeDependetes, tipo_veiculo, titulo_eleitor, situacao_titulo, estimativa_votos, zona_moradia, profissao, local_votacao, cartao_sus, observacao,bairroconjunto, estado_civil', 'safe', 'on'=>'search'),

		);

	}



	public function verificaVeiculo(){

		if (Yii::app()->controller->action->id === 'novo') {

			if($this->possui_veiculo == "S"){

				if($this->tipo_veiculo == null || $this->tipo_veiculo == "")

					$this->addError($this->cpf, 'Obrigatório informar o tipo do veículo!');

			}

		}

	}

	

		public function verificaQualbairro(){



		if (Yii::app()->controller->action->id === 'novo') {



			if($this->bairro == "Outro"){



				if($this->qualbairro == null || $this->qualbairro == "")



					$this->addError($this->cpf, 'Obrigatório informar o qual bairro!');



			}



		}



	}



	public function verificaDuplicidade(){



		if (Yii::app()->controller->action->id === 'novo') {

			$eleitor = Eleitor::model()->findByPk($this->cpf);



			if($eleitor->lider != null)

				$lider = Usuario::model()->findByPk($eleitor->lider);



			if(!empty($eleitor) || $eleitor != null){

				$this->addError($this->cpf, 'CPF já cadastrado na base de dados pelo líder :' . @$lider->nome);

			}

		}



	}



	/**

	 * @return array relational rules.

	 */

	public function relations()

	{

		// NOTE: you may need to adjust the relation name and the related

		// class name for the relations automatically generated below.

		return array(

		);

	}



	/**

	 * @return array customized attribute labels (name=>label)

	 */

	public function attributeLabels()

	{

		return array(

			'cpf' 					=> 'Cpf',

			'nome' 					=> 'Nome',

			'rg' 					=> 'Rg',

			'codigo' 				=> 'Codigo',

			'data_nasc' 			=> 'Data Nasc',

			'sexo' 					=> 'Sexo',

			'estado_civil'          => 'Estado Civil',

			'nome_mae' 				=> 'Nome Mae',

			'wathsapp' 				=> 'Wathsapp',

			'telefone' 				=> 'Telefone',

			'email' 				=> 'Email',

			'coordenador' 			=> 'Coordenador',

			'lider' 				=> 'Lider',

			'data_visita' 			=> 'Data Visita',

			'hora_visita' 			=> 'Hora Visita',

			'indicado_por' 			=> 'Indicado Por',

			'facebook' 				=> 'Facebook',

			'instagram' 			=> 'Instagram',

			'engajamento' 			=> 'Engajamento',

			'endereco' 				=> 'Endereco',

			'numero' 				=> 'Numero',

			'estado' 				=> 'Estado',

			'municipio' 			=> 'Municipio',

			'bairro' 				=> 'Bairro',

			'bairroconjunto'        => 'Bairro/Conjunto',

			'qualbairro'        	=> 'Qual bairro?',

			'cep' 						=> 'Cep',

			'zona' 						=> 'Zona',

			'secao' 					=> 'Secao',

			'numero_pesssoas_familia' 	=> 'Numero Pesssoas Familia',

			'lider_comunitario' 		=> 'Lider Comunitario',

			'tipo_residencia' 			=> 'Tipo Residencia',

			'possui_veiculo' 			=> 'Possui Veiculo',

			'tipo_veiculo' 				=> 'Tipo Veiculo',

			'titulo_eleitor' 			=> 'Titulo Eleitor',

			'situacao_titulo' 			=> 'Situacao Titulo',

			'estimativa_votos' 			=> 'Estimativa Votos',

			'zona_moradia'				=> 'Zona de moradia',

			'profissao'					=> 'Profissão',

			'local_votacao'				=> 'Local de votação',

			'cartao_sus'				=> 'Cartão SUS',

			'domicilio_eleitoral'		=> 'Domicilio Eleitoral',

			'falecido'					=> 'Falecido ?',

			'observacao'				=> 'Observação',

			'qtdeDependetes'            => 'Quantidade de Dependentes',

			'nome_dep_1'                  => 'Nome Dependente 1',

			'faixa_dep_1'                 => 'Faixa Etária Dependente 1',

			'nome_dep_2'                  => 'Nome Dependente 2',

			'faixa_dep_2'                 => 'Faixa Etária Dependente 2',

			'nome_dep_3'                  => 'Nome Dependente 3',

			'faixa_dep_3'                 => 'Faixa Etária Dependente 3',

			'nome_dep_4'                  => 'Nome Dependente 4',

			'faixa_dep_4'                 => 'Faixa Etária Dependente 4',
			
		    'nome_pai'                   => 'Nome do Pai',

            'endereco_votacao'           => 'Endereço Votação',

            'bairro_votacao'             => 'Bairro Votação',

            'municipio_votacao'          => 'Município Votação',

            'twitter'                    => 'Twitter',

            'telefone_recado'            => 'Telefone para Recados',

            'cel_recado'                 => 'Celular para Recados'

		);

	}





	public function listEleitor() {



		$id  			= Yii::app()->user->id;



		try

		{

			if(Yii::app()->user->perfil == "C"){

				//como coordenador, tem a visao de todos os eleitores de sua coordenacao

				$sql  = Yii::app()->db->createCommand("SELECT * FROM vw_eleitor WHERE coordenador_cpf = '".$id."' order by data_cadastro desc")->queryAll();

			} elseif(Yii::app()->user->perfil == "L") {

				//como lider, tema a visao de todos os eleitores de sua lideranca

				$sql  = Yii::app()->db->createCommand("SELECT * FROM vw_eleitor WHERE lider_cpf = '".$id."' order by data_cadastro desc")->queryAll();

			} else {

				//como administrador, tem a visao geral de todos os eleitores cadastrados

				$sql  = Yii::app()->db->createCommand("SELECT * FROM vw_eleitor order by data_cadastro desc")->queryAll();

			}

		

			$num_rows 			  = count($sql);

			$dataProvider = new CArrayDataProvider($sql, array(

				'id' => 'cpf',

				'pagination' => false,

			));



			return array($dataProvider->getData(), $num_rows);



		}catch(Exception $e){

			return array();

		}    



	}





	public function listData() {



		$id  			= Yii::app()->user->id;



		if(Yii::app()->user->perfil == "C"){

			//como coordenador, tem a visao de todos os eleitores de sua coordenacao

			$sql  = Yii::app()->db->createCommand("SELECT * FROM vw_eleitor WHERE coordenador_cpf = '".$id."' order by data_cadastro desc")->queryAll();

		} elseif(Yii::app()->user->perfil == "L") {

			//como lider, tema a visao de todos os eleitores de sua lideranca

			$sql  = Yii::app()->db->createCommand("SELECT * FROM vw_eleitor WHERE lider_cpf = '".$id."' order by data_cadastro desc")->queryAll();

		} else {

			//como administrador, tem a visao geral de todos os eleitores cadastrados

			$sql  = Yii::app()->db->createCommand("SELECT * FROM vw_eleitor order by data_cadastro desc")->queryAll();

		}



		$num_rows = count($sql);



		$obj = new stdClass();

		$obj->draw = 1;

		$obj->recordsTotal = $num_rows;

		$obj->recordsFiltered = $num_rows;

		$obj->data = array();



		foreach ($sql as $key => $value) {

				$obj->data[] = list(

									$coordenador

									,$lider

									,$cpf

									,$nome

									,$codigo

									,$profissao

									,$data_nasc

									,$wathsapp 

									,$bairro

									,$sexo 

									,$rg 

									,$nome_mae 

									,$telefone 

									,$email 

									,$lider_cpf 

									,$data_visita 

									,$hora_visita 

									,$indicado_por 

									,$facebook 

									,$instagram 

									,$engajamento 

									,$endereco 

									,$numero 

									,$estado 

									,$municipio 

									,$cep 

									,$zona 

									,$secao 

									,$numero_pesssoas_familia 

									,$lider_comunitario 

									,$tipo_residencia 

									,$possui_veiculo 

									,$tipo_veiculo 

									,$titulo_eleitor 

									,$situacao_titulo 

									,$estimativa_votos 

									,$data_cadastro 

									,$zona_moradia 

									,$local_votacao 

									,$coordenador_cpf 

									,$qtdeDependetes

									,$nome_dep_1

									,$faixa_dep_1
									
									,$nome_dep_2

									,$faixa_dep_2

									,$nome_dep_3

									,$faixa_dep_3
									
									,$nome_dep_4

									,$faixa_dep_4

									,$nome_pai

									,$endereco_votacao
						
									,$bairro_votacao
						
									,$municipio_votacao
						
									,$twitter
						
									,$telefone_recado
						
									,$cel_recado

				) = array_values($value);

		}



		return CJSON::encode($obj);

}



	public function afterFind(){



		$this->data_nasc   		  = Yii::app()->dateFormatter->format('dd/MM/yyyy', $this->data_nasc);

		$this->data_visita 		  = Yii::app()->dateFormatter->format('dd/MM/yyyy', $this->data_visita);

		$this->codigos_escolhidos = $this->codigo;

		$this->codigo			  = '';		



		return true;

	}



	protected function beforeSave()

	{

		//$this->data_nasc   = isset($this->data_nasc) ? Yii::app()->dateFormatter->format('yyyy-MM-dd', $this->data_nasc) : null;

		//$this->data_visita = isset($this->data_visita) ? Yii::app()->dateFormatter->format('yyyy-MM-dd', $this->data_visita) : null;



		$this->data_nasc   = isset($this->data_nasc) ? date('Y-m-d', CDateTimeParser::parse($this->data_nasc, 'dd/MM/yyyy')) : null;

		$this->data_visita = isset($this->data_visita) && $this->data_visita != null ? date('Y-m-d', CDateTimeParser::parse($this->data_visita, 'dd/MM/yyyy')) : null;

        $this->bairroconjunto = strtoupper($this->bairroconjunto);

		$this->bairro = strtoupper($this->bairro);

		$this->qualbairro = strtoupper($this->qualbairro);

		$this->sexo = strtoupper($this->sexo);

		$this->estado_civil = strtoupper($this->estado_civil);

		$this->nome = strtoupper($this->nome);

		$this->nome_mae = strtoupper($this->nome_mae);

		$this->email = strtoupper($this->email);

		$this->coordenador = strtoupper($this->coordenador);

		$this->lider = strtoupper($this->lider);

        $this->indicado_por = strtoupper($this->indicado_por);

		$this->facebook = strtoupper($this->facebook);

		$this->instagram = strtoupper($this->instagram);

		$this->engajamento = strtoupper($this->engajamento);

		$this->engajamento = strtoupper($this->engajamento);

		$this->endereco = strtoupper($this->endereco);

		$this->estado = strtoupper($this->estado);

		$this->municipio = strtoupper($this->municipio);

		$this->zona = strtoupper($this->zona);

		$this->secao = strtoupper($this->secao);

		$this->zona_moradia = strtoupper($this->zona_moradia);

		$this->profissao = strtoupper($this->profissao);

		$this->local_votacao = strtoupper($this->local_votacao);

		$this->domicilio_eleitoral = strtoupper($this->domicilio_eleitoral);

		$this->observacao = strtoupper($this->observacao);

		$this->nome_dep_1 = strtoupper($this->nome_dep_1);

		$this->nome_dep_2 = strtoupper($this->nome_dep_2);

		$this->nome_dep_3 = strtoupper($this->nome_dep_3);

		$this->nome_dep_4 = strtoupper($this->nome_dep_4);


		return true;

	}



	public function search()

	{

		// @todo Please modify the following code to remove attributes that should not be searched.



		$criteria=new CDbCriteria;



		$criteria->compare('cpf',$this->cpf,true);

		$criteria->compare('nome',$this->nome,true);

		$criteria->compare('rg',$this->rg,true);

		$criteria->compare('codigo',$this->codigo,true);

		$criteria->compare('data_nasc',$this->data_nasc,true);

		$criteria->compare('sexo',$this->sexo,true);

		$criteria->compare('nome_mae',$this->nome_mae,true);

		$criteria->compare('wathsapp',$this->wathsapp,true);

		$criteria->compare('telefone',$this->telefone,true);

		$criteria->compare('email',$this->email,true);

		$criteria->compare('coordenador',$this->coordenador,true);

		$criteria->compare('lider',$this->lider,true);

		$criteria->compare('data_visita',$this->data_visita,true);

		$criteria->compare('hora_visita',$this->hora_visita,true);

		$criteria->compare('indicado_por',$this->indicado_por,true);

		$criteria->compare('facebook',$this->facebook,true);

		$criteria->compare('instagram',$this->instagram,true);

		$criteria->compare('engajamento',$this->engajamento,true);

		$criteria->compare('endereco',$this->endereco,true);

		$criteria->compare('numero',$this->numero,true);

		$criteria->compare('estado',$this->estado);

		$criteria->compare('municipio',$this->municipio);

		$criteria->compare('bairro',$this->bairro);

		$criteria->compare('cep',$this->cep,true);

		$criteria->compare('zona',$this->zona,true);

		$criteria->compare('secao',$this->secao,true);

		$criteria->compare('numero_pesssoas_familia',$this->numero_pesssoas_familia,true);

		$criteria->compare('lider_comunitario',$this->lider_comunitario,true);

		$criteria->compare('tipo_residencia',$this->tipo_residencia,true);

		$criteria->compare('possui_veiculo',$this->possui_veiculo,true);

		$criteria->compare('tipo_veiculo',$this->tipo_veiculo,true);

		$criteria->compare('titulo_eleitor',$this->titulo_eleitor,true);

		$criteria->compare('situacao_titulo',$this->situacao_titulo,true);

		$criteria->compare('estimativa_votos',$this->estimativa_votos);

		$criteria->compare('qtdeDependetes',$this->qtdeDependetes);

		$criteria->compare('nome_dep_1',$this->nome_dep_1);

		$criteria->compare('faixa_dep_1',$this->faixa_dep_1);

		$criteria->compare('nome_dep_2',$this->nome_dep_2);

		$criteria->compare('faixa_dep_2',$this->faixa_dep_2);
		
		$criteria->compare('nome_dep_3',$this->nome_dep_3);

		$criteria->compare('faixa_dep_3',$this->faixa_dep_3);

		$criteria->compare('nome_dep_4',$this->nome_dep_4);

		$criteria->compare('faixa_dep_4',$this->faixa_dep_4);

		$criteria->compare('nome_pai',$this->nome_pai);
		$criteria->compare('endereco_votacao',$this->endereco_votacao);
		$criteria->compare('bairro_votacao',$this->bairro_votacao);
		$criteria->compare('municipio_votacao',$this->municipio_votacao);
		$criteria->compare('twitter',$this->twitter);
		$criteria->compare('telefone_recado',$this->telefone_recado);
		$criteria->compare('cel_recado',$this->cel_recado);

		return new CActiveDataProvider($this, array(

			'criteria'=>$criteria,

		));

	}



	/**

	 * Returns the static model of the specified AR class.

	 * Please note that you should have this exact method in all your CActiveRecord descendants!

	 * @param string $className active record class name.

	 * @return Eleitor the static model class

	 */

	public static function model($className=__CLASS__)

	{

		return parent::model($className);

	}

}

