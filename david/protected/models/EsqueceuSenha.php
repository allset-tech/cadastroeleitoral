<?php


class EsqueceuSenha extends CFormModel
{
    public $usuario_login;
    public $usuario_cpf;
    public $usuario_nova_senha;
    
    public function attributeLabels() {
        return array(
            'usuario_login'=>'Nome de Usuário',
            'usuario_cpf'=>'CPF',
            'usuario_nova_senha'=>'Nova senha'
        );
    }
    
    public function rules() {
        return array(
            arraY('usuario_login, usuario_cpf, usuario_nova_senha', 'required', 'message'=>'O campo {attribute} é obrigatório!'),
            array('usuario_cpf', 'length', 'min'=>11),
        );
    }
}
?>
