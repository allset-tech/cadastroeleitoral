<?php

/**
 * This is the model class for table "tb_bairro_zona".
 *
 * The followings are the available columns in table 'tb_bairro_zona':
 * @property string $bairro
 * @property string $zona
 * @property string $zona_abrev
 */
class BairroZona extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_bairro_zona';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('bairro', 'required'),
			array('bairro', 'length', 'max'=>150),
			array('zona', 'length', 'max'=>45),
			array('zona_abrev', 'length', 'max'=>2),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('bairro, zona, zona_abrev', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'bairro' => 'Bairro',
			'zona' => 'Zona',
			'zona_abrev' => 'Zona Abrev',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('bairro',$this->bairro,true);
		$criteria->compare('zona',$this->zona,true);
		$criteria->compare('zona_abrev',$this->zona_abrev,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BairroZona the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
