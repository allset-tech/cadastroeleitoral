<?php

/**
 * This is the model class for table "tb_zona_eleitoral".
 *
 * The followings are the available columns in table 'tb_zona_eleitoral':
 * @property integer $zona
 * @property string $num_uf
 * @property string $endereco
 * @property string $cep
 * @property string $bairro
 * @property string $municipio
 * @property string $sigla_uf
 */
class Zona extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_zona_eleitoral';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('num_uf, endereco, bairro, municipio, sigla_uf', 'required'),
			array('num_uf', 'length', 'max'=>15),
			array('endereco, bairro', 'length', 'max'=>100),
			array('cep', 'length', 'max'=>9),
			array('municipio', 'length', 'max'=>45),
			array('sigla_uf', 'length', 'max'=>2),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('zona, num_uf, endereco, cep, bairro, municipio, sigla_uf', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'zona' => 'Zona',
			'num_uf' => 'Num Uf',
			'endereco' => 'Endereco',
			'cep' => 'Cep',
			'bairro' => 'Bairro',
			'municipio' => 'Municipio',
			'sigla_uf' => 'Sigla Uf',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('zona',$this->zona);
		$criteria->compare('num_uf',$this->num_uf,true);
		$criteria->compare('endereco',$this->endereco,true);
		$criteria->compare('cep',$this->cep,true);
		$criteria->compare('bairro',$this->bairro,true);
		$criteria->compare('municipio',$this->municipio,true);
		$criteria->compare('sigla_uf',$this->sigla_uf,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Zona the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
