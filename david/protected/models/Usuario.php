<?php

/**
 * This is the model class for table "tb_usuario".
 *
 * The followings are the available columns in table 'tb_usuario':
 * @property string $cpf
 * @property string $nome
 * @property string $data_nasc
 * @property string $sexo
 * @property string $telefone
 * @property string $endereco
 * @property string $numero
 * @property integer $estado
 * @property integer $municipio
 * @property integer $bairro
 * @property string $cep
 * @property string $email
 * @property string $perfil
 * @property string $status
 * @property string $senha
 */
class Usuario extends Model
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_usuario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cpf, nome, data_nasc, sexo, telefone, endereco, numero, estado, municipio, bairro, cep, email, perfil, status, senha', 'required'),
			//array('estado, municipio, bairro', 'numerical', 'integerOnly'=>true),
			array('cpf, telefone', 'length', 'max'=>14),
			array('cpf', 'verificaDuplicidade', 'length', 'max'=>14),
			array('nome', 'length', 'max'=>200),
			array('sexo, perfil, status', 'length', 'max'=>1),
			array('endereco, email', 'length', 'max'=>100),
			array('numero', 'length', 'max'=>8),
			array('cep', 'length', 'max'=>9),
			array('bairro', 'length', 'max'=>45),
			array('municipio', 'length', 'max'=>30),
			array('estado', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('cpf, nome, data_nasc, sexo, telefone, endereco, numero, estado, municipio, bairro, cep, email, perfil, status, senha', 'safe', 'on'=>'search'),
		);
	}


	public function listUsuario() {

		$id  = Yii::app()->user->id;

		try
		{
			if(Yii::app()->user->perfil == "C"){
				//coordenador
				$sql  = Yii::app()->db->createCommand("SELECT * FROM vw_usuario WHERE coordenador = '".$id."' order by nome ASC")->queryAll();
			} else {
				//administrador
				$sql  = Yii::app()->db->createCommand("SELECT * FROM vw_usuario order by nome ASC")->queryAll();
			}		

			$num_rows 	  = count($sql);

			$dataProvider = new CArrayDataProvider($sql, array(
				'id' => 'cpf',
				'pagination' => false,
			));

			return array($dataProvider->getData(), $num_rows);
		}catch(Exception $e){
			return array();
		}

	}


	public function verificaDuplicidade(){

		if (Yii::app()->controller->action->id === 'novo') {
			$usuario = Usuario::model()->findByPk($this->cpf);

			if(!empty($usuario) || $usuario != null){
				$this->addError($this->cpf, 'CPF já cadastrado na base de dados!');
			}
		}

	}

	public function afterFind(){

		$this->data_nasc = Yii::app()->dateFormatter->format('dd/MM/yyyy', $this->data_nasc);

		return true;
	}

	protected function beforeSave()
	{
		 
		if (Yii::app()->controller->action->id == "novo") {
            $this->senha 	 =  md5($this->senha);
        } else {

            $model = Usuario::model()->findByPk($this->cpf);
			if ($model->senha != $this->senha) {
                $pass = md5($this->senha);
                $this->senha = $pass;
            }
        }

		$this->data_nasc = isset($this->data_nasc) ? date('Y-m-d', CDateTimeParser::parse($this->data_nasc, 'dd/MM/yyyy')) : null;

		return true;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cpf' 		=> 'Cpf',
			'nome' 		=> 'Nome',
			'data_nasc' => 'Data Nasc',
			'sexo' 		=> 'Sexo',
			'telefone' 	=> 'Telefone',
			'endereco' 	=> 'Endereco',
			'numero' 	=> 'Numero',
			'estado' 	=> 'Estado',
			'municipio' => 'Municipio',
			'bairro' 	=> 'Bairro',
			'cep' 		=> 'Cep',
			'email' 	=> 'Email',
			'perfil' 	=> 'Perfil',
			'status' 	=> 'Status',
			'senha' 	=> 'Senha',
			'coordenador' => 'Coordenador',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('cpf',$this->cpf,true);
		$criteria->compare('nome',$this->nome,true);
		$criteria->compare('data_nasc',$this->data_nasc,true);
		$criteria->compare('sexo',$this->sexo,true);
		$criteria->compare('telefone',$this->telefone,true);
		$criteria->compare('endereco',$this->endereco,true);
		$criteria->compare('numero',$this->numero,true);
		$criteria->compare('estado',$this->estado);
		$criteria->compare('municipio',$this->municipio);
		$criteria->compare('bairro',$this->bairro);
		$criteria->compare('cep',$this->cep,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('perfil',$this->perfil,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('senha',$this->senha,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Usuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
