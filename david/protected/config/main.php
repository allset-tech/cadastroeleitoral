<?php



Yii::setPathOfAlias('widgets', dirname(__FILE__) . '/../widgets');



return array(

    'basePath'   => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',

    'name'       => 'Cadastro',

    'theme'      => 'cadeleitoral',

    'preload'    => array('log'),

    'language'   => 'pt_br',

    'import'     => array(

        'application.models.*',

        'application.components.*',

        'application.extensions.CJuiDateTimePicker.*',

        'application.extensions.tcpdf.*',

        'ext.select2.*',

        'ext.dropzone.EDropzone',

    ),

    'modules'    => array(

  ///     'gii'       => array(

   //         'class'     => 'system.gii.GiiModule',

    //        'password'  => '123456',

    //        'ipFilters' => array('*'),

    //    ),

    ),

    'components' => array(

        'user'         => array(

            'loginUrl'       => array('acesso/index'),

            //'returnUrl'      => array('site/index'),

            //'class'          => 'application.components.AuthWebUser',

            'allowAutoLogin' => true,

        ),

        'phpThumb'     => array(

            'class' => 'ext.EPhpThumb.EPhpThumb',

        ),

        'urlManager'   => array(

            'urlFormat'      => 'path',

            'showScriptName' => false,

            'rules'=>array(

    'news/<controller:\w+>/<id:\d+>'=>'news/<controller>/view',

    'news/<controller:\w+>/<action:\w+>/<id:\d+>'=>'news/<controller>/<action>',

    'news/<controller:\w+>/<action:\w+>'=>'news/<controller>/<action>',

 ),

        ),

        

        'db'           => array(

            //dominio triedro

//            'connectionString'   => 'mysql:host=localhost;dbname=triedr14_cadastro_eleitoral',

//             'username'           => 'triedr14_joao',

//             'password'           => 'a1b2c3d421@',



/*           desenvolvimento local

            'connectionString'   => 'mysql:host=localhost;dbname=cadastro_eleitoral',

            'username'           => 'root',

            'password'           => 'Tr13dr0@aminSYS21', */



            //dominio all tech

            'connectionString'   => 'mysql:host=localhost;dbname=cadastroeleitoraDavi',

            'username'           => 'admin',

            'password'           => 'cadastro2Eleitora',


            'emulatePrepare'     => true,

            'charset'            => 'utf8',

            'enableProfiling'    => true,

            'enableParamLogging' => true,

        ),

        'errorHandler' => array(

            'errorAction' => 'site/error',

        ),

       //'IReport'      => array('class' => 'IReport'),

    ),

    'params'     => array(

        'adminEmail' => 'rhullyanna@gmail.com',

    ),

);

