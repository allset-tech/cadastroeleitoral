<?php

require_once('config/tcpdf_config_alt.php');
require_once('tcpdf.php');

// Extend the TCPDF class to create custom Header and Footer
class Recibo extends TCPDF
{

    //Page header
    public function Header()
    {
    }

    // Page footer
    public function Footer()
    {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('opensanslighti', '', 8);
        $this->Cell(0, 0, 'GFiN - Gerenciador Financeiro' . ' -  Página ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, 10, 'R', 0, '', 0, false, 'M', 'M');
    }



    public function MultiRow($left, $right)
    {
        $page_start = $this->getPage();
        $y_start    = $this->GetY();

        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(2, 136, 204);
        $this->SetFont('','B');
        // write the left cell
        $this->MultiCell(50, 7, $left, 0, 'L', 1, 2, '', '', true, 0);
        //$this->MultiCell(75, 7, $left, 0, 'R', 1, 2, '', '', true, 0);

        $page_end_1 = $this->getPage();
        $y_end_1    = $this->GetY();

        $this->setPage($page_start);

        $this->SetTextColor(0);
        $this->SetFont('');
        // write the right cell
        $this->MultiCell(0, 0, $right, 0, 'L', 0, 1, $this->GetX(), $y_start, true, 0);

        $page_end_2 = $this->getPage();
        $y_end_2    = $this->GetY();

        // set the new row position by case
        if (max($page_end_1, $page_end_2) == $page_start) {
            $ynew = max($y_end_1, $y_end_2);
        } elseif ($page_end_1 == $page_end_2) {
            $ynew = max($y_end_1, $y_end_2);
        } elseif ($page_end_1 > $page_end_2) {
            $ynew = $y_end_1;
        } else {
            $ynew = $y_end_2;
        }

        $this->setPage(max($page_end_1, $page_end_2));
        $this->SetXY($this->GetX(), $ynew);
    }


    public function template($valor, $responsavel, $valorPorExtensso, $observacao)
    {

      $str = '<table border="1" width="100%">
                <tr>
                    <td>
                        <table width="100%" cellspacing="10">
                            <tr><td colspan="3"></td></tr>
                            <tr>
                                <td style="vertical-align:middle" width="33%" align="center">
                                1a via
                                </td>
                                <td style="vertical-align:middle" width="33%" align="center">
                                    <h2>RECIBO</h2>
                                </td>
                                <td style="vertical-align:middle" width="33%" align="center">
                                    VALOR R$ '.$valor.'
                                </td>
                            </tr>
                            <tr>
                            <td colspan="3" style="vertical-align:middle" height="10">
                                RECEBEMOS DE
                            </td>
                            </tr>
                            <tr><td colspan="3" style="vertical-align:middle" height="40" bgcolor="#E4E0E0">
                                '.$responsavel.'
                            </td></tr>
                            <tr>
                            <td colspan="3" style="vertical-align:middle" height="10">
                                A IMPORTÂNCIA DE
                            </td></tr>
                            <tr>
                            <td colspan="3" style="vertical-align:middle" height="40" bgcolor="#E4E0E0">
                                '.$valorPorExtensso.'
                            </td></tr>
                            <tr>
                            <td colspan="3" style="vertical-align:middle" height="10">
                                REFERENTE A
                            </td></tr>
                            <tr>
                            <td colspan="3" style="vertical-align:middle" height="40" bgcolor="#E4E0E0">
                                '.$observacao.'
                            </td></tr>
                            <tr>
                                <td colspan="3" height="10">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" height="auto">
                                        <tr>
                                            <td>
                                              Data:______/_______/__________
                                            </td>
                                        </tr>
                                        <tr height="20">
                                            <td>
                                                ________________________________
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td></td>
                                <td>
                                <table width="100%" height="auto">
                                    <tr>
                                        <td>
                                          Data:______/_______/__________
                                        </td>
                                    </tr>
                                    <tr height="20">
                                        <td>
                                            ________________________________
                                        </td>
                                    </tr>

                                </table>
                              </td>
                            </tr>
                            <tr><td colspan="3"></td></tr>
                        </table>

                    </td>
                </tr>
            </table>
            <table border="1" width="100%">
                      <tr>
                          <td>
                              <table width="100%" cellspacing="10">
                                  <tr><td colspan="3"></td></tr>
                                  <tr>
                                      <td style="vertical-align:middle" width="33%" align="center">
                                      2a via
                                      </td>
                                      <td style="vertical-align:middle" width="33%" align="center">
                                          <h2>RECIBO</h2>
                                      </td>
                                      <td style="vertical-align:middle" width="33%" align="center">
                                          VALOR R$ '.$valor.'
                                      </td>
                                  </tr>
                                  <tr>
                                  <td colspan="3" style="vertical-align:middle" height="10">
                                      RECEBEMOS DE
                                  </td>
                                  </tr>
                                  <tr><td colspan="3" style="vertical-align:middle" height="40" bgcolor="#E4E0E0">
                                      '.$responsavel.'
                                  </td></tr>
                                  <tr>
                                  <td colspan="3" style="vertical-align:middle" height="10">
                                      A IMPORTÂNCIA DE
                                  </td></tr>
                                  <tr>
                                  <td colspan="3" style="vertical-align:middle" height="40" bgcolor="#E4E0E0">
                                      '.$valorPorExtensso.'
                                  </td></tr>
                                  <tr>
                                  <td colspan="3" style="vertical-align:middle" height="10">
                                      REFERENTE A
                                  </td></tr>
                                  <tr>
                                  <td colspan="3" style="vertical-align:middle" height="40" bgcolor="#E4E0E0">
                                      '.$observacao.'
                                  </td></tr>
                                  <tr>
                                      <td colspan="3" height="10">
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <table width="100%" height="auto">
                                              <tr>
                                                  <td>
                                                    Data:______/_______/__________
                                                  </td>
                                              </tr>
                                              <tr height="20">
                                                  <td>
                                                      ________________________________
                                                  </td>
                                              </tr>
                                          </table>
                                      </td>
                                      <td></td>
                                      <td>
                                      <table width="100%" height="auto">
                                          <tr>
                                              <td>
                                                Data:______/_______/__________
                                              </td>
                                          </tr>
                                          <tr height="20">
                                              <td>
                                                  ________________________________
                                              </td>
                                          </tr>

                                      </table>
                                    </td>
                                  </tr>
                                  <tr><td colspan="3"></td></tr>
                              </table>

                          </td>
                      </tr>
                  </table>';

                  return $str;

    }

}

//============================================================+
// END OF FILE
//============================================================+
