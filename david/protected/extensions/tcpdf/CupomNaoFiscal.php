<?php

require_once('tcpdf/config/tcpdf_config_cupom.php');
require_once('tcpdf.php');

// Extend the TCPDF class to create custom Header and Footer

class CupomNaoFiscal extends TCPDF
{
    var $htmlHeader;
    var $htmlFooter;
    public $isLastPage = false;

    public function setHtmlHeader($htmlHeader) {
        $this->htmlHeader = $htmlHeader;
    }

    public function setHtmlFooter($htmlFooter) {
        $this->htmlFooter = $htmlFooter;
    }

      //Page header
    public function Header() {
        if($this->page == 1){
            $this->writeHTMLCell(
                $w = 0, $h = 0, $x = '', $y = '',
                $this->htmlHeader, $border = 0, $ln = 1, $fill = 0,
                $reseth = true, $align = 'top', $autopadding = true);
        }
    }



    public function lastPage($resetmargins=false) {
        $this->setPage($this->getNumPages(), $resetmargins);
        $this->isLastPage = true;
    }    

    public function Footer()
    {
        if($this->isLastPage) {  
            $this->SetY(-7);  
           // $this->writeHTML($this->htmlFooter);
           $this->SetFont('dejavusansmono', '', 6.5);
            $this->writeHTMLCell(
                $w = 0, $h = 0, $x = '15', $y = '',
                $this->htmlFooter, $border = 0, $ln = 1, $fill = 0,
                $reseth = true, $align = 'center', $autopadding = true);
        }

    }
}


//============================================================+

// END OF FILE

//============================================================+

