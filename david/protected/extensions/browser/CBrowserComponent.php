<?php
	//Yii::import('application.extensions.browser.Browser');
        Yii::import('application.extensions.browser.Plataforma');
	class CBrowserComponent extends CApplicationComponent
	{
		private $_myBrowser;
                private $_isMobile;
                
		public function init() {}
		public function __construct()
		{
			$this->_myBrowser = new Plataforma();
		}

		/**
		* Call a Browser function
		*
		* @return string
		*/
		public function __call($method, $params)
		{
			if (is_object($this->_myBrowser) && get_class($this->_myBrowser)==='Plataforma') return call_user_func_array(array($this->_myBrowser, $method), $params);
			else throw new CException(Yii::t('Browser', 'Can not call a method of a non existent object'));
		}
	}
?>