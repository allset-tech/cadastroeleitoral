<?php

class EleitoreslogController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/inside';

    public function init()
    {
        
        try{
            if(Yii::app()->user->perfil == "L")
                throw new Exception("Usuário não tem permissão de acesso!!");
            if(Yii::app()->user->perfil == "C")
                throw new Exception("Usuário não tem permissão de acesso!!");
        }catch(Exception $e){
            $this->redirect(array('acesso/login'));
        }

        parent::init();

        Yii::import('ext.PHPExcel.YPHPExcel');

        Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . "/js/plugins/jquery.mask.js", CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . "/js/usuario.js", CClientScript::POS_BEGIN);

    }      

    public function actionIndex()
    {
        $model       = new EleitorLog();

        if(empty($model->listEleitorlog()))
            $this->redirect(array('acesso/login'));


        $this->render('index', array(
            'model'         => json_encode($model->listEleitorlog()[0]),
            'num_rows'      => $model->listEleitorlog()[1],
        ));
    }

    public function actionListaEleitorlog()
    {
      if (Yii::app()->request->isAjaxRequest)
      {
              $produtos = new Eleitorlog();
              echo $produtos->listEleitorlog();
      }
    }

    public function actionNovo()
    {

        $model       = new Eleitorlog;

        try
        {
            $meu_perfil  = @Yii::app()->user->perfil;            
        }catch(Exception $e){
            $this->redirect(array('acesso/login')); 
        } 
        
        
        $model->unsetAttributes();


        if (isset($_POST['Eleitorlog'])) {

            $errors             = array();
            $transaction        = Yii::app()->db->beginTransaction();            
            $model->attributes  = $_POST['Eleitorlog'];

            $model->validate();

            //salvando informacoes do funcionario
            if(!$model->save()){
                $errors[] = $model->getErrors();
            }

            //salvando informacoes do usuario

            if (empty($errors)) {
                $transaction->commit();
                Yii::app()->user->setFlash('success', "Registro criado com sucesso!");
                //$this->redirect(array('visualizar', 'id' => $model->cpf));
            } else {
               // print_r($errors);
               // exit;
                Yii::app()->user->setFlash('error', "Falha ao criar registro:");
                $transaction->rollback();
            }

        }

        $this->render('novo', array(
            'model'             => $model,
        ));
    }



    public function implode_all( $glue, $arr ){
        if( is_array( $arr ) ){
      
          foreach( $arr as $key => &$value ){
      
            if( @is_array( $value ) ){
              $arr[ $key ] = $this->implode_all( $glue, $value );
            }
          }
      
          return implode( $glue, $arr );
        }
      
        // Not array
        return $arr;
      }


    public function actionVisualizar($id)
    {

        if(isset($id)){

            $model          = Eleitorlog::model()->findByPk($id);

            try
            {
              $meu_perfil  = @Yii::app()->user->perfil;
            }catch(Exception $e){
                $this->redirect(array('acesso/login')); 
            } 


            $this->render('visualizar', array(
                'model'             => $model,   
            ));

        }
        
    }

    public function actionSalvar($id)
    {
        $model          = $this->loadModel($id);

        try
        {
            $meu_perfil  = @Yii::app()->user->perfil;
        }catch(Exception $e){
            $this->redirect(array('acesso/login')); 
        } 


        if (isset($_POST['Eleitorlog'])) {
            $model->attributes = $_POST['Eleitorlog'];

            $errors            = array();
            $transaction       = Yii::app()->db->beginTransaction();            
            $model->attributes = $_POST['Eleitorlog'];


            $model->validate();

            //salvando informacoes do funcionario
            if(!$model->save()){
                $errors[] = $model->getErrors();
            }

            //salvando informacoes do usuario
            if (empty($errors)) {
                $transaction->commit();
                Yii::app()->user->setFlash('success', "Registro atualizado com sucesso!");
            } else {
                Yii::app()->user->setFlash('error', "Falha ao atualizar registro:");
                $transaction->rollback();
            }

        }

        $this->render('visualizar', array(
            'model'             => $model,
        ));
    }

    public function actionFiltrar()
    {

        try
        {

            $id  			= Yii::app()->user->id;
            $filtro_sessao	= $_POST;
            $filtro_sql 	= array();

            
            if(isset($filtro_sessao['cpf']) && $filtro_sessao['cpf'] != " ")
                $filtro_sql[] = "cpf like '" . $filtro_sessao['cpf'] . "%'";                
           

            $filtro_str = '';

            if(count($filtro_sql) > 1)
                $filtro_str = implode(' and ', $filtro_sql);
            else if(count($filtro_sql) == 1)
                $filtro_str = ' ' . $filtro_sql[0];
            else if(count($filtro_sql) == 0)
                $filtro_str = ' true ';


            if(Yii::app()->user->perfil == "C"){
				//coordenador
				$sql  = Yii::app()->db->createCommand("SELECT * FROM vw_usuario WHERE coordenador = '".$id."' and {$filtro_str} order by data_cadastro desc")->queryAll();
			} else {
				//administrador
				$sql  = Yii::app()->db->createCommand("SELECT * FROM vw_usuario WHERE {$filtro_str} order by data_cadastro desc")->queryAll();
			}
        
            $num_rows  = count($sql);
            
            $dataProvider = new CArrayDataProvider($sql, array(
                'id'         => 'id',
                'pagination' => false,
            ));


            echo json_encode(array(
                        'status'    => 'Ok',
                        'detalhes'  => $dataProvider->getData(),
                        'total'     => $num_rows,
                        ));

        }catch(Exception $e)
        {
            echo json_encode(array(
                    'status' => 'Error',
                    'detalhes' => $e->getMessage()
                ));
        }
    }


    public function actionExportar()
    {
        if (Yii::app()->request->isAjaxRequest) {

                try
                {

                    $id  			= Yii::app()->user->id;
                    $filtro_sessao	= $_POST;
                    $filtro_sql 	= array();
                        
                    if(isset($filtro_sessao['cpf']) && $filtro_sessao['cpf'] != " ")
                        $filtro_sql[] = "cpf like '" . $filtro_sessao['cpf'] . "%'";                       
 
                    $filtro_str = '';

                    if(count($filtro_sql) > 1)
                        $filtro_str = implode(' and ', $filtro_sql);
                    else if(count($filtro_sql) == 1)
                        $filtro_str = ' ' . $filtro_sql[0];
                    else if(count($filtro_sql) == 0)
                        $filtro_str = ' true ';

                       //echo $filtro_str;exit;

                    if(@Yii::app()->user->perfil == "C"){
                        //coordenador
                        $sql  = Yii::app()->db->createCommand("SELECT * FROM tb_usuario WHERE coordenador = '".$id."' and {$filtro_str} order by data_cadastro desc")->queryAll();
                    } else {
                        //administrador
                        $sql  = Yii::app()->db->createCommand("SELECT * FROM tb_usuario WHERE {$filtro_str} order by data_cadastro desc")->queryAll();
                    }

                }catch(Exception $e){
                    $this->redirect(array('acesso/login')); 
                } 

                try {

                    $file_name = str_replace("-", "", str_replace(".", "", $id));

                    if (file_exists(Yii::app()->basePath . '/data/templates/'.$file_name.'_Eleitorlog.xlsx'))
                        unlink(Yii::app()->basePath . '/data/templates/'.$file_name.'_Eleitorlog.xlsx');

                    YPHPExcel::createPHPExcel();

                    $inputFileName = Yii::app()->basePath . '/data/templates/tmpl.xlsx';

                    $objReader   = PHPExcel_IOFactory::createReader('Excel2007');
                    $objPHPExcel = $objReader->load($inputFileName);
                    $sheetData   = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);


                    $objPHPExcel->setActiveSheetIndex(0);
                    $objPHPExcel->getActiveSheet()->setTitle("Lista de usuarios");

                    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('20'); //cpf
                    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('20'); //administrador
                    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('40'); //data exclusão


                    $cont = 1;

                   $objPHPExcel->getActiveSheet()
                            ->setCellValue('A' . $cont, "CPF")
                            ->setCellValue('B' . $cont, "ADMINISTRADOR")
                            ->setCellValue('C' . $cont, "DATA EXCLUSÃO");


                    $cont++;

                    if(!empty($sql)){
                        foreach ($sql as $row) {

                            $objPHPExcel->getActiveSheet()
                                    ->setCellValue('A' . $cont, $row['cpf'])
                                    ->setCellValue('Q' . $cont, $row['administrador'])
                                    ->setCellValue('R' . $cont, $row['data_exclusao']);

                            $cont++;
                        }
                    }

                    //ini_set('display_errors', 1);
                    //ini_set('display_startup_errors', 1);
                    //error_reporting(E_ALL);
                    

                    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                    ob_end_clean();
                    header('Content-type: application/vnd.ms-excel');
                    header('Content-Disposition: attachment; filename="'.$file_name.'_Eleitorlog.xlsx"');
                    //header('Cache-Control: max-age=0');
                    //$objWriter->save('php://output');
                    $objWriter->save(Yii::app()->basePath . '/data/templates/'.$file_name.'_Eleitorlog.xlsx');

                    $url_file = "";
                    
                    if(file_exists(Yii::app()->basePath . '/data/templates/'.str_replace("-", "", str_replace(".", "", Yii::app()->user->id)).'_Eleitorlog.xlsx')){
                        $url_file = Yii::app()->assetManager->publish(Yii::app()->basePath . '/data/templates/'.str_replace("-", "", str_replace(".", "", Yii::app()->user->id)).'_Eleitorlog.xlsx');
                    }

                    echo CJSON::encode(array(
                        'resultado' => "OK",
                        'file'      => $url_file,
                    ));
                } catch (Exception $ex) {
                    echo CJSON::encode(array(
                        'resultado' => "Erro",
                        'detalhes'  => $ex->getMessage(),
                    ));
                }
            //}
        }
    }

    public function loadModel($id)
    {
        $model = Eleitorlog::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'A página solicitada não existe.');
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'usuario-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }


}
