<?php

class UsuariosController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/inside';
    public $sexo   = array('M' => 'Masculino', 'F' => 'Feminino', 'O' => 'Outros');
    public $perfil = array('A' => 'Administrador', 'C' => 'Coordenador', 'L' => 'Líder');
    public $status = array('A' => 'Ativo', 'I' => 'Inativo');

    public $perfil_f = array('Administrador' => 'Administrador', 'Coordenador' => 'Coordenador', 'Líder' => 'Líder');
    public $status_f = array('Ativo' => 'Ativo', 'Inativo' => 'Inativo');

    public function init()
    {
        
        try{
            if(Yii::app()->user->perfil == "L")
                throw new Exception("Usuário não tem permissão de acesso!!");
        }catch(Exception $e){
            $this->redirect(array('acesso/login'));
        }

        parent::init();

        Yii::import('ext.PHPExcel.YPHPExcel');

        Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . "/js/plugins/jquery.mask.js", CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . "/js/usuario.js", CClientScript::POS_BEGIN);

    }      

    public function actionIndex()
    {
        $status      = $this->status_f;
        $perfil      = $this->perfil_f;
        $model       = new Usuario();

        if(empty($model->listUsuario()))
            $this->redirect(array('acesso/login'));


        $this->render('index', array(
            'model'         => json_encode($model->listUsuario()[0]),
            'status'        => $status,
            'perfil'        => $perfil,
            'num_rows'      => $model->listUsuario()[1],
        ));
    }

    public function actionListaUsuarios()
    {
      if (Yii::app()->request->isAjaxRequest)
      {
              $produtos = new Usuario();
              echo $produtos->listUsuario();
      }
    }

    public function actionNovo()
    {

        $model       = new Usuario;
        $sexo        = $this->sexo;
        $status      = $this->status; 
        $coordenador = CHtml::listData(Usuario::model()->findAll("perfil = 'C'"), 'cpf', 'nome');

        try
        {
            $meu_perfil  = @Yii::app()->user->perfil;            
        }catch(Exception $e){
            $this->redirect(array('acesso/login')); 
        } 
        
        $perfil      = $this->perfil;

        if($meu_perfil == "C"){
            unset($perfil['A']);
            unset($perfil['C']);
        }        
        
        $model->unsetAttributes();


        if (isset($_POST['Usuario'])) {

            $errors             = array();
            $transaction        = Yii::app()->db->beginTransaction();            
            $model->attributes  = $_POST['Usuario'];

            if($meu_perfil == "A"){
                $model->coordenador = $_POST['Usuario']['coordenador'];
            } else {
                $model->coordenador = $meu_perfil;
            }               
            

            $model->status      = 'A';

            //echo "<pre>";
            //print_r($model->attributes);
            //exit;
            $model->validate();

            //salvando informacoes do funcionario
            if(!$model->save()){
                $errors[] = $model->getErrors();
            }

            //salvando informacoes do usuario

            if (empty($errors)) {
                $transaction->commit();
                Yii::app()->user->setFlash('success', "Registro criado com sucesso!");
                //$this->redirect(array('visualizar', 'id' => $model->cpf));
            } else {
               // print_r($errors);
               // exit;
                Yii::app()->user->setFlash('error', "Falha ao criar registro:");
                $transaction->rollback();
            }

        }

        $this->render('novo', array(
            'model'             => $model,
            'sexo'              => $sexo,
            'perfil'            => $perfil,
            'status'            => $status,   
            'coordenador'        => $coordenador, 
        ));
    }

    public function actionTransferir($id)
    {

        if(isset($id)){

            $id                     = base64_decode($id);
            $model                  = Usuario::model()->findByPk($id);

            $sql                    = Yii::app()->db->createCommand("SELECT * FROM tb_eleitor where lider = '".$model->cpf."'")->queryAll();
            $num_eleitores          = count($sql);

            $lideres_coordenador    = CHtml::listData(Usuario::model()->findAll("coordenador = '".Yii::app()->user->id."'"), 'cpf', 'nome');

            unset($lideres_coordenador[$model->cpf]);               

            $this->render('transferir', array(
                'model'                 => $model,
                'num_eleitores'         => $num_eleitores,
                'lideres_coordenador'   => $lideres_coordenador,
            ));

        }
        
    }

    public function implode_all( $glue, $arr ){
        if( is_array( $arr ) ){
      
          foreach( $arr as $key => &$value ){
      
            if( @is_array( $value ) ){
              $arr[ $key ] = $this->implode_all( $glue, $value );
            }
          }
      
          return implode( $glue, $arr );
        }
      
        // Not array
        return $arr;
      }

    public function actionFinalizar()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $errors         = array();
            $origem         = $_POST['origem'];
            $destino        = $_POST['destino'];

            if(isset($_POST)){
                try
                {
                    Yii::app()->db->createCommand('update tb_eleitor set lider = :lider_destino where lider = :lider_origem')
                    ->bindValue(':lider_destino',$destino)
                    ->bindValue(':lider_origem', $origem)
                    ->execute();

                }catch(Exception $e){
                    $errors[]   = 'Erro ao finalizar transferência das informações. ' . $e->getMessage(); 
                }

                if (empty($errors)) {
                     echo CJSON::encode(array(
                        'status' => "Ok",
                        'detalhes'  => '',
                    ));

                } else {
                   echo CJSON::encode(array(
                    'status' => "Error",
                    'detalhes'  => $this->implode_all(',', $errors),
                ));
                    
                }
            }           

            
        }
    }

    public function actionVisualizar($id)
    {

        if(isset($id)){

            $model          = Usuario::model()->findByPk($id);
            $sexo           = $this->sexo;
            $status         = $this->status;
            $coordenador    = CHtml::listData(Usuario::model()->findAll("perfil = 'C'"), 'cpf', 'nome');

            try
            {
              $meu_perfil  = @Yii::app()->user->perfil;
            }catch(Exception $e){
                $this->redirect(array('acesso/login')); 
            } 
            
            $perfil      = $this->perfil;

            if($meu_perfil == "C"){
                unset($perfil['A']);
                unset($perfil['C']);
            }           


            $this->render('visualizar', array(
                'model'             => $model,
                'sexo'              => $sexo,
                'perfil'            => $perfil,
                'status'            => $status,
                'coordenador'        => $coordenador,     
            ));

        }
        
    }

    public function actionSalvar($id)
    {
        $model          = $this->loadModel($id);
        $sexo           = $this->sexo;
        $status         = $this->status;
        $coordenador    = CHtml::listData(Usuario::model()->findAll("perfil = 'C'"), 'cpf', 'nome');

        try
        {
            $meu_perfil  = @Yii::app()->user->perfil;
        }catch(Exception $e){
            $this->redirect(array('acesso/login')); 
        } 

        $perfil      = $this->perfil;

        if($meu_perfil == "C"){
            unset($perfil['A']);
            unset($perfil['C']);
        }

        if (isset($_POST['Usuario'])) {
            $model->attributes = $_POST['Usuario'];

            $errors            = array();
            $transaction       = Yii::app()->db->beginTransaction();            
            $model->attributes = $_POST['Usuario'];

            if($meu_perfil == "A"){
                $model->coordenador = $_POST['Usuario']['coordenador'];
            } else {
                $model->coordenador = $meu_perfil;
            }

            $model->validate();

            //salvando informacoes do funcionario
            if(!$model->save()){
                $errors[] = $model->getErrors();
            }

            //salvando informacoes do usuario
            if (empty($errors)) {
                $transaction->commit();
                Yii::app()->user->setFlash('success', "Registro atualizado com sucesso!");
            } else {
                Yii::app()->user->setFlash('error', "Falha ao atualizar registro:");
                $transaction->rollback();
            }

        }

        $this->render('visualizar', array(
            'model'             => $model,
            'sexo'              => $sexo,
            'perfil'            => $perfil,
            'status'            => $status,    
            'coordenador'        => $coordenador,
        ));
    }

    public function actionFiltrar()
    {

        try
        {

            $id  			= Yii::app()->user->id;
            $filtro_sessao	= $_POST;
            $filtro_sql 	= array();

            if(isset($filtro_sessao['nome']) && $filtro_sessao['nome'] != " ")
                $filtro_sql[] = "nome like '" . $filtro_sessao['nome'] . "%'";
                
            if(isset($filtro_sessao['cpf']) && $filtro_sessao['cpf'] != " ")
                $filtro_sql[] = "cpf like '" . $filtro_sessao['cpf'] . "%'";                
           
            if(isset($filtro_sessao['perfil']) && $filtro_sessao['perfil'] != " ")
                $filtro_sql[] = "perfil = '" . $filtro_sessao['perfil'] ."'";
                
            if(isset($filtro_sessao['status']) && $filtro_sessao['status'] != " ")
                $filtro_sql[] = "status = '" . $filtro_sessao['status'] ."'";

            $filtro_str = '';

            if(count($filtro_sql) > 1)
                $filtro_str = implode(' and ', $filtro_sql);
            else if(count($filtro_sql) == 1)
                $filtro_str = ' ' . $filtro_sql[0];
            else if(count($filtro_sql) == 0)
                $filtro_str = ' true ';


            if(Yii::app()->user->perfil == "C"){
				//coordenador
				$sql  = Yii::app()->db->createCommand("SELECT * FROM vw_usuario WHERE coordenador = '".$id."' and {$filtro_str} order by data_cadastro desc")->queryAll();
			} else {
				//administrador
				$sql  = Yii::app()->db->createCommand("SELECT * FROM vw_usuario WHERE {$filtro_str} order by data_cadastro desc")->queryAll();
			}
        
            $num_rows  = count($sql);
            
            $dataProvider = new CArrayDataProvider($sql, array(
                'id'         => 'cpf',
                'pagination' => false,
            ));


            echo json_encode(array(
                        'status'    => 'Ok',
                        'detalhes'  => $dataProvider->getData(),
                        'total'     => $num_rows,
                        ));

        }catch(Exception $e)
        {
            echo json_encode(array(
                    'status' => 'Error',
                    'detalhes' => $e->getMessage()
                ));
        }
    }


    public function actionExportar()
    {
        if (Yii::app()->request->isAjaxRequest) {

                try
                {

                    $id  			= Yii::app()->user->id;
                    $filtro_sessao	= $_POST;
                    $filtro_sql 	= array();

                    if(isset($filtro_sessao['nome']) && $filtro_sessao['nome'] != " ")
                        $filtro_sql[] = "nome like '" . $filtro_sessao['nome'] . "%'";
                        
                    if(isset($filtro_sessao['cpf']) && $filtro_sessao['cpf'] != " ")
                        $filtro_sql[] = "cpf like '" . $filtro_sessao['cpf'] . "%'";                       
 
                    
                    if(isset($filtro_sessao['perfil']) && $filtro_sessao['perfil'] != " ")
                        $filtro_sql[] = "perfil = '" . $filtro_sessao['perfil'] ."'";
                    
                    if(isset($filtro_sessao['status']) && $filtro_sessao['status'] != " ")
                        $filtro_sql[] = "status = '" . $filtro_sessao['status'] ."'";                       


                    $filtro_str = '';

                    if(count($filtro_sql) > 1)
                        $filtro_str = implode(' and ', $filtro_sql);
                    else if(count($filtro_sql) == 1)
                        $filtro_str = ' ' . $filtro_sql[0];
                    else if(count($filtro_sql) == 0)
                        $filtro_str = ' true ';

                       //echo $filtro_str;exit;

                    if(@Yii::app()->user->perfil == "C"){
                        //coordenador
                        $sql  = Yii::app()->db->createCommand("SELECT * FROM tb_usuario WHERE coordenador = '".$id."' and {$filtro_str} order by data_cadastro desc")->queryAll();
                    } else {
                        //administrador
                        $sql  = Yii::app()->db->createCommand("SELECT * FROM tb_usuario WHERE {$filtro_str} order by data_cadastro desc")->queryAll();
                    }

                }catch(Exception $e){
                    $this->redirect(array('acesso/login')); 
                } 

                try {

                    $file_name = str_replace("-", "", str_replace(".", "", $id));

                    if (file_exists(Yii::app()->basePath . '/data/templates/'.$file_name.'_usuarios.xlsx'))
                        unlink(Yii::app()->basePath . '/data/templates/'.$file_name.'_usuarios.xlsx');

                    YPHPExcel::createPHPExcel();

                    $inputFileName = Yii::app()->basePath . '/data/templates/tmpl.xlsx';

                    $objReader   = PHPExcel_IOFactory::createReader('Excel2007');
                    $objPHPExcel = $objReader->load($inputFileName);
                    $sheetData   = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);


                    $objPHPExcel->setActiveSheetIndex(0);
                    $objPHPExcel->getActiveSheet()->setTitle("Lista de usuarios");

                    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('12'); //cpf
                    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('12'); //nome
                    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('12'); //data_nasc
                    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('25'); //sexo
                    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('25'); //telefone
                    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('12'); //endereco
                    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth('20'); //numero
                    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth('20'); //estado
                    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth('20'); //municipio
                    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth('20'); //bairro
                    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth('20'); //cep
                    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth('20'); //email
                    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth('20'); //perfil
                    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth('20'); //status
                    $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth('20'); //senha
                    $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth('20'); //coordenador
                    $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth('20'); //data cadastro


                    $cont = 1;

                   $objPHPExcel->getActiveSheet()
                            ->setCellValue('A' . $cont, "CPF")
                            ->setCellValue('B' . $cont, "NOME")
                            ->setCellValue('C' . $cont, "DATA NASC")
                            ->setCellValue('D' . $cont, "SEXO")
                            ->setCellValue('E' . $cont, "TELEFONE")
                            ->setCellValue('F' . $cont, "ENDEREÇO")
                            ->setCellValue('G' . $cont, "NÚMERO")
                            ->setCellValue('H' . $cont, "ESTADO")
                            ->setCellValue('I' . $cont, "MUNICÍPIO")
                            ->setCellValue('J' . $cont, "BAIRRO")
                            ->setCellValue('L' . $cont, "CEP")
                            ->setCellValue('M' . $cont, "EMAIL")
                            ->setCellValue('N' . $cont, "PERFIL")
                            ->setCellValue('O' . $cont, "STATUS")
                            ->setCellValue('P' . $cont, "SENHA")
                            ->setCellValue('Q' . $cont, "COORDENADOR")
                            ->setCellValue('R' . $cont, "DATA CADASTRO");


                    $cont++;

                    if(!empty($sql)){
                        foreach ($sql as $row) {

                            $objPHPExcel->getActiveSheet()
                                    ->setCellValue('A' . $cont, $row['cpf'])
                                    ->setCellValue('B' . $cont, $row['nome'])
                                    ->setCellValue('C' . $cont, $row['data_nasc'])
                                    ->setCellValue('D' . $cont, $row['sexo'])
                                    ->setCellValue('E' . $cont, $row['telefone'])
                                    ->setCellValue('F' . $cont, $row['endereco'])
                                    ->setCellValue('G' . $cont, $row['numero'])
                                    ->setCellValue('H' . $cont, $row['estado'])
                                    ->setCellValue('I' . $cont, $row['municipio'])
                                    ->setCellValue('J' . $cont, $row['bairro'])
                                    ->setCellValue('L' . $cont, $row['cep'])
                                    ->setCellValue('M' . $cont, $row['email'])
                                    ->setCellValue('N' . $cont, $row['perfil'])
                                    ->setCellValue('O' . $cont, $row['status'])
                                    ->setCellValue('P' . $cont, $row['senha'])
                                    ->setCellValue('Q' . $cont, $row['coordenador'])
                                    ->setCellValue('R' . $cont, $row['data_cadastro']);

                            $cont++;
                        }
                    }

                    //ini_set('display_errors', 1);
                    //ini_set('display_startup_errors', 1);
                    //error_reporting(E_ALL);
                    

                    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                    ob_end_clean();
                    header('Content-type: application/vnd.ms-excel');
                    header('Content-Disposition: attachment; filename="'.$file_name.'_usuarios.xlsx"');
                    //header('Cache-Control: max-age=0');
                    //$objWriter->save('php://output');
                    $objWriter->save(Yii::app()->basePath . '/data/templates/'.$file_name.'_usuarios.xlsx');

                    $url_file = "";
                    
                    if(file_exists(Yii::app()->basePath . '/data/templates/'.str_replace("-", "", str_replace(".", "", Yii::app()->user->id)).'_usuarios.xlsx')){
                        $url_file = Yii::app()->assetManager->publish(Yii::app()->basePath . '/data/templates/'.str_replace("-", "", str_replace(".", "", Yii::app()->user->id)).'_usuarios.xlsx');
                    }

                    echo CJSON::encode(array(
                        'resultado' => "OK",
                        'file'      => $url_file,
                    ));
                } catch (Exception $ex) {
                    echo CJSON::encode(array(
                        'resultado' => "Erro",
                        'detalhes'  => $ex->getMessage(),
                    ));
                }
            //}
        }
    }

    public function loadModel($id)
    {
        $model = Usuario::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'A página solicitada não existe.');
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'usuario-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }


}
