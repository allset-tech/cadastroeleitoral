<?php

class SiteController extends Controller {

    public $layout = '//layouts/main';

    public function init() {
        parent::init();

       // Yii::app()->user->setState('titulo', 'Sistema');

        Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/plugins/fullcalendar/fullcalendar.css');

        Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . "/js/plugins/mask/jquery.mask.js", CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . "/js/plugins/maskmoney/jquery.maskmoney.js", CClientScript::POS_BEGIN);

        Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . "/js/plugins/fullcalendar/moment.min.js", CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . "/js/plugins/fullcalendar/fullcalendar.js", CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . "/js/plugins/fullcalendar/pt-br.js", CClientScript::POS_END);

        //Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . "/js/plugins/hightcharts/highcharts.js", CClientScript::POS_END);
        //Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . "/js/plugins/hightcharts/modules/exporting.js", CClientScript::POS_END);
    
    }


    public function actions() {
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }   
    
    
    public function actionIndex(){

        if (Yii::app()->user->isGuest)
            $this->redirect(array("acesso/login"));
              
        $this->render('index', array());

    }
 
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionDownload($nome) {
        $caminho = Yii::app()->basePath . "\\data\\templates\\" . $nome;
        Yii::app()->getRequest()->sendFile($nome, @file_get_contents($caminho));
    }

    public function actionArquivoModelo($nome) {
        $caminho = Yii::app()->basePath . "\\data\\templates\\" . $nome;
        Yii::app()->getRequest()->sendFile($nome, @file_get_contents($caminho));
    }

}

?>
