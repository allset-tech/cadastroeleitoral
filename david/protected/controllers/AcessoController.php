<?php


class AcessoController extends Controller
{

    public $layout = 'login';

    public function init()
    {
        $baseUrl = Yii::app()->theme->baseUrl;

        $cs = Yii::app()->getClientScript();
        $cs->registerCssFile($baseUrl . '/css/login.css');
        //$cs->registerScriptFile($baseUrl . '/js/plugins/jquery-ui/jquery-ui.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/login.js', CClientScript::POS_END);

        Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . "/js/plugins/maskedinput/jquery.maskedinput.min.js", CClientScript::POS_END);
    }

    public function actionIndex()
    {
        $model              = new LoginForm();
        $modelEsqueceuSenha = new EsqueceuSenha();
        $this->render('index', array('model' => $model, 'modelEsqueceuSenha' => $modelEsqueceuSenha));
    }

    public function actionLogin()
    {
        
        if (!Yii::app()->user->isGuest) {
            //$this->redirect(Yii::app()->homeUrl);
            $this->redirect(array('acesso/login'));
        }         

        $model              = new LoginForm;
        $modelEsqueceuSenha = new EsqueceuSenha();

        if (isset($_POST['LoginForm'])) {
            if (strlen($_POST['LoginForm']['password']) > 0 && $_POST['LoginForm']['password'] != " " &&
                    strlen($_POST['LoginForm']['username']) > 0 && $_POST['LoginForm']['password'] != " ") {                     
                       
                $model->attributes = $_POST['LoginForm'];


                if (/*$model->validate() && */$model->login()) {

                   // ARMAZENA DADOS NA SESSION, QUE SERÃO UTILIZADOS NA SESSÃO DO USUÁRIO
                    //$this->armazenaDadosNaSessao();
                    $this->redirect(Yii::app()->user->returnUrl);                 
                    
                } else {

                    $authErros    = array();
                    //if ($model->_identity->errorCode === UserIdentity::ERROR_PERMISSION)
                     //   $authErros [] = Yii::app()->user->setFlash('error', 'Este usuário não tem permissão!');

                    if ($model->_identity->errorCode === UserIdentity::ERROR_USERNAME_INVALID ||
                            $model->_identity->errorCode === UserIdentity::ERROR_PASSWORD_INVALID)
                        $authErros [] = Yii::app()->user->setFlash('error', 'Usuário ou Senha inválidos!');

                    if ($model->_identity->errorCode === UserIdentity::ERROR_INACTIVE)
                        $authErros [] = Yii::app()->user->setFlash('block', 'Este usuário está desativado!');

                    if ($model->_identity->errorCode === UserIdentity::ERROR_PERFIL_INVALIDO)
                        $authErros [] = Yii::app()->user->setFlash('error', 'Usuário com perfil não identificado!');

                    echo $authErros[0];
                }
            } else
                Yii::app()->user->setFlash('error', 'Usuário ou Senha inválidos!');
        }
        $this->render('index', array('model' => $model, 'modelEsqueceuSenha' => $modelEsqueceuSenha));
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    private function removePontosDoCPF($cpf)
    {
        $cpf = str_replace(".", "", $cpf);
        $cpf = str_replace("-", "", $cpf);
        return $cpf;
    }

    /*public function actionEnviarNovaSenha()
    {

        $modelEsqueceuSenha = new EsqueceuSenha();


        if (isset($_POST['EsqueceuSenha'])) {
            if (Yii::app()->request->isAjaxRequest) {
                $modelEsqueceuSenha->attributes = $_POST['EsqueceuSenha'];

                if (!strlen(trim($modelEsqueceuSenha->usuario_nova_senha)) > 0 && !strlen(trim($modelEsqueceuSenha->usuario_cpf)) > 0) {
                    echo CJSON::encode(array(
                        'status'    => 'failure',
                        'resultado' => 'Por favor preencha todos os campos.',
                    ));
                    exit;
                } else {
                    $funcionario = ViewFuncionario::model()->findByAttributes(array('func_cpf' => $this->removePontosDoCPF($modelEsqueceuSenha->usuario_cpf)));
                    $usuario     = Usuario::model()->findByAttributes(array('usua_func_id' => $funcionario->func_id));
                }

                if ($usuario === null || $funcionario->func_cpf !== $this->removePontosDoCPF($modelEsqueceuSenha->usuario_cpf)) {
                    echo CJSON::encode(array(
                        'status'    => 'failure',
                        'resultado' => 'Nome de usuário ou CPF inválidos.',
                    ));
                } else if ($usuario !== null && $funcionario->func_cpf === $this->removePontosDoCPF($modelEsqueceuSenha->usuario_cpf)) {
                    $usuario->usua_pswd = $modelEsqueceuSenha->usuario_nova_senha;
                    $usuario->save();

                    echo CJSON::encode(array(
                        'status'    => 'success',
                        'id'        => $usuario->usua_id,
                        'resultado' => 'Sua senha foi alterada com sucesso. Você pode efetuar o login novamente'
                    ));
                    exit;
                }
            }
        }
    }*/

}
