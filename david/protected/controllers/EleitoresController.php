<?php


class EleitoresController extends Controller

{


    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */

    public $layout = '//layouts/inside';

    public $sexo = array('M' => 'Masculino', 'F' => 'Feminino', 'O' => 'Outros', '' => '');

    public $estado_civil = array('' => '', 'S' => 'Solteiro(a)', 'C' => 'Casado(a)', 'D' => 'Divorciado(a)', 'V' => 'Viúvo', 'U' => 'União Estável');

    public $perfil = array('A' => 'Administrador', 'C' => 'Coordenador', 'L' => 'Líder', '' => '');

    public $status = array('A' => 'Ativo', 'I' => 'Inativo', '' => '');

    public $engajamento = array('POS' => 'Positivo', 'NEG' => 'Negativo', 'NEU' => 'Neutro', '' => '');

    public $lider_comunitario = array('S' => 'Sim', 'N' => 'Nao', '' => '');

    public $tipo_residencia = array('P' => 'Própria', 'A' => 'Alugada', 'C' => 'Cedida', '' => '',  'N' => '');

    public $possui_veiculo = array('S' => 'Sim', 'N' => 'Nao', '' => '');

    public $qtdeDependetes = array('0' => 0, '1' => 1, '2' => 2, '3' => 3, '4' => 4, '' => '');

    public $faixaEtaria = array('0' => 'Menor de 16 anos', '1' => 'Acima de 60 anos', '' => '');

    public $situacao_titulo = array('R' => 'Regular', 'P' => 'Pendente', '' => '');

    public $zona_moradia = array('CO' => 'CENTRO-OESTE', 'CS' => 'CENTRO-SUL', 'L' => 'LESTE', 'N' => 'NORTE', 'O' => 'OESTE', 'S' => 'SUL', 'R' => 'RURAL', '' => '', 'I' => 'Interior');

    public $codigos = array(

        '01' => '01',

        '02' => '02',

        '03' => '03',

        '04' => '04',

        '05' => '05',

        '06' => '06',

        '07' => '07',

        '08' => '08',

        '09' => '09',

        '10' => '10',

        '11' => '11',

        '12' => '12',

        '13' => '13',

        '14' => '14',

        '15' => '15',

        '16' => '16',

        '17' => '17',

        '18' => '18',

        '19' => '19',

        '20' => '20',

        '' => ''

    );


    public $tipo_veiculo = array('Moto' => 'Moto',

        'Sedã' => 'Sedã',

        'Hatch' => 'Hatch',

        'Pick-up P' => 'Pick-up P',

        'Pick-up G' => 'Pick-up G',

        'SUV' => 'SUV', '' => '');


    public $falecido = array('S' => 'Sim', 'N' => 'Nao', '' => '');


    public function init()

    {

        parent::init();


        if (empty(Yii::app()->user))

            $this->redirect(array('acesso/login'));


        Yii::import('ext.PHPExcel.YPHPExcel');

        Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . "/js/plugins/jquery.mask.js", CClientScript::POS_END);

        Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . "/js/eleitor.js", CClientScript::POS_BEGIN);


    }


    public function actionIndex()

    {

        $engajamento = $this->engajamento;

        $qtdeDependetes = $this->qtdeDependetes;

        $faixaEtaria = $this->faixaEtaria;

        $codigos = $this->codigos;

        $model = new Eleitor;

        $model->unsetAttributes();


        if (isset($_GET['Eleitor'])) {

            $model->attributes = $_GET['Eleitor'];

        }

        $this->render('index', array('model' => $model,

            'engajamento' => $engajamento,

            'qtdeDependetes' => $qtdeDependetes,

            'faixaEtaria' => $faixaEtaria,

            'codigos' => $codigos

        ));


    }


    public function actionImprimeFichap($id)

    {

        $model = Eleitor::model()->findByPk($id);

        $sexo = $this->sexo;

        $estado_civil = $this->estado_civil;

        $engajamento = $this->engajamento;

        $lider_comunitario = $this->lider_comunitario;

        $tipo_residencia = $this->tipo_residencia;

        $possui_veiculo = $this->possui_veiculo;

        $qtdeDependetes = $this->qtdeDependetes;

        $situacao_titulo = $this->situacao_titulo;

        $zona_moradia = $this->zona_moradia;

        $codigos = $this->codigos;

        $falecido = $this->falecido;

        $tipo_veiculo = $this->tipo_veiculo;

        $faixaEtaria =  $this->faixaEtaria;


        $coordenador = Usuario::model()->findByPk($model->coordenador);

        $lider = Usuario::model()->findByPk($model->lider);


        $html_footer = Yii::app()->name . " v1.5 -" . date('d/m/Y H:i:s');


        $html_titulo = 'FICHA DE CADASTRO ' . date('Y');


        $pdf = new Relatorio(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information

        $pdf->SetCreator('TCPDF');

        $pdf->SetAuthor('Cadastro');

        $pdf->SetLeftMargin(0);


        $pdf->AddPage();

        $pdf->SetFont('dejavusansmono', '', 8);


        $table = '';

        $rows = '';

        $cont = 0;


        $pdf->SetLeftMargin(10);

        //definindo o cabecalho


        /*$html_header  = $html_titulo;

        $pdf->writeHTMLCell(

            $w = 0, $h = 0, $x = '', $y = '',

            $html_header, $border = 0, $ln = 1, $fill = 0,

            $reseth = true, $align = 'top', $autopadding = false);*/


        $pdf->Ln(2);


        $pdf->writeHTML('<H2 align="center">' . $html_titulo . '</H2>', true, false, false, false, '');


        $pdf->Ln(10);

        //CONTEUDO


        $conteudo_html = '<table width="100%" border="0" CELLSPACING="7">
        <tr><td><b>INDICADOR POR:</b> ' . $model->indicado_por . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>DATA DA VISITA:</b> ' . $model->data_visita. '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>ENGAJAMENTO:</b> ' . $model->engajamento . '</td></tr>
        <tr><td><b>LÍDER:</b> ' . $model->nome . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>CÓDIGO:</b> ' . $codigos[$model->codigo]  . '</td></tr>
        
        <tr><td></td></tr>  
        <tr><td><STRONG>DADOS PESSOAIS:</STRONG> </td></tr>
        <tr><td><b>NOME:</b> ' . $model->nome . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>CPF:</b> '.  $model->cpf . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>RG:</b> '.  $model->rg . ' </td></tr>
        <tr><td><b>DT DE NASCIMENTO:</b> ' . $model->data_nasc . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>NOME DA MÃE:</b> '.  $model->nome_mae . ' </td></tr>
        <tr><td><b>NOME DO PAI:</b> '.  $model->nome_pai .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>SEXO:</b> ' .  $sexo[$model->sexo] . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>ESTADO CIVIL:</b> '. $estado_civil[$model->estado_civil] . ' </td></tr>
        <tr><td><b>CEP:</b> ' . $model->cep . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>ENDEREÇO:</b> '.  $model->endereco . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>N:</b> '.  $model->numero . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>BAIRRO/CONJUNTO:</b> '.  $model->bairroconjunto . ' </td></tr>
        <tr><td><b>BAIRRO:</b> ' . $model->bairro . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>ZONA:</b> ' . $model->zona_moradia . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>ESTADO:</b> '.  $model->estado . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>MUNICÍPIO:</b> '.  $model->municipio .' </td></tr>
       
        <tr><td></td></tr>  
        <tr><td><STRONG>DADOS DE CONTATO \ REDES SOCIAIS:</STRONG> </td></tr>
        <tr><td><b>TELEFONE:</b> ' . $model->telefone . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>TELEFONE RECADO:</b> '.  $model->telefone_recado . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>CELULAR RECADO:</b> '.  $model->cel_recado . ' </td></tr>
        <tr><td><b>WHATSAPP:</b> ' . $model->wathsapp . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>E-MAIL:</b> '.  $model->email .' </td></tr>
        <tr><td><b>INSTAGRAM:</b> ' . $model->instagram. '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>FACE:</b> '.  $model->facebook . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>TWITTER:</b> '.  $model->twitter . ' </td></tr>
        
        <tr><td></td></tr>  
        <tr><td><STRONG>DADOS DE DEPENDENTES:</STRONG> </td></tr>
        <tr><td><b>QUANTIDADE DE DEPENDENTES:</b> ' . $model->qtdeDependetes . ' </td></tr>';

        if ( $model->qtdeDependetes >= 1 )
            $conteudo_html = $conteudo_html . '<tr><td><b>DEPENDENTE:</b> ' . $model->nome_dep_1 .' ('.  $faixaEtaria[$model->faixa_dep_1].') </td></tr>';
        if ( $model->qtdeDependetes >= 2 )
            $conteudo_html =$conteudo_html . '<tr><td><b>DEPENDENTE:</b> ' . $model->nome_dep_2 .' ('.  $faixaEtaria[$model->faixa_dep_2].') </td></tr>';
        if ( $model->qtdeDependetes >= 3 )
            $conteudo_html =$conteudo_html . '<tr><td><b>DEPENDENTE:</b> ' . $model->nome_dep_3 .' ('.  $faixaEtaria[$model->faixa_dep_3].') </td></tr>';
        if ( $model->qtdeDependetes >= 4 )
            $conteudo_html =$conteudo_html . ' <tr><td><b>DEPENDENTE:</b> ' . $model->nome_dep_4 .' ('.  $faixaEtaria[$model->faixa_dep_4].') </td></tr>';
        
        $conteudo_html =$conteudo_html . '<tr><td></td></tr> 
        
        <tr><td></td></tr>  
        <tr><td><STRONG>DADOS SOCIECONÔMICOS:</STRONG> </td></tr>
        <tr><td><b>N. PESSOAS NA FAMÍLIA:</b> ' . $model->numero_pesssoas_familia . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>PROFISSÃO:</b> '.  $model->profissao . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>TIPO DE RESIDÊNCIA:</b> '.   $tipo_residencia[$model->tipo_residencia] . ' </td></tr>
        <tr><td><b>N. CARTÃO SUS:</b> ' . $model->cartao_sus . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>LÍDER COMUNITÁRIO:</b> '.  $model->lider_comunitario . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>POSSUI VEÍCULO:</b> '. $possui_veiculo[$model->possui_veiculo] . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TIPO: ' . $tipo_veiculo[$model->tipo_veiculo] . ' </td></tr>
        
        <tr><td></td></tr>  
        <tr><td><STRONG>DADOS ELEITORAIS:</STRONG> </td></tr>
        <tr><td>TÍTULO DE ELEITOR: ' . $model->titulo_eleitor . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZONA: ' . $model->zona . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SEÇÃO: ' . $model->secao  .' </td></tr>
        <tr><td>LOCAL DE VOTAÇÃO: '. $model->local_votacao . '</td></tr>
        <tr><td>ENDEREÇO VOTAÇÃO: ' . $model->endereco_votacao .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BAIRRO VOTAÇÃO: ' . $model->bairro_votacao . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MUNICÍPIO VOTAÇÃO: ' . $model->municipio_votacao. '</td></tr>
        <tr><td>DOMICÍLIO ELEITORAL: ' . $model->domicilio_eleitoral . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SITUAÇÃO DO TÍTULO: ' . $situacao_titulo[$model->situacao_titulo] . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EST. VOTOS: ' .  $model->estimativa_votos . '</td></tr>

        <tr><td></td></tr>  
        <tr><td style="width:480px"> OBSERVAÇÃO: ' . $model->observacao . '</td></tr>

        </table>';


        $pdf->SetLeftMargin(20);

        $pdf->writeHTML($conteudo_html, true, false, false, false, '');


        $pdf->Ln(2);


        //definindo o rodape

        $pdf->SetLeftMargin(10);

        $pdf->SetFont('dejavusansmono', '', 6.5);

        $pdf->writeHTMLCell(

            $w = 0, $h = 0, $x = '', $y = '',

            $html_footer, $border = 0, $ln = 1, $fill = 0,

            $reseth = true, $align = 'top', $autopadding = false);


        $pdf->Output('ficha_cadastral.pdf', 'I'); //utilizado p debug e visualizar a saida


        //$pdf->Output('cupomnaofiscal.pdf', 'D'); //esse aqui é o certo p doewnload


    }


    public function actionImprimeFicha()
    {
        $html_footer = Yii::app()->name . " v1.5 -" . date('d/m/Y H:i:s');

        $html_titulo = 'FICHA DE CADASTRO ' . date('Y');


        $pdf = new Relatorio(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information

        $pdf->SetCreator('TCPDF');

        $pdf->SetAuthor('Cadastro');

        $pdf->SetLeftMargin(0);


        $pdf->AddPage();

        $pdf->SetFont('dejavusansmono', '', 8);


        $table = '';

        $rows = '';

        $cont = 0;


        $pdf->SetLeftMargin(10);

        //definindo o cabecalho


        /*$html_header  = $html_titulo;

        $pdf->writeHTMLCell(

            $w = 0, $h = 0, $x = '', $y = '',

            $html_header, $border = 0, $ln = 1, $fill = 0,

            $reseth = true, $align = 'top', $autopadding = false);*/


        $pdf->Ln(2);


        $pdf->writeHTML('<H2 align="center">' . $html_titulo . '</H2>', true, false, false, false, '');


        $pdf->Ln(6);

        //CONTEUDO


        $conteudo_html = '<table width="800" border="0" CELLSPACING="6">
        <tr><td>LÍDER:_________________________________________  INDICADOR POR:__________________________________</td></tr>
        <tr><td>DATA DA VISITA:____/______/_______ HORA:___:___ ENGAJAMENTO:_____________________________________</td></tr>
        <tr><td>CÓDIGO</td></tr> 
        <tr><td>   ( )CÓD01   ( )CÓD02   ( )CÓD03   ( )CÓD04   ( )CÓD05   ( )CÓD06   ( )CÓD07   ( )CÓD08   ( )CÓD09   ( )CÓD10</td></tr>
        <tr><td>   ( )CÓD11   ( )CÓD12   ( )CÓD13   ( )CÓD14   ( )CÓD15   ( )CÓD16   ( )CÓD17   ( )CÓD18   ( )CÓD19   ( )CÓD20</td></tr>
        <tr><td></td></tr> 
        <tr><td><STRONG>DADOS PESSOAIS:</STRONG>                                                      </td></tr>
        <tr><td>NOME:________________________________________________ CPF:____________________ RG:______________</td></tr>
        <tr><td>NOME DA MÃE:___________________________________ NOME DO PAI:____________________________________</td></tr>
        <tr><td>DT DE NASCIMENTO:___/____/______ SEXO: ()F ()M ()O ESTADO CIVIL:________________________________</td></tr>
        <tr><td>CEP:_______________ ENDEREÇO:________________________________________________________N:_________</td></tr>
        <tr><td>BAIRRO/CONJUNTO:_____________________________________________ ZONA:_____________________________</td></tr>
        <tr><td>ESTADO:___________________________________________________ MUNICÍPIO:___________________________</td></tr>
        <tr><td></td></tr>
        <tr><td><STRONG>DADOS DE CONTATO \ REDES SOCIAIS:</STRONG>                                    </td></tr>
        <tr><td>TELEFONE:(  )_________________ TEL RECADO:(  )_________________ CEL RECADO:( )__________________</td></tr>
        <tr><td>WHATSAPP:(  )_________________ EMAIL:___________________________________________________________</td></tr>
        <tr><td>INSTAGRAM:_______________________ FACE:________________________TWITTER:_________________________</td></tr>
        <tr><td></td></tr> 
        <tr><td><STRONG>DEPENDENTES:</STRONG>                                                         </td></tr>
        <tr><td>NOME:____________________________________________________ ()MENOR DE 16 ANOS ()ACIMA DE 60 ANOS </td></tr>
        <tr><td>NOME:____________________________________________________ ()MENOR DE 16 ANOS ()ACIMA DE 60 ANOS </td></tr>
        <tr><td>NOME:____________________________________________________ ()MENOR DE 16 ANOS ()ACIMA DE 60 ANOS </td></tr>
        <tr><td>NOME:____________________________________________________ ()MENOR DE 16 ANOS ()ACIMA DE 60 ANOS </td></tr>
        <tr><td></td></tr> 
        <tr><td><STRONG>DADOS SOCIECONÔMICOS:</STRONG>                                                </td></tr>
        <tr><td>QUANTIDADE PESSOAS NA FAMÍLIA:_____________ PROFISSÃO:__________________________________________</td></tr>
        <tr><td>TIPO DE RESIDÊNCIA: ( )PRÓPRIA ( )ALUGADA ( )MORA A FAVOR ( )OUTROS                   </td></tr>
        <tr><td>N. DO CARTÃO SUS:_________________________________________ LÍDER COMUNITÁRIO: ( )SIM ( )NÃO     </td></tr>
        <tr><td>VEÍCULO:( )SIM ( )NÃO            TIPO:( )MOTO ( )SEDÃ ( )HATCH ( )PICK-UP P ( )PICK-UP G ( )SUV </td></tr>
        <tr><td></td></tr> 
        <tr><td><STRONG>DADOS ELEITORAIS:</STRONG>                                                    </td></tr>
        <tr><td>TÍTULO DE ELEITOR:_______________________ ZONA:_____________________ SEÇÃO:_____________________</td></tr>
        <tr><td>LOCAL DE VOTAÇÃO:_______________________________________________________________________________</td></tr>
        <tr><td>ENDEREÇO:____________________________________________ BAIRRO DE VOTAÇÃO:________________________</td></tr>
        <tr><td>MUNICÍPIO:______________________________________ DOMICÍLIO ELEITORAL:___________________________</td></tr>
        <tr><td>SITUAÇÃO DO TÍTULO:____________________________________ EST. VOTOS:_____________________________</td></tr>
        <tr><td></td></tr>
        <tr><td>OBSERVAÇÃO:_____________________________________________________________________________________</td></tr>
        <tr><td>________________________________________________________________________________________________</td></tr>
        <tr><td>________________________________________________________________________________________________</td></tr>
        <tr><td>________________________________________________________________________________________________</td></tr>
        <tr><td>________________________________________________________________________________________________</td></tr>
        </table>
        ';

        $pdf->SetLeftMargin(20);

        $pdf->writeHTML($conteudo_html, true, false, false, false, '');


        $pdf->Ln(2);


        //definindo o rodape

        $pdf->SetLeftMargin(10);

        $pdf->SetFont('dejavusansmono', '', 6.5);

        $pdf->writeHTMLCell(

            $w = 0, $h = 0, $x = '', $y = '',

            $html_footer, $border = 0, $ln = 1, $fill = 0,

            $reseth = true, $align = 'top', $autopadding = false);


        $pdf->Output('ficha_cadastral.pdf', 'I'); //utilizado p debug e visualizar a saida


        //$pdf->Output('cupomnaofiscal.pdf', 'D'); //esse aqui é o certo p doewnload


    }


    public function actionFiltrar()
    {
        try {
            $id = Yii::app()->user->id;

            $filtro_sessao = $_GET;

            $filtro_sql = array();
            if (isset($filtro_sessao['nome']) && $filtro_sessao['nome'] != "")
                $filtro_sql[] = "nome like '%" . $filtro_sessao['nome'] . "%'";
            
            if (isset($filtro_sessao['cpf']) && $filtro_sessao['cpf'] != "")
                $filtro_sql[] = "cpf = '" . $filtro_sessao['cpf'] . "'";    

            if (isset($filtro_sessao['coordenador']) && strlen($filtro_sessao['coordenador']) > 0)

                $filtro_sql[] = "coordenador like '%" . $filtro_sessao['coordenador'] . "%'";


            if (isset($filtro_sessao['lider']) && strlen($filtro_sessao['lider']) > 0)

                $filtro_sql[] = "lider like '%" . $filtro_sessao['lider'] . "%'";


            if (isset($filtro_sessao['bairro']) && strlen($filtro_sessao['bairro']) > 0)

                $filtro_sql[] = "bairro like '%" . $filtro_sessao['bairro'] . "%'";


            if (isset($filtro_sessao['municipio']) && strlen($filtro_sessao['municipio']) > 0)

                $filtro_sql[] = "municipio like '%" . $filtro_sessao['municipio'] . "%'";
            
            if (isset($filtro_sessao['observacao']) && strlen($filtro_sessao['observacao']) > 0)

                $filtro_sql[] = "observacao like '%" . $filtro_sessao['observacao'] . "%'";


            if (isset($filtro_sessao['engajamento']) && strlen($filtro_sessao['engajamento']) > 0)

                $filtro_sql[] = "engajamento = '" . $filtro_sessao['engajamento'] . "'";


            if (isset($filtro_sessao['qtdeDependetes']) && strlen($filtro_sessao['qtdeDependetes']) > 0)

                $filtro_sql[] = "qtdeDependetes = " . $filtro_sessao['qtdeDependetes'] ;


            if (isset($filtro_sessao['codigo']) && strlen($filtro_sessao['codigo']) > 0)

                $filtro_sql[] = "codigo like '%" . $filtro_sessao['codigo'] . "%'";

            if (isset($filtro_sessao['nomeDep']) && strlen($filtro_sessao['nomeDep']) > 0)

                $filtro_sql[] = "( nome_dep_1 like '%" . $filtro_sessao['nomeDep'] . "%' or nome_dep_2 like '%" . $filtro_sessao['nomeDep'] . "%' or nome_dep_3 like '%" . $filtro_sessao['nomeDep'] . "%' or nome_dep_4 like '%" . $filtro_sessao['nomeDep'] . "%')";
            
            if (isset($filtro_sessao['faixaEtaria']) && strlen($filtro_sessao['faixaEtaria']) > 0) {
                $faixa = '';
                if ($filtro_sessao['faixaEtaria'] === '0') {
                    $faixa = 'Menor de 16 anos';
                } else {
                    $faixa = 'Acima de 60 anos';
                }
                $filtro_sql[] = "( faixa_dep_1 ='" . $faixa . "' or faixa_dep_2 ='" . $faixa . "' or faixa_dep_3 ='" . $faixa . "' or faixa_dep_4 ='" . $faixa . "')";
            }

            if (isset($filtro_sessao['nome_pai']) && strlen($filtro_sessao['nome_pai']) > 0)
                $filtro_sql[] = "nome_pai like '%" . $filtro_sessao['nome_pai'] . "%'";
            
            if (isset($filtro_sessao['endereco_votacao']) && strlen($filtro_sessao['endereco_votacao']) > 0)
                $filtro_sql[] = "endereco_votacao like '%" . $filtro_sessao['endereco_votacao'] . "%'";

            if (isset($filtro_sessao['bairro_votacao']) && strlen($filtro_sessao['bairro_votacao']) > 0)
                $filtro_sql[] = "bairro_votacao like '%" . $filtro_sessao['bairro_votacao'] . "%'";
            if (isset($filtro_sessao['municipio_votacao']) && strlen($filtro_sessao['municipio_votacao']) > 0)
                $filtro_sql[] = "municipio_votacao like '%" . $filtro_sessao['municipio_votacao'] . "%'";

            if (isset($filtro_sessao['twitter']) && strlen($filtro_sessao['twitter']) > 0)
                $filtro_sql[] = "twitter like '%" . $filtro_sessao['twitter'] . "%'";

            if (isset($filtro_sessao['telefone_recado']) && strlen($filtro_sessao['telefone_recado']) > 0)
                $filtro_sql[] = "telefone_recado like '%" . $filtro_sessao['telefone_recado'] . "%'";

            if (isset($filtro_sessao['cel_recado']) && strlen($filtro_sessao['cel_recado']) > 0)
                $filtro_sql[] = "cel_recado like '%" . $filtro_sessao['cel_recado'] . "%'";

            $filtro_str = '';


            if (count($filtro_sql) > 1)

                $filtro_str = implode(' and ', $filtro_sql);

            else if (count($filtro_sql) == 1)

                $filtro_str = ' ' . $filtro_sql[0];

            else if (count($filtro_sql) == 0)

                $filtro_str = ' true ';


            if (Yii::app()->user->perfil == "C") {

                //como coordenador, tem a visao de todos os eleitores de sua coordenacao

                $sql = Yii::app()->db->createCommand("SELECT * FROM vw_eleitor WHERE coordenador_cpf = '" . $id . "' and {$filtro_str} order by data_cadastro desc")->queryAll();

            } elseif (Yii::app()->user->perfil == "L") {

                //como lider, tema a visao de todos os eleitores de sua lideranca

                $sql = Yii::app()->db->createCommand("SELECT * FROM vw_eleitor WHERE lider_cpf = '" . $id . "' and {$filtro_str} order by data_cadastro desc")->queryAll();

            } else {

                //como administrador, tem a visao geral de todos os eleitores cadastrados

                $sql = Yii::app()->db->createCommand("SELECT * FROM vw_eleitor WHERE {$filtro_str} order by data_cadastro desc")->queryAll();

            }

            $num_rows = count($sql);


            $obj = new stdClass();

            $obj->draw = 1;

            $obj->recordsTotal = $num_rows;

            $obj->recordsFiltered = $num_rows;

            $obj->data = array();


            foreach ($sql as $key => $value) {

                $obj->data[] = list(

                    $coordenador

                    , $lider

                    , $cpf

                    , $nome

                    , $codigo

                    , $profissao

                    , $data_nasc

                    , $wathsapp

                    , $bairro

                    , $sexo

                    , $rg

                    , $nome_mae

                    , $telefone

                    , $email

                    , $lider_cpf

                    , $data_visita

                    , $hora_visita

                    , $indicado_por

                    , $facebook

                    , $instagram

                    , $engajamento

                    , $endereco

                    , $numero

                    , $estado

                    , $municipio

                    , $cep

                    , $zona

                    , $secao

                    , $numero_pesssoas_familia

                    , $lider_comunitario

                    , $tipo_residencia

                    , $possui_veiculo

                    , $tipo_veiculo

                    , $titulo_eleitor

                    , $situacao_titulo

                    , $estimativa_votos

                    , $data_cadastro

                    , $zona_moradia

                    , $local_votacao

                    , $coordenador_cpf

                    , $observacao

                    , $qtdeDependetes

                    ,$nome_dep_1

                    ,$faixa_dep_1

                    ,$nome_dep_2

                    ,$faixa_dep_2

                    ,$nome_dep_3

                    ,$faixa_dep_3

                    ,$nome_dep_4

                    ,$faixa_dep_4

                    ,$nome_pai

                    ,$endereco_votacao
        
                    ,$bairro_votacao
        
                    ,$municipio_votacao
        
                    ,$twitter
        
                    ,$telefone_recado
        
                    ,$cel_recado

                    ) = array_values($value);

            }


            echo CJSON::encode($obj);


        } catch (Exception $e) {

            echo json_encode(array(

                'status' => 'Error',

                'detalhes' => $e->getMessage()

            ));

        }

    }


    public function actionListaEleitores()

    {

        if (Yii::app()->request->isAjaxRequest) {

            $produtos = new Eleitor();

            echo $produtos->listData();

        }

    }


    public function actionListaEleitoreslog()

    {

        if (Yii::app()->request->isAjaxRequest) {

            $produtos = new Eleitorlog();

            echo $produtos->listData();

        }

    }


    public function actionListaBairros()

    {

        if (Yii::app()->request->isAjaxRequest) {

            try {

                if (isset($_POST['municipio'])) {


                    $municipio = $_POST['municipio'];


                    $sql = Yii::app()->db->createCommand("SELECT distinct bairro FROM tb_zona_eleitoral where UPPER(municipio) = UPPER('$municipio')")->queryAll();


                    $html = '<option value="">--selecione--</option>';

                    for ($i = 0; $i <= count($sql) - 1; $i++) {

                        $html .= '<option value="' . $sql[$i]['bairro'] . '">' . $sql[$i]['bairro'] . '</option>';

                    }


                }


                echo json_encode(array(

                    'status' => 'OK',

                    'detalhes' => $html

                ));


            } catch (Exception $e) {

                echo json_encode(array(

                    'status' => 'Error',

                    'detalhes' => $e->getMessage()

                ));

            }


        }

    }


    public function actionListaZona()

    {

        if (Yii::app()->request->isAjaxRequest) {

            try {

                if (isset($_POST['bairro'])) {


                    $bairro = $_POST['bairro'];

                    $encontrado = '';


                    $sql = Yii::app()->db->createCommand("SELECT distinct zona_abrev FROM tb_bairro_zona where UPPER(bairro) = UPPER('$bairro')")->queryAll();


                    if (!empty($sql)) {

                        $encontrado = $sql[0]['zona_abrev'];

                    }


                }


                echo json_encode(array(

                    'status' => 'OK',

                    'detalhes' => $encontrado

                ));


            } catch (Exception $e) {

                echo json_encode(array(

                    'status' => 'Error',

                    'detalhes' => $e->getMessage()

                ));

            }


        }

    }


    public function actionPesquisarCpf()

    {

        if (Yii::app()->request->isAjaxRequest) {


            try {

                if (isset($_POST['cpf'])) {


                    $resultado = '';

                    $cpf_eleitor = $_POST['cpf'];

                    $cpf_lider = '';


                    //verifica se CPF está duplicado

                    $eleitor = Eleitor::model()->findByPk($cpf_eleitor);


                    if ($eleitor != NULL) {


                        //cpf já cadastrado

                        $resultado = 'SIM';

                        $cpf_lider = $eleitor->lider;


                        //verifica quem é o lider

                        $lider = Usuario::model()->findByPk($eleitor->lider);


                        if (!empty($eleitor) || $eleitor != NULL || $lider != NULL) {

                            $errors[] = 'CPF já cadastrado na base de dados pelo líder :' . @$lider->nome;

                        }

                    } else {

                        $resultado = 'NÃO';

                    }


                    echo CJSON::encode(array(

                        'status' => "Ok",

                        'resultado' => $resultado,

                        'lider' => $cpf_lider,

                        'lider_nome' => ($resultado == 'SIM') ? @$lider->nome : '',

                        'eleitor' => $cpf_eleitor,

                    ));


                }

            } catch (Exception $e) {

                echo CJSON::encode(array(

                    'status' => "Erro",

                    'detalhes' => $e->getMessage(),

                ));

            }

        }

    }


    public function actionPesquisar()

    {

        $this->render('pesquisar', array());

    }


    public function actionNovo()

    {


        $model = new Eleitor;

        $sexo = $this->sexo;

        $estado_civil = $this->estado_civil;

        $engajamento = $this->engajamento;

        $lider_comunitario = $this->lider_comunitario;

        $tipo_residencia = $this->tipo_residencia;

        $possui_veiculo = $this->possui_veiculo;

        $qtdeDependetes = $this->qtdeDependetes;

        $situacao_titulo = $this->situacao_titulo;

        $zona_moradia = $this->zona_moradia;

        $codigos = $this->codigos;

        $falecido = $this->falecido;

        $tipo_veiculo = $this->tipo_veiculo;

        $faixaEtaria = $this->faixaEtaria;


        $bairros = CHtml::listData(BairroZona::model()->findAll(), 'bairro', 'bairro');


        $model->unsetAttributes();


        $this->render('novo', array(

            'model' => $model,

            'sexo' => $sexo,

            'estado_civil' => $estado_civil,

            'engajamento' => $engajamento,

            'lider_comunitario' => $lider_comunitario,

            'tipo_residencia' => $tipo_residencia,

            'possui_veiculo' => $possui_veiculo,

            'qtdeDependetes' => $qtdeDependetes,

            'situacao_titulo' => $situacao_titulo,

            'zona_moradia' => $zona_moradia,

            'codigos' => $codigos,

            'falecido' => $falecido,

            'tipo_veiculo' => $tipo_veiculo,

            'bairros' => $bairros,

            'faixaEtaria' => $faixaEtaria


        ));

    }


    /*public function implode_all($glue, $arr){



        for ($i=0; $i<count($arr); $i++) {



            if (@is_array($arr[$i])) 



                $arr[$i] = $this->implode_all($glue, $arr[$i]);



        }            



        return implode($glue, $arr);



    }*/


    public function implode_all($glue, $arr)
    {

        if (is_array($arr)) {


            foreach ($arr as $key => &$value) {


                if (@is_array($value)) {

                    $arr[$key] = $this->implode_all($glue, $value);

                }

            }


            return implode($glue, $arr);

        }


        // Not array

        return $arr;

    }


    public function actionAdicionaEleitor()

    {


        if (Yii::app()->request->isAjaxRequest) {

            if (isset($_POST['Eleitor'])) {


                // echo "<pre>";

                // print_r($_REQUEST);

                // exit;


                $model = new Eleitor;


                $errors = array();

                $transaction = Yii::app()->db->beginTransaction();

                $model->attributes = $_POST['Eleitor'];

                $model->codigo = (!empty($_POST['Eleitor']['codigo'])) ? implode($_POST['Eleitor']['codigo'], '#') : '';


                $model->zona = $_POST['Eleitor']['zona'];

                $model->observacao = $_POST['Eleitor']['observacao'];


                //se for um lider, pegar seu coordenador. Se for um coordenador, atribuir o proprio ID

                try {


                    if (empty(Yii::app()->user) || @Yii::app()->user->perfil == null) {

                        $this->redirect(array('acesso/login'));

                    } else {

                        $model->coordenador = (Yii::app()->user->perfil == "L") ? Yii::app()->user->coordenador : Yii::app()->user->id;

                        $model->lider = Yii::app()->user->id;

                    }


                } catch (Exception $e) {

                    $this->redirect(array('acesso/login'));

                }


                //verifica se CPF está duplicado

                $eleitor = Eleitor::model()->findByPk($model->cpf);


                if ($eleitor != NULL) {

                    $lider = Usuario::model()->findByPk($eleitor->lider);


                    if (!empty($eleitor) || $eleitor != NULL || $lider != NULL) {

                        $errors[] = 'CPF já cadastrado na base de dados pelo líder :' . @$lider->nome;

                    }

                }


                if (empty($errors)) {


                    $model->validate();


                    if (!$model->save()) {

                        $errors[] = $model->getErrors();

                    }


                }


                //salvando informacoes do eleitor


                if (empty($errors)) {

                    $transaction->commit();

                    //Yii::app()->user->setFlash('success', "Registro criado com sucesso!");

                    //$this->redirect(array('index', array()));


                    echo CJSON::encode(array(

                        'status' => "Ok",

                        'detalhes' => '',

                    ));


                } else {

                    $transaction->rollback();

                    // print_r($errors);

                    // exit;

                    echo CJSON::encode(array(

                        'status' => "Error",

                        'detalhes' => $this->implode_all(',', $errors),

                    ));

                    // Yii::app()->user->setFlash('error', "Falha ao criar registro:");


                }


            }

        }


    }


    public function actionVisualizar($id)

    {


        if (isset($id)) {


            $model = Eleitor::model()->findByPk($id);

            $sexo = $this->sexo;

            $estado_civil = $this->estado_civil;

            $engajamento = $this->engajamento;

            $lider_comunitario = $this->lider_comunitario;

            $tipo_residencia = $this->tipo_residencia;

            $possui_veiculo = $this->possui_veiculo;

            $qtdeDependetes = $this->qtdeDependetes;

            $faixaEtaria = $this->faixaEtaria;

            $situacao_titulo = $this->situacao_titulo;

            $zona_moradia = $this->zona_moradia;

            $codigos = $this->codigos;

            $falecido = $this->falecido;

            $tipo_veiculo = $this->tipo_veiculo;


            $bairros = CHtml::listData(BairroZona::model()->findAll(), 'bairro', 'bairro');

            // $lideres = CHtml::listData(Usuario::model()->findAll("perfil = 'L'"), 'cpf', 'nome');

            $lideres = CHtml::listData(Usuario::model()->findAll("perfil = 'L' order by nome ASC"), 'cpf', 'nome');

            $coordenadores = CHtml::listData(Usuario::model()->findAll("perfil = 'C' order by nome ASC"), 'cpf', 'nome');


            //lista bairros do municipio

            if ($model->municipio != "") {

                $sql = Yii::app()->db->createCommand("SELECT distinct bairro FROM tb_zona_eleitoral where municipio = '$model->municipio'")->queryAll();


                for ($i = 0; $i <= count($sql) - 1; $i++) {

                    $bairros_municipio[$sql[$i]['bairro']] = $sql[$i]['bairro'];

                }


            }


            //lista zonas do bairro

            if ($model->bairro != "") {

                $sql = Yii::app()->db->createCommand("SELECT distinct zona FROM tb_zona_eleitoral /*where UPPER(bairro) = UPPER('$model->bairro')*/")->queryAll();


                for ($i = 0; $i <= count($sql) - 1; $i++) {

                    $zonas_bairro[$sql[$i]['zona']] = $sql[$i]['zona'];

                }


            }


            $coordenador = Usuario::model()->findByPk($model->coordenador);

            $lider = Usuario::model()->findByPk($model->lider);


            $this->render('visualizar', array(

                'model' => $model,

                'sexo' => $sexo,

                'estado_civil' => $estado_civil,

                'engajamento' => $engajamento,

                'lider_comunitario' => $lider_comunitario,

                'tipo_residencia' => $tipo_residencia,

                'possui_veiculo' => $possui_veiculo,

                'situacao_titulo' => $situacao_titulo,

                'zona_moradia' => $zona_moradia,

                'codigos' => $codigos,

                'coordenador' => $coordenador->nome,

                'lider' => $lider->nome,

                'falecido' => $falecido,

                'tipo_veiculo' => $tipo_veiculo,

                'bairros' => $bairros,

                'lideres' => $lideres,

                'coordenadores' => $coordenadores,

                'qtdeDependetes' => $qtdeDependetes,

                'faixaEtaria' => $faixaEtaria

            ));


        }


    }


    public function actionSalvar($id)

    {


        if (Yii::app()->request->isAjaxRequest) {

            //echo "salvar <pre> id: " . $id;

            // print_r($_POST);

            // die;

            //exit;

            $model = Eleitor::model()->findByPk($id);

            if (isset($_POST['Eleitor'])) {

                // $model->attributes = $_POST['Eleitor'];

                $errors = array();

                $transaction = Yii::app()->db->beginTransaction();


                $model->attributes = $_POST['Eleitor'];


                // print_r($model->attributes);

                // die;


                $model->codigo = (!empty($_POST['Eleitor']['codigo'])) ? implode($_POST['Eleitor']['codigo'], '#') : '';


                $model->zona = $_POST['Eleitor']['zona'];


                try {

                    //se for um lider, pegar seu coordenador. Se for um coordenador, atribuir o proprio ID

                    // $model->coordenador = (@Yii::app()->user->perfil == "L") ? @Yii::app()->user->coordenador : @Yii::app()->user->id;

                    if (@Yii::app()->user->perfil == "L") {

                        $model->lider = Yii::app()->user->id;

                        $model->coordenador = (@Yii::app()->user->perfil == "L") ? @Yii::app()->user->coordenador : @Yii::app()->user->id;

                    }

                } catch (Exception $e) {

                    $this->redirect(array('acesso/login'));

                }


                $model->validate();


                //salvando informacoes do eleitor

                if (!$model->save()) {

                    $errors[] = $model->getErrors();

                }


                //salvando informacoes do eleitor

                if (empty($errors)) {

                    $transaction->commit();

                    echo CJSON::encode(array(

                        'status' => "Ok",

                        'detalhes' => '',

                    ));


                } else {

                    $transaction->rollback();

                    echo CJSON::encode(array(

                        'status' => "Error",

                        'detalhes' => $this->implode_all(',', $errors),

                    ));


                }


            }

        }


        /*  $sexo               = $this->sexo;



          $engajamento        = $this->engajamento;



          $lider_comunitario  = $this->lider_comunitario;



          $tipo_residencia    = $this->tipo_residencia;



          $possui_veiculo     = $this->possui_veiculo;



          $situacao_titulo    = $this->situacao_titulo;



          $zona_moradia       = $this->zona_moradia;



          $codigos             = $this->codigos;







          $coordenador        = Usuario::model()->findByPk($model->coordenador);



          $lider              = Usuario::model()->findByPk($model->lider); */


        /* $this->render('visualizar', array(



             'model'              => $model,



             'sexo'               => $sexo,



             'engajamento'        => $engajamento,



             'lider_comunitario'  => $lider_comunitario,



             'tipo_residencia'    => $tipo_residencia,



             'possui_veiculo'     => $possui_veiculo,



             'situacao_titulo'    => $situacao_titulo,



             'zona_moradia'       => $zona_moradia,



             'codigos'            => $codigos,



             'coordenador'        => @$coordenador->nome,



             'lider'              => @$lider->nome,



         )); */

    }

    public function actionExportExcel()
    {
        $id = Yii::app()->user->id;
        $filtro_sessao = $_POST;

        $filtro_sql = array();
        if (isset($filtro_sessao['e_lider']) && $filtro_sessao['e_lider'] != "")
            $filtro_sql[] = "lider like '" . $filtro_sessao['e_lider'] . "%'";

        if (isset($filtro_sessao['e_cpf']) && $filtro_sessao['e_cpf'] != "")
            $filtro_sql[] = "cpf = '" . $filtro_sessao['e_cpf'] . "'";    

        if (isset($filtro_sessao['e_bairro']) && $filtro_sessao['e_bairro'] != "")
            $filtro_sql[] = "bairro like '" . $filtro_sessao['e_bairro'] . "%'";

        if (isset($filtro_sessao['e_municipio']) && strlen($filtro_sessao['e_municipio']) > 0)

            $filtro_sql[] = "municipio like '" . $filtro_sessao['e_municipio'] . "%'";

        if (isset($filtro_sessao['e_qtdeDependetes']) && strlen($filtro_sessao['e_qtdeDependetes']) > 0)

            $filtro_sql[] = "qtdeDependetes = " . $filtro_sessao['e_qtdeDependetes'] ;
            
        if (isset($filtro_sessao['e_observacao']) && strlen($filtro_sessao['e_observacao']) > 0)

            $filtro_sql[] = "observacao like '%" . $filtro_sessao['e_observacao'] . "%'";

        if (isset($filtro_sessao['e_nomeDep']) && strlen($filtro_sessao['e_nomeDep']) > 0)

            $filtro_sql[] = "( nome_dep_1 like '%" . $filtro_sessao['e_nomeDep'] . "%' or nome_dep_2 like '%" . $filtro_sessao['e_nomeDep'] . "%' or nome_dep_3 like '%" . $filtro_sessao['e_nomeDep'] . "%' or nome_dep_4 like '%" . $filtro_sessao['e_nomeDep'] . "%')";
        
        if (isset($filtro_sessao['e_faixaEtaria']) && strlen($filtro_sessao['e_faixaEtaria']) > 0) {
            $faixa = '';
            if ($filtro_sessao['e_faixaEtaria'] === '0') {
                $faixa = 'Menor de 16 anos';
            } else {
                $faixa = 'Acima de 60 anos';
            }
            $filtro_sql[] = "( faixa_dep_1 ='" . $faixa . "' or faixa_dep_2 ='" . $faixa . "' or faixa_dep_3 ='" . $faixa . "' or faixa_dep_4 ='" . $faixa . "')";
        }

        if (isset($filtro_sessao['e_nome_pai']) && $filtro_sessao['e_nome_pai'] != "")
            $filtro_sql[] = "nome_pai like '" . $filtro_sessao['e_nome_pai'] . "%'";
        if (isset($filtro_sessao['e_endereco_votacao']) && $filtro_sessao['e_endereco_votacao'] != "")
            $filtro_sql[] = "endereco_votacao like '" . $filtro_sessao['e_endereco_votacao'] . "%'";
        if (isset($filtro_sessao['e_bairro_votacao']) && $filtro_sessao['e_bairro_votacao'] != "")
            $filtro_sql[] = "bairro_votacao like '" . $filtro_sessao['e_bairro_votacao'] . "%'";
        if (isset($filtro_sessao['e_municipio_votacao']) && $filtro_sessao['e_municipio_votacao'] != "")
            $filtro_sql[] = "municipio_votacao like '" . $filtro_sessao['e_municipio_votacao'] . "%'";
        if (isset($filtro_sessao['e_twitter']) && $filtro_sessao['e_twitter'] != "")
            $filtro_sql[] = "twitter like '" . $filtro_sessao['e_twitter'] . "%'";
        if (isset($filtro_sessao['e_telefone_recado']) && $filtro_sessao['e_telefone_recado'] != "")
            $filtro_sql[] = "telefone_recado like '" . $filtro_sessao['e_telefone_recado'] . "%'";
        if (isset($filtro_sessao['e_cel_recado']) && $filtro_sessao['e_cel_recado'] != "")
            $filtro_sql[] = "cel_recado like '" . $filtro_sessao['e_cel_recado'] . "%'";


        if (isset($filtro_sessao['e_engajamento']) && $filtro_sessao['e_engajamento'] != "") {
            $eng = '';
            if ($filtro_sessao['e_engajamento'] == 'NEG') {
                $eng = 'Negativo';
            } else if ($filtro_sessao['e_engajamento'] == 'POS') {
                $eng = 'Positivo';
            } else {
                $eng = 'Neutro';
            }

            $filtro_sql[] = "engajamento = '" . $eng . "'";
        }

        if (isset($filtro_sessao['e_codigo']) && $filtro_sessao['e_codigo'] != "") {
                $filtro_sql[] = "codigo like '%" . $filtro_sessao['e_codigo'] . "%'";
        }

        if (count($filtro_sql) > 1)
            $filtro_str = implode(' and ', $filtro_sql);
        else if (count($filtro_sql) == 1)
            $filtro_str = ' ' . $filtro_sql[0];
        else if (count($filtro_sql) == 0)
            $filtro_str = ' true ';

        if (Yii::app()->user->perfil == "C") {
            //como coordenador, tem a visao de todos os eleitores de sua coordenacao
            $sql = Yii::app()->db->createCommand("SELECT * FROM vw_eleitor WHERE coordenador_cpf = '" . $id . "' and {$filtro_str} order by data_cadastro desc")->queryAll();
        } elseif (Yii::app()->user->perfil == "L") {
            //como lider, tema a visao de todos os eleitores de sua lideranca
            $sql = Yii::app()->db->createCommand("SELECT * FROM vw_eleitor WHERE lider_cpf = '" . $id . "' and {$filtro_str} order by data_cadastro desc")->queryAll();
        } else {
            //como administrador, tem a visao geral de todos os eleitores cadastrados
            $sql = Yii::app()->db->createCommand("SELECT * FROM vw_eleitor WHERE {$filtro_str} order by data_cadastro desc")->queryAll();
        }

        try {

            $colnames = [
                'lider' => "LIDER",
                'cpf' => "CPF",
                'nome' => "NOME",
                'codigo' => "CODIGO",
                'profissao' => "PROFISSÃO",
                'data_nasc' => "DATA NASC",
                'wathsapp' => "WATHSAPP",
                'bairro' => "BAIRRO",
                'sexo' => "SEXO",
                'rg' => "RG",
                'nome_mae' => "NOME MÃE",
                'telefone' => "TELEFONE",
                'email' => "EMAIL",
                'lider_cpf' => 'CPF LIDER',
                'data_visita' => "DATA VISITA",
                'hora_visita' => "HORA VISITA",
                'indicado_por' => "INDICADO POR",
                'facebook' => "FACEBOOK",
                'instagram' => "INSTAGRAM",
                'engajamento' => "ENGAJAMENTO",
                'endereco' => "ENDEREÇO",
                'numero' => "NÚMERO",
                'estado' => "ESTADO",
                'municipio' => "MUNICÍPIO",
                'cep' => "CEP",
                'zona' => "ZONA",
                'secao' => "SEÇÃO",
                'numero_pesssoas_familia' => "NÚM.PESSSOAS FAMILIA",
                'lider_comunitario' => "LÍDER COMUNITÁRIO",
                'tipo_residencia' => "TIPO RESIDÊNCIA",
                'possui_veiculo' => "POSSUI VEÍCULO",
                'tipo_veiculo' => "TIPO VEÍCULO",
                'titulo_eleitor' => "TÍTULO ELEITOR",
                'situacao_titulo' => "SITUAÇÃO TÍTULO",
                'estimativa_votos' => "ESTIMATIVA VOTOS",
                'data_cadastro' => "DATA CADASTRO",
                'zona_moradia' => "ZONA MORADIA",
                'local_votacao' => "LOCAL VOTAÇÃO",
                'observacao' => 'OBSERAÇÃO',
                'qtdeDependetes' => 'QUANTIDADE DE DEPENDENTES',
                'nome_dep_1' => 'NOME DEPENDENTE 1',
                'faixa_dep_1' => 'FAIXA DEPENDENTE 1',
                'nome_dep_2' => 'NOME DEPENDENTE 2',
                'faixa_dep_2' => 'FAIXA DEPENDENTE 2',
                'nome_dep_3' => 'NOME DEPENDENTE 3',
                'faixa_dep_3' => 'FAIXA DEPENDENTE 3',
                'nome_dep_4' => 'NOME DEPENDENTE 4',
                'faixa_dep_4' => 'FAIXA DEPENDENTE 4',
                'nome_pai' => 'NOME DO PAI',
                'endereco_votacao' => 'ENDEREÇO VOTAÇÃO',
                'bairro_votacao' => 'BAIRRO VOTAÇÃO',
                'municipio_votacao' => 'MUNICÍPIO VOTAÇÃO',
                'twitter'  => 'TWITTER',
                'telefone_recado' => 'TELEFONE RECADOS',
                'cel_recado' => 'CELULAR RECADOS'
            ];

            $result = array();
            array_push($result, $colnames);
            foreach ($sql as $row) {
                array_splice($row, 0, 1);
                array_splice($row, 38, 1);
                array_push($result, $row);
            }
            $file_name = str_replace("-", "", str_replace(".", "", $id));
            $xlsx = SimpleXLSXGen::fromArray($result);
            $xlsx->downloadAs($file_name . '_eleitores.xlsx');
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }


    public function loadModel($id)

    {

        $model = Eleitor::model()->findByPk($id);

        if ($model === null)

            throw new CHttpException(404, 'A página solicitada não existe.');

        return $model;

    }


    protected function performAjaxValidation($model)

    {

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'usuario-form') {

            echo CActiveForm::validate($model);

            Yii::app()->end();

        }

    }


    public function actionExcluir()
    {

        $id = $_POST['id'];

        $model = $this->loadModel($id);

        $log = new EleitorLog();

        $log->cpf = $model->cpf;

        $log->administrador = Yii::app()->user->id;

        if ($log->save(false) && $model->delete()) {

            echo CJSON::encode(array('resultado' => 1));

        } else {

            echo CJSON::encode(array('resultado' => 0));

        }

    }


}

