<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    private $_id;
    public $empresa;

    const ERROR_PERMISSION = 3;
    const ERROR_INACTIVE = 4;
    const ERROR_PERFIL_INVALIDO = 5;

    /**
     * override the construct method in CUserIdentity because add field empresa
     * 
     */
    public function __construct($username,$password)
    {
        $this->username=$username;
        $this->password=$password;
    }

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate() {

        Yii::import('application.models.teste');

        $Usuario = Usuario::model()->findByPk($this->username);

        if ($Usuario === null || empty($Usuario)) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
            Yii::log('Usuário inválido!', 'info', Yii::app()->controller->id);
        } elseif (md5($this->password) !== TRIM($Usuario->senha)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
            Yii::log('Senha inválida!', 'info', Yii::app()->controller->id);      
        } elseif ($Usuario->status == "I"){
            $this->errorCode = self::ERROR_INACTIVE;
            Yii::log('Usuário Inativo!', 'info', Yii::app()->controller->id);
        } elseif (!in_array($Usuario->perfil, array("A","C","L"))){
            $this->errorCode = self::ERROR_PERFIL_INVALIDO;
            Yii::log('Usuário com Perfil Inválido!', 'info', Yii::app()->controller->id);
        } else {
            

            $this->_id = $Usuario->cpf;
            $this->setState("login", $Usuario->cpf);
            $this->setState("usuario", $Usuario->nome);
            $this->setState('email', $Usuario->email);
            $this->setState("status", $Usuario->status);
            $this->setState("perfil", $Usuario->perfil);
            $this->setState("coordenador", $Usuario->coordenador);
            $this->setState("data_login", date("d/m/Y H:i:s"));
            $this->setState("data_login_strtotime",  strtotime(date("d/m/Y H:i:s")));


            //definindo campos dos filtros

            $filtro_eleitor  = array('coordenador' => '', 'lider' => '', 'engajamento' => '', 'bairro' => '', 'codigos' => array(), 'filtro_ativo' => 0);
            $filtro_usuario  = array('nome' => '', 'cpf' => '', 'perfil' => '', 'status' => '', 'filtro_ativo' => 0);

            $this->setState("filtro_eleitor", $filtro_eleitor);
            $this->setState("filtro_usuario", $filtro_usuario);


            $this->errorCode = self::ERROR_NONE;
            Yii::log('Usuário acessou o sitema -> ' . $this->username, 'info', Yii::app()->controller->id);

        }

        return !$this->errorCode;
    }

    public function getId() {
        return $this->_id;
    }

}
