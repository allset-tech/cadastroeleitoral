<?php

class Controller extends CController
{

    public $layout = '/layouts/main';
    public $menu        = array();
    public $breadcrumbs = array();

    public function init()
    {
        parent::init();

        if(empty(Yii::app()->user))
            $this->redirect(array('acesso/login'));

    }


    protected function beforeAction($action)
    {
        

        //verificar o tempo de sessao, encerrar após 5 minutos



        if ($this->id == 'site') {
            $this->pageTitle = Yii::app()->name;
        } else {
            if ($this->action->id == 'create') {
                $this->pageTitle = Yii::app()->name . " - " . ucwords($this->id) . '- Novo';
            } elseif ($this->action->id == 'view') {
                $this->pageTitle = Yii::app()->name . " - " . ucwords($this->id) . '- Visualizar ';
            } elseif ($this->action->id == 'index') {
                $this->pageTitle = Yii::app()->name . " - " . ucwords($this->id);
            } else {
                $this->pageTitle = Yii::app()->name . " - " . ucwords($this->id) . ' - ' . ucwords($this->action->id);
            }
        }
        return true;
    }

}
