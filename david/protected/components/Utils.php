<?php

class Utils {

    public $formatter;

    public static function enviaEmail($msg, $endereco, $titulo) {
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->IsSMTP();
        $mail->SMTPKeepAlive = true;
        $mail->Mailer = 'smtp';
        $mail->SMTPDebug = 1;  // Debugar: 1 = erros e mensagens, 2 = mensagens apenas
        $mail->SMTPAuth = true;  // Autenticação ativada
        $mail->SMTPSecure = 'ssl'; // SSL REQUERIDO pelo GMail
        $mail->Host = 'smtp.gmail.com'; // SMTP utilizado
        $mail->Port = 465;    // A porta 587 deverá estar aberta em seu servidor
        $mail->CharSet = "UTF-8";
        $mail->Username = 't@gmail.com';
        $mail->Password = 'm@01219#p@ss#gmail';
        $mail->SetFrom('t@gmail.com', 'Gerência de Tecnologia');
        $mail->Subject = $titulo;
        $mail->MsgHTML($msg);
        $mail->AddAddress($endereco);

        $mail->Send();
    }

    /**
     *  SALVA O ARQUIVO NO DIRETORIO UPLOAD
     */
    public static function salvaArquivo($modelo, $atributo, $id, $prefixo) {
        //pegando a extensao
        $extensao = '.' . substr(strrchr($_FILES[$modelo]['name'][$atributo], "."), 1);

        //controe nome do arquivo
        if ($modelo == 'Tipopauta' or $modelo == 'Edital')
            $nome = $prefixo . $id . $extensao;

        //salva arquivo
        if (move_uploaded_file($_FILES[$modelo]['tmp_name'][$atributo], 'upload/' . $nome))
            return $nome;
        else
            throw new CHttpException(404, 'Envio de arquivo falhou!');
    }

    /** Endode parametros GET */
    public static function encodeGET($data) {
        return base64_encode(base64_encode(base64_encode(base64_encode(base64_encode($data)))));
    }

    /** Dedode parametros GET */
    public static function decodeGET($data) {
        return base64_decode(base64_decode(base64_decode(base64_decode(base64_decode($data)))));
    }

    /**
     *  Converte as datasdo formato americano para brasileiro e vice-versa
     */
    public static function converte($data, $lang = 'ing') {

        switch ($lang) {
            case 'ing' :
                return date('Y-m-d', strtotime(str_replace('/', '-', $data)));
                break;
            case 'pt' :
                return date('d/m/Y', strtotime(str_replace('-', '/', $data)));
                break;
        }
    }

    /**
     *  Converte as datasdo formato americano para brasileiro e vice-versa
     */
    public static function converteMoeda($valor, $lang = 'ing') {
        switch ($lang) {
            case 'ing' :
                $novoValor = str_replace('.', '', $valor);
                $novoValor = str_replace(',', '.', $novoValor);
                return $novoValor;
                break;
            case 'pt' :
                $formatter = new NumberFormatter('pt_BR', NumberFormatter::CURRENCY);
                $novoValor = $formatter->formatCurrency($valor, 'BRL');
                //$aux = explode('R$ ', $valor); 
                //$novoValor = str_replace('.', '', $valor);
                return $novoValor;
                break;
            /* default:
              $novoValor = str_replace('.', '', $valor);
              $novoValor = str_replace(',', '.', $novoValor);
              return $novoValor; */
        }
    }

    /* ############################################# */
    /* ## Força o download no browser do cliente  ## */
    /* ############################################# */

    public static function downloadArquivo($nomeArquivo) {
        $arquivo = getcwd() . '/upload/' . $nomeArquivo;

        header("Content-Type: application/octet-stream"); //generico, qualquer arquivo por stream
        header('Cache-Control: no-cache, no-store, max-age=0, must-revalidate');
        header('Pragma: no-cache');
        header('Expires: 0');
        header("Content-Length: " . filesize($arquivo));
        header("Content-Disposition: attachment; filename=" . basename($arquivo));

        readfile($arquivo);
    }

    
       

    /**
     * Recebe 2 datas no formato brasileiro (dd/mm/yyyy) e calcula a quantidade
     * de dias entre as mesmas
     *
     * @param string $data1
     * @param string $data2
     * @return int
     */
    public function calcularQuantidadeDiasEntreDuasDatas($data1, $data2) {
        $partesData1 = explode('/', $data1);
        $data1 = $partesData1['2'] . '-' . $partesData1['1'] . '-' . $partesData1['0'];
        $partesData2 = explode('/', $data2);
        $data2 = $partesData2['2'] . '-' . $partesData2['1'] . '-' . $partesData2['0'];


        // Usa a funÃ§Ã£o strtotime() e pega o timestamp das duas datas:
        $time_inicial = strtotime($data1);
        $time_final = strtotime($data2);

        // Calcula a diferenÃ§a de segundos entre as duas datas:
        $diferenca = $time_final - $time_inicial; // 19522800 segundos
        // Calcula a diferenÃ§a de dias
        $dias = (int) floor($diferenca / (60 * 60 * 24)); // 225 dias
        // Exibe uma mensagem de resultado:
        return $dias;
    }

    public function formatMoeda($value) {
        $formatter = new NumberFormatter('pt_Br', NumberFormatter::CURRENCY);

        return str_replace("R$", "", $formatter->format($value));
    }

   
}
