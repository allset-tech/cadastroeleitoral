<?php

class Model extends CActiveRecord {

    public function init() {
        
    }

	public function onException() {
		echo "erro";
		exit;
	}
	
    public function afterSave() {
        parent::afterSave();
        if (@Yii::app()->controller->action->id == "create") {
            @Yii::log('Registro criado!', 'info', Yii::app()->controller->id);
        } else {
            @Yii::log('ID: ' . $_GET['id'], 'info', Yii::app()->controller->id);
        }
    }

    public function afterDelete() {
        parent::afterDelete();
        @Yii::log('ID: ' . $_GET['id'], 'info', Yii::app()->controller->id);
    }

}