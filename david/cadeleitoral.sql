-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.37-MariaDB


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema cadastro_eleitoral
--

CREATE DATABASE IF NOT EXISTS cadastro_eleitoral;
USE cadastro_eleitoral;

--
-- Temporary table structure for view `vw_coordenador`
--
DROP TABLE IF EXISTS `vw_coordenador`;
DROP VIEW IF EXISTS `vw_coordenador`;
CREATE TABLE `vw_coordenador` (
  `cpf` varchar(14),
  `nome` varchar(200),
  `engajamento` varchar(3),
  `endereco` varchar(100),
  `codigo` mediumtext,
  `sexo` varchar(1),
  `profissao` varchar(45),
  `lider` varchar(200),
  `coordenador` varchar(14),
  `data_cadastro` timestamp
);

--
-- Temporary table structure for view `vw_eleitor`
--
DROP TABLE IF EXISTS `vw_eleitor`;
DROP VIEW IF EXISTS `vw_eleitor`;
CREATE TABLE `vw_eleitor` (
  `cpf` varchar(14),
  `nome` varchar(200),
  `rg` varchar(15),
  `codigo` mediumtext,
  `data_nasc` date,
  `sexo` varchar(9),
  `nome_mae` varchar(200),
  `wathsapp` varchar(14),
  `telefone` varchar(14),
  `email` varchar(100),
  `coordenador_cpf` varchar(14),
  `coordenador` varchar(200),
  `lider_cpf` varchar(14),
  `lider` varchar(200),
  `data_visita` date,
  `hora_visita` varchar(5),
  `indicado_por` varchar(120),
  `facebook` varchar(250),
  `instagram` varchar(250),
  `engajamento` varchar(8),
  `endereco` varchar(100),
  `numero` varchar(8),
  `estado` varchar(2),
  `municipio` varchar(30),
  `bairro` varchar(45),
  `cep` varchar(9),
  `zona` varchar(2),
  `secao` varchar(3),
  `numero_pesssoas_familia` varchar(3),
  `lider_comunitario` varchar(3),
  `tipo_residencia` varchar(7),
  `possui_veiculo` varchar(3),
  `tipo_veiculo` varchar(30),
  `titulo_eleitor` varchar(30),
  `situacao_titulo` varchar(8),
  `estimativa_votos` int(8),
  `data_cadastro` timestamp,
  `zona_moradia` varchar(5),
  `profissao` varchar(45),
  `local_votacao` varchar(45)
);

--
-- Temporary table structure for view `vw_usuario`
--
DROP TABLE IF EXISTS `vw_usuario`;
DROP VIEW IF EXISTS `vw_usuario`;
CREATE TABLE `vw_usuario` (
  `cpf` varchar(14),
  `nome` varchar(200),
  `perfil` varchar(13),
  `status` varchar(7),
  `coordenador` varchar(14),
  `data_cadastro` timestamp
);

--
-- Definition of table `tb_eleitor`
--

DROP TABLE IF EXISTS `tb_eleitor`;
CREATE TABLE `tb_eleitor` (
  `cpf` varchar(14) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `rg` varchar(15) NOT NULL,
  `codigo` text NOT NULL COMMENT 'de 01 a 15',
  `data_nasc` date NOT NULL,
  `sexo` varchar(1) DEFAULT NULL COMMENT 'M = Masculino, F = Feminino, O = Outros',
  `nome_mae` varchar(200) NOT NULL,
  `wathsapp` varchar(14) NOT NULL,
  `telefone` varchar(14) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `coordenador` varchar(14) NOT NULL,
  `lider` varchar(14) NOT NULL COMMENT 'Pegar o usuario da sessao',
  `data_visita` date NOT NULL,
  `hora_visita` varchar(5) NOT NULL COMMENT 'HH:MM',
  `indicado_por` varchar(120) NOT NULL,
  `facebook` varchar(250) DEFAULT NULL,
  `instagram` varchar(250) DEFAULT NULL,
  `engajamento` varchar(3) DEFAULT NULL COMMENT 'POS = Positivo, NEG = Negativo, NEU = Neutro',
  `endereco` varchar(100) NOT NULL,
  `numero` varchar(8) NOT NULL,
  `estado` varchar(2) NOT NULL,
  `municipio` varchar(30) NOT NULL,
  `bairro` varchar(45) NOT NULL,
  `cep` varchar(9) NOT NULL,
  `zona` varchar(2) NOT NULL,
  `secao` varchar(3) NOT NULL,
  `numero_pesssoas_familia` varchar(3) DEFAULT NULL COMMENT 'Numero de Pessoas na Familia',
  `lider_comunitario` varchar(1) DEFAULT NULL COMMENT 'S = Sim, N = Nao',
  `tipo_residencia` varchar(1) DEFAULT NULL COMMENT 'P=Própria/ A=Alugada/ C=Cedida',
  `possui_veiculo` varchar(1) DEFAULT NULL COMMENT 'S = Sim, N = Nao',
  `tipo_veiculo` varchar(30) DEFAULT NULL,
  `titulo_eleitor` varchar(30) NOT NULL,
  `situacao_titulo` varchar(1) NOT NULL COMMENT 'R = Regular / P = Pendente',
  `estimativa_votos` int(8) NOT NULL,
  `data_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'CURRENT_DATE',
  `zona_moradia` varchar(1) NOT NULL COMMENT 'L = LESTE, O = OESTE, N = NORTE, S = SUL',
  `profissao` varchar(45) DEFAULT NULL,
  `local_votacao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`cpf`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cadastro de eleitores';

--
-- Dumping data for table `tb_eleitor`
--

/*!40000 ALTER TABLE `tb_eleitor` DISABLE KEYS */;
INSERT INTO `tb_eleitor` (`cpf`,`nome`,`rg`,`codigo`,`data_nasc`,`sexo`,`nome_mae`,`wathsapp`,`telefone`,`email`,`coordenador`,`lider`,`data_visita`,`hora_visita`,`indicado_por`,`facebook`,`instagram`,`engajamento`,`endereco`,`numero`,`estado`,`municipio`,`bairro`,`cep`,`zona`,`secao`,`numero_pesssoas_familia`,`lider_comunitario`,`tipo_residencia`,`possui_veiculo`,`tipo_veiculo`,`titulo_eleitor`,`situacao_titulo`,`estimativa_votos`,`data_cadastro`,`zona_moradia`,`profissao`,`local_votacao`) VALUES 
 ('473.990.960-07','Eleitor 5 Cad Admin','98765466','06#07#09','1958-08-15','F','Francisca Maria','(92)98181-8181','(92)98181-8181','email123@email.com','746.067.782-53','746.067.782-53','2021-10-28','09:45','Marilia','https://pt-br.facebook.com/doacaocongasfake/','https://pt-br.facebook.com/doacaocongasfake/','NEG','Rua Madri','150','AM','Manaus','Planalto','69045-150','62','80','3','N','A','N','','5544648794654','R',3,'2021-11-04 19:46:56','N','','EE Vicente Telles'),
 ('789.456.456-56','Eleitor 4 Cad Admin','654654987','12#13#14#15','1991-04-25','F','Maria do Carmo','(92)92929-2922','(92)98124-2020','mcar@mail.com','746.067.782-53','746.067.782-53','2021-10-25','09:30','Marcos dos Santos','','','','Conjunto Huascar Angelin','4566','AM','Manaus','Aleixo','69060-200','56','54','','','','','','456456','R',5,'2021-11-04 19:46:56','O','','EE Vicente Telles'),
 ('789.465.321-32','Eleitor 3 Cad Admin','789456654','05#06#07','2021-10-28','','Maria da Silva','(92)92292-9292','(92)98102-2121','email123@email.com','746.067.782-53','746.067.782-53','2021-10-28','09:20','fatima','','','','Travessa Maromba','987','AM','Manaus','Chapada','69050-150','89','56','','','','','','987987','R',2,'2021-11-04 19:46:56','L','','EE Vicente Telles'),
 ('807.879.980-28','Eleitor 2 Cad Admin','12345','45','1978-04-16','F','Francisca Aparecida da Silva','(92)98202-5678','(92)98181-8181','re123@bol.com','746.067.782-53','746.067.782-53','2021-10-25','09:30','Fernando','https://pt-br.facebook.com/doacaocongasfake/','https://pt-br.facebook.com/doacaocongasfake/','POS','Rua Madri','150','AM','Manaus','Planalto','69045-150','65','78','3','S','P','S','Honda Civic','654654654654','R',20,'2021-11-04 19:46:56','O',NULL,NULL),
 ('961.038.800-09','Eleitor 1 Cad Admin','456462321','12','1998-10-10','F','Maria do Ceu','(92)98181-8181','(92)98181-8181','email123@email.com','746.067.782-53','746.067.782-53','2021-10-20','08:20','Marcio dos Santos','https://pt-br.facebook.com/doacaocongasfake/','https://pt-br.facebook.com/doacaocongasfake/','NEU','Travessa Maromba','6543','AM','Manaus','Chapada','69050-150','58','74','3','N','P','S','Corsa Sedan 2020','654654654654','R',5,'2021-11-04 19:46:56','S',NULL,NULL),
 ('987.654.654-65','Eleitor 2 Lider 1 C3','654654654','02#04#05#06#07','1979-08-25','M','Zenaide da Silva','(92)92929-2929','(92)92929-2929','','320.086.580-66','111.111.111-11','2021-10-30','10:30','janio','','','POS','Beco Rio Negro','6546','AM','Manaus','Educandos','69070-150','54','59','3','S','A','N','','9879879','R',5,'2021-11-04 19:46:56','S','Analista','EE Solon de Lucena'),
 ('987.987.987-65','Eleitor 1 Lider 1 C3','654654656','10#14#15','1993-11-17','F','Fatima do Carmo','(29)29292-9299','(92)92929-2929','re123@bol.com','320.086.580-66','111.111.111-11','2021-11-28','20:35','Josemara','','','NEU','150 Rua Madri','9879','AM','Manaus','Planalto','69045-150','65','59','8','S','P','N','','65465','P',20,'2021-11-04 19:46:56','O','Diarista','EE Solon de Lucena');
/*!40000 ALTER TABLE `tb_eleitor` ENABLE KEYS */;


--
-- Definition of table `tb_usuario`
--

DROP TABLE IF EXISTS `tb_usuario`;
CREATE TABLE `tb_usuario` (
  `cpf` varchar(14) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `data_nasc` date NOT NULL,
  `sexo` varchar(1) NOT NULL COMMENT 'M = masculino, F = Feminino, O = Outros',
  `telefone` varchar(14) NOT NULL,
  `endereco` varchar(100) NOT NULL,
  `numero` varchar(8) NOT NULL,
  `estado` varchar(2) NOT NULL,
  `municipio` varchar(30) NOT NULL,
  `bairro` varchar(45) NOT NULL,
  `cep` varchar(9) NOT NULL,
  `email` varchar(100) NOT NULL,
  `perfil` varchar(1) NOT NULL COMMENT 'A = admin, C = Coordenador, L = Lider',
  `status` varchar(1) NOT NULL COMMENT 'A = ativo, I = inativo',
  `senha` text NOT NULL,
  `coordenador` varchar(14) DEFAULT NULL,
  `data_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'CURRENT_DATE',
  PRIMARY KEY (`cpf`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cadastro de Usuários';

--
-- Dumping data for table `tb_usuario`
--

/*!40000 ALTER TABLE `tb_usuario` DISABLE KEYS */;
INSERT INTO `tb_usuario` (`cpf`,`nome`,`data_nasc`,`sexo`,`telefone`,`endereco`,`numero`,`estado`,`municipio`,`bairro`,`cep`,`email`,`perfil`,`status`,`senha`,`coordenador`,`data_cadastro`) VALUES 
 ('111.111.111-11','Lider 1 Coord 3','1978-05-26','F','(92)92929-2929','Travessa Luís Mendes','654','AM','Manaus','Nossa Senhora das Graças','69057-150','email123@bol.com','L','A','e10adc3949ba59abbe56e057f20f883e','320.086.580-66','2021-11-04 19:41:32'),
 ('167.616.530-45','Rhullyana (admin)','1989-06-15','F','(92)98181-8181','Rua Francisco Duarte','4566','AM','Manaus','Japiim','69078-150','rhully@email.com','A','A','e10adc3949ba59abbe56e057f20f883e','746.067.782-53','2021-11-04 19:36:17'),
 ('320.086.580-66','Coordenador 3','1975-12-26','M','(92)98104-2552','Rua Barão de Alencar','545','AM','Manaus','Flores','69058-150','email@email.com','C','A','e10adc3949ba59abbe56e057f20f883e','746.067.782-53','2021-10-24 22:29:14'),
 ('568.310.950-70','Lider 1 Coord 1','1975-05-15','F','(92)98124-2020','Conjunto Huascar Angelin','6521','AM','Manaus','Aleixo','69060-200','mcar@mail.com','L','I','e10adc3949ba59abbe56e057f20f883e','692.181.270-71','2021-11-04 19:41:32'),
 ('646.913.450-51','Coordenador 2','1969-12-31','F','(92)99144-2050','Beco Irara','654','AM','Manaus','Compensa','69035-150','mfer@bol.com','C','A','e10adc3949ba59abbe56e057f20f883e','','2021-11-04 19:36:49'),
 ('692.181.270-71','Coordenador 1','1999-05-15','F','(92)99151-5050','Rua Lisboa','9878','AM','Manaus','Planalto','69045-120','email@bol.com','C','A','e10adc3949ba59abbe56e057f20f883e','','2021-11-04 19:36:49'),
 ('746.067.782-53','João (admin)','1983-10-02','M','(92)98274-8146','Rua Paraopeba','135','AM','Manaus','Flores','69028-388','joaobrasil@gmail.com','A','A','e10adc3949ba59abbe56e057f20f883e','','2021-11-04 19:36:17'),
 ('775.141.150-30','Lider 2 Coord 1','1983-04-06','M','(92)81402-021','Travessa Maromba','6543','AM','Manaus','Chapada','69050-150','email@wmail.com','L','A','e10adc3949ba59abbe56e057f20f883e','692.181.270-71','2021-11-04 19:41:32'),
 ('904.779.910-09','Lider 1 Coord 2','1982-06-02','M','(92)99105-8180','Rua Rio Amapá','500','AM','Manaus','Nossa Senhora das Graças','69053-150','email0201@email.com','L','A','e10adc3949ba59abbe56e057f20f883e','646.913.450-51','2021-11-04 19:41:32'),
 ('971.329.670-29','Lider 2 Coord 2','1969-12-31','F','(92)99202-8448','Rua Alegrete','1200','AM','Manaus','Santa Etelvina','69059-100','jef123@bol.com','L','A','e10adc3949ba59abbe56e057f20f883e','646.913.450-51','2021-11-04 19:41:32');
/*!40000 ALTER TABLE `tb_usuario` ENABLE KEYS */;


--
-- Definition of view `vw_coordenador`
--

DROP TABLE IF EXISTS `vw_coordenador`;
DROP VIEW IF EXISTS `vw_coordenador`;
CREATE ALGORITHM=UNDEFINED DEFINER=`admin`@`localhost` SQL SECURITY DEFINER VIEW `vw_coordenador` AS select `e`.`cpf` AS `cpf`,`e`.`nome` AS `nome`,`e`.`engajamento` AS `engajamento`,`e`.`endereco` AS `endereco`,replace(`e`.`codigo`,'#',',') AS `codigo`,`e`.`sexo` AS `sexo`,`e`.`profissao` AS `profissao`,`u`.`nome` AS `lider`,`e`.`coordenador` AS `coordenador`,`e`.`data_cadastro` AS `data_cadastro` from (`tb_eleitor` `e` join `tb_usuario` `u`) where (`e`.`lider` = `u`.`cpf`);

--
-- Definition of view `vw_eleitor`
--

DROP TABLE IF EXISTS `vw_eleitor`;
DROP VIEW IF EXISTS `vw_eleitor`;
CREATE ALGORITHM=UNDEFINED DEFINER=`admin`@`localhost` SQL SECURITY DEFINER VIEW `vw_eleitor` AS select `e`.`cpf` AS `cpf`,`e`.`nome` AS `nome`,`e`.`rg` AS `rg`,replace(`e`.`codigo`,'#',',') AS `codigo`,`e`.`data_nasc` AS `data_nasc`,(case when (`e`.`sexo` = 'F') then 'Feminino' when (`e`.`sexo` = 'M') then 'Masculino' else 'Outros' end) AS `sexo`,`e`.`nome_mae` AS `nome_mae`,`e`.`wathsapp` AS `wathsapp`,`e`.`telefone` AS `telefone`,`e`.`email` AS `email`,`e`.`coordenador` AS `coordenador_cpf`,`c`.`nome` AS `coordenador`,`e`.`lider` AS `lider_cpf`,`l`.`nome` AS `lider`,`e`.`data_visita` AS `data_visita`,`e`.`hora_visita` AS `hora_visita`,`e`.`indicado_por` AS `indicado_por`,`e`.`facebook` AS `facebook`,`e`.`instagram` AS `instagram`,(case when (`e`.`engajamento` = 'NEG') then 'Negativo' when (`e`.`engajamento` = 'POS') then 'Positivo' else 'Neutro' end) AS `engajamento`,`e`.`endereco` AS `endereco`,`e`.`numero` AS `numero`,`e`.`estado` AS `estado`,`e`.`municipio` AS `municipio`,`e`.`bairro` AS `bairro`,`e`.`cep` AS `cep`,`e`.`zona` AS `zona`,`e`.`secao` AS `secao`,`e`.`numero_pesssoas_familia` AS `numero_pesssoas_familia`,(case when (`e`.`lider_comunitario` = 'S') then 'Sim' else 'Não' end) AS `lider_comunitario`,(case when (`e`.`tipo_residencia` = 'P') then 'Própria' when (`e`.`tipo_residencia` = 'A') then 'Alugada' else 'Cedida' end) AS `tipo_residencia`,(case when (`e`.`possui_veiculo` = 'S') then 'Sim' else 'Não' end) AS `possui_veiculo`,`e`.`tipo_veiculo` AS `tipo_veiculo`,`e`.`titulo_eleitor` AS `titulo_eleitor`,(case when (`e`.`situacao_titulo` = 'R') then 'Regular' else 'Pendente' end) AS `situacao_titulo`,`e`.`estimativa_votos` AS `estimativa_votos`,`e`.`data_cadastro` AS `data_cadastro`,(case when (`e`.`zona_moradia` = 'O') then 'Oeste' when (`e`.`zona_moradia` = 'L') then 'Leste' when (`e`.`zona_moradia` = 'N') then 'Norte' else 'Sul' end) AS `zona_moradia`,`e`.`profissao` AS `profissao`,`e`.`local_votacao` AS `local_votacao` from ((`tb_eleitor` `e` join `tb_usuario` `c` on((`c`.`cpf` = `e`.`coordenador`))) join `tb_usuario` `l` on((`l`.`cpf` = `e`.`lider`)));

--
-- Definition of view `vw_usuario`
--

DROP TABLE IF EXISTS `vw_usuario`;
DROP VIEW IF EXISTS `vw_usuario`;
CREATE ALGORITHM=UNDEFINED DEFINER=`admin`@`localhost` SQL SECURITY DEFINER VIEW `vw_usuario` AS select `tb_usuario`.`cpf` AS `cpf`,`tb_usuario`.`nome` AS `nome`,(case when (`tb_usuario`.`perfil` = 'A') then 'Administrador' when (`tb_usuario`.`perfil` = 'C') then 'Coordenador' else 'Líder' end) AS `perfil`,(case when (`tb_usuario`.`status` = 'A') then 'Ativo' else 'Inativo' end) AS `status`,`tb_usuario`.`coordenador` AS `coordenador`,`tb_usuario`.`data_cadastro` AS `data_cadastro` from `tb_usuario`;



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
