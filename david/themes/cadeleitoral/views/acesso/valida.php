<div class="container">
    <div class="content">
        <div class="row-fluid">
            <div class="login-form">
                <h1 class="text-center">
                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo-icon.png" width="50" height="50" /><br />
                </h1>
                <form action="<?php Yii::app()->urlManager->createUrl('/acesso/valida', array('id'=>$id)); ?>" method="post">
                    <fieldset>
                        <div class="input-prepend clearfix">
                            <span class="add-on"><i class="fa fa-key"></i></span>
                            <input type="password" placeholder="Entre com sua nova senha" name="new_pswd"/>
                        </div><br/>
                        <div class="input-prepend clearfix">
                            <span class="add-on"><i class="fa fa-key"></i></span>
                            <input type="password" placeholder="Confirme sua nova senha" name="confirm_pswd"/>
                        </div>
                        <button type="submit" class="btn btn-block btn-primary" >Redefinir Senha</button>
                        <a class="btn btn-block" href="<?php echo Yii::app()->baseUrl; ?>/acesso/alterar">Voltar</a>
                    </fieldset>
                </form>
            </div>
            <div class="row-fluid">
                <?php
                $flashMessages = Yii::app()->user->getFlashes();
                if ($flashMessages):
                    foreach ($flashMessages as $key => $message):
                        ?>
                        <div class="alert alert-<?php echo $key; ?> text-center">
                            <?php echo $message; ?>
                        </div>
                        <?php
                    endforeach;
                endif;
                ?>
            </div>
        </div>
    </div> 
</div>