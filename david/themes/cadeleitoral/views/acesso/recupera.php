<div class="container">
    <div class="content">
        <div class="row-fluid">
            <div class="login-form">
                <h1 class="text-center">
                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo-icon.png" width="50" height="50" /><br />
                </h1>
                <form action="<?php echo Yii::app()->baseUrl; ?>/acesso/recupera" method="post"
                      id="recuperar-form">
                    <fieldset>
                        <div class="input-prepend clearfix">
                            <span class="add-on"><i class="fa fa-user"></i></span>
                            <input type="text" placeholder="Entre com seu Login" name="usua_login"
                                   id="username">
                        </div><br/>
                        <div class="input-prepend clearfix">
                            <span class="add-on"><i class="fa fa-key"></i></span>
                            <input type="text" placeholder="Entre com seu CPF" name="usua_cpf"
                                   id="cpf_user">
                        </div>
                        <button type="submit" class="btn btn-block btn-primary">Validar Usuário</button>
                        <a class="btn btn-block" href="<?php echo Yii::app()->baseUrl; ?>/acesso/index">Voltar</a>
                    </fieldset>
                </form>
            </div>
            <div class="row-fluid" id="msg-box">
                <?php
                $flashMessages = Yii::app()->user->getFlashes();
                if($flashMessages):
                    foreach ($flashMessages as $key => $message):
                        ?>
                        <div class="alert alert-<?php echo $key; ?> text-center" id="msg">
                            <?php echo $message; ?>
                        </div>
                        <?php
                    endforeach;
                endif;
                ?>
            </div>
        </div>
    </div> 
</div>						