<div class="titleValidaTroca">
    <i class="fa fa-key"></i> Trocar Senha
</div>
<?php $form = $this->beginWidget('CActiveForm', array(
    'id'=>'form_altera',
    'enableAjaxValidation'=>false,
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>
<div class="row-fluid">
    <div class="span12">
        <?php echo $form->labelEx($modelAltera, 'nova_senha'); ?>
        <?php echo $form->passwordField($modelAltera, 'nova_senha', array('placeholder'=>'Entre com sua nova senha')); ?>
        <?php echo $form->error($modelAltera, 'nova_senha'); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <?php echo $form->labelEx($modelAltera, 'confirm_senha'); ?>
        <?php echo $form->passwordField($modelAltera, 'confirm_senha', array('placeholder'=>'Confirme sua nova senha')); ?>
        <?php echo $form->error($modelAltera, 'confirm_senha'); ?>
    </div>
</div>
<div class="row-fluid">          
    <div class="span12">
        <?php echo CHtml::button('Redefinir Senha', array('class' => 'btnBlue pull-right', 'onclick' => 'alteraSenha()', 'id' => 'btn_altera_senha')) ?>
    </div> 
</div>
<?php echo $form->hiddenField($modelAltera, 'usuario_id'); ?>
<?php $this->endWidget(); ?>