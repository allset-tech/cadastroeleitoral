<div id="areaLogin">
  <div class="areaCarregando" style="display:none">
    <div class="imgCarregando">
      <img  src = "<?php echo Yii::app()->theme->baseUrl . '/img/carregando.gif'; ?>" id= "gifCarregando"   />
      <br/>
      <span>Aguarde...</span>
    </div>            
    <div class="opacityCarregando"></div>
  </div>    
  <div id="areaAlertaLogin">
    <?php
    $flashMessages = Yii::app()->user->getFlashes();
    if ($flashMessages):
      foreach ($flashMessages as $key => $message):
        ?>           
        <div id="mensagemAlerta" class="alert alert-<?php echo $key; ?>">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <i class="fa fa-warning"></i>
          <h4><?php echo $message; ?></h4>                        
        </div>
        <?php
      endforeach;
    endif;
    ?>
    <!-- Este alerta aparece somente quando é solicitado via AJAX -->
    <div class="alert" style="display: none;" id="mensagemAlertaAjax">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      <i></i>
      <h4></h4>
      <span></span>
    </div>
  </div>
  <!-- Área Logo -->
  <div class="pull-left logo">      
    <img  src="<?php echo Yii::app()->theme->baseUrl; ?>/img/gpli_logo.png" width="120" height="141" />
  </div>        
  <div class="pull-right formLogin">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'form_primeiro_acesso',
        'action' => CHtml::normalizeUrl(array('primeiroAcesso')),
            //'action' => 'primeiroAcesso',
    ));
    ?>
    <div class="row-fluid">
      <div class="span12">
        <?php echo $form->labelEx($modelPrimeiroAcesso, 'username'); ?>
        <?php
        echo $form->textField($modelPrimeiroAcesso, 'username', array(
            'placeholder' => 'Entre com seu usuário enviado pelo email', 'readOnly' => 'readOnly'));
        ?>
        <?php echo $form->error($modelPrimeiroAcesso, 'username'); ?>
      </div>
    </div>
    <div class="row-fluid">
      <div class="span12">
        <?php echo $form->labelEx($modelPrimeiroAcesso, 'password'); ?>
        <?php
        echo $form->passwordField($modelPrimeiroAcesso, 'password', array(
            'placeholder' => 'Entre com sua senha enviada pelo email'));
        ?>
        <?php echo $form->error($modelPrimeiroAcesso, 'password'); ?>
      </div>
    </div>
    <div class="row-fluid">
      <div class="span12">
        <?php echo $form->labelEx($modelPrimeiroAcesso, 'nova_senha'); ?>
        <?php
        echo $form->passwordField($modelPrimeiroAcesso, 'nova_senha', array(
            'placeholder' => 'Entre com sua nova senha'));
        ?>
        <?php echo $form->error($modelPrimeiroAcesso, 'nova_senha'); ?>
      </div>
    </div>
    <div class="row-fluid">
      <div class="span12">
        <?php echo $form->labelEx($modelPrimeiroAcesso, 'confirm_senha'); ?>
        <?php
        echo $form->passwordField($modelPrimeiroAcesso, 'confirm_senha', array(
            'placeholder' => 'Confirme sua nova senha'));
        ?>
        <?php echo $form->error($modelPrimeiroAcesso, 'confirm_senha'); ?>
      </div>
    </div>
    <div class="row-fluid">          
      <div class="span12">
        <?php
        echo CHtml::submitButton('Acessar GPLI', array(
            'class' => 'btnBlue', 'id' => 'btn_altera_senha'))
        ?>
      </div> 
    </div>
    <?php $this->endWidget(); ?>
  </div>
</div>
  <script>
    jQuery("#PrimeiroAcessoForm_password").focus();
    jQuery('#btn_altera_senha').click(function() {
      jQuery('.areaCarregando').show();
    });
  </script>