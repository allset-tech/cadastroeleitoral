<script type="text/javascript">

    $().ready(function () {    

        $("#username").mask('999.999.999-99');

    });

</script>

<?php

 $form = $this->beginWidget('CActiveForm', array(

     'id'                     => 'form_login',

     'action'                 => $this->createUrl('/acesso/login'),

     'enableAjaxValidation'   => false,

     'enableClientValidation' => false,

 ));

 ?>
<div class="row" style="width:100%;background:#FFF">
      <div>
        <img src="<?php echo Yii::app()->theme->baseUrl;?>/img/logo.png" style="height:75px"/>
      </div>
</div>
<div class="row justify-content-center">
  <div class="col-xl-5 col-lg-6 col-md-5">
    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-12">
            <div class="col-lg-12 text-center">
							<?php
							$flashMessages = Yii::app()->user->getFlashes();
							if ($flashMessages):
							    foreach ($flashMessages as $key => $message):
							        ?>
							        <div class="alert alert-danger">
							            <span><b> <?php echo $message; ?> </b> <?php echo $form->errorSummary($model, null, null, null); ?></span>
							        </div>
							        <?php
							    endforeach;
							endif;
							?>
						</div>
            <div class="p-5">
              <div class="text-center">
                <h1 class="bemvindo">BEM VINDO!</h1>
              </div>
              <form class="user" >
                <div class="form-group" style="margin-top:50px">
                  <?php echo $form->textField($model, 'username', array('id' => 'login', 'placeholder' => 'CPF', 'class' => 'form-control form-control-user', 'id' => 'username')); ?>
                </div>

                <div class="form-group">
                  <!-- <input type="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password"> -->
                  <?php echo $form->passwordField($model, 'password', array('id' => 'senha', 'placeholder' => 'Senha', 'class' => 'form-control form-control-pass')); ?>
                </div>
                <?php echo CHtml::submitButton('Entrar', array('class' => 'btn btn-primary btn-user btn-block')); ?>
              </form>
              <hr>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row" style="width:100%;background:#FFF;height:30px; position: absolute;bottom: 0px;">
</div>
<?php $this->endWidget(); ?>