<script type="text/javascript">    
    $(document).ready(function(){
        $("#cpf").mask('999.999.999-99');
        $("#cpf").keyup(function(e){
            let cpf = $("#cpf").val();
            if (cpf.length == 14) {
                e.preventDefault();
                
                let meu_cpf     = '<?php echo @Yii::app()->user->id;?>'; 
                if(cpf <= 0){
                    sweetAlert('Oops...', 'Informe o campo CPF!', 'error');
                    return false;
                }
                $.ajax({
                    'url': '<?php echo Yii::app()->baseUrl; ?>/eleitores/pesquisarcpf',
                    'data': 'cpf='+cpf,
                    'type': 'post',
                    'dataType': 'json',
                    'success': function(data) {
                        if (data.status == 'Ok') {
                            //CPF encontrado
                            if(data.resultado == 'SIM'){
                                    //Eu sou o lider
                                    if(data.lider == meu_cpf){
                                        sweetAlert({
                                                    title: 'Deseja realizar alterações no cadastro?',
                                                    //showDenyButton: true,
                                                    showCancelButton: true,
                                                    confirmButtonText: 'Sim',
                                                    //denyButtonText: `Não`,
                                                }).then((result) => {
                                                if (result.value == true) {
                                                    //swal('Saved!', '', 'success')
                                                    window.location.href = '<?php echo Yii::app()->baseUrl; ?>/eleitores/visualizar/id/' + data.eleitor
                                                } else if (result.dismiss == 'cancel') {
                                                    //swal('Changes are not saved', '', 'info')
                                                    $("#cpf").val('');
                                                    $("#cpf").focus();
                                                }
                                                }); 
                                    } else {
                                        sweetAlert('Oops...', 'Este CPF já foi cadastrado por : ' + data.lider_nome, 'error');
                                        $("#cpf").val('');
                                        $("#cpf").focus();
                                    }
                            } else {
                                    sweetAlert('Oops...', 'CPF ainda não cadastrado!', 'error');
                                    setTimeout(function() {
                                        window.location.href = '<?php echo Yii::app()->baseUrl; ?>/eleitores/novo'
                                    }, 2000);
                            }
                            } else {
                                sweetAlert('Oops...', 'Erro:'+ data.detalhes, 'error');
                            }
                    },
                    'cache': false,
                });
            }
        });
    
    
    $("#cpf_consulta").mask('999.999.999-99');
        $("#cpf_consulta").keyup(function(e){
            let cpf = $("#cpf_consulta").val();
            if (cpf.length == 14) {
                e.preventDefault();
                if(cpf <= 0){
                    sweetAlert('Oops...', 'Informe o campo CPF!', 'error');
                    return false;
                }
                window.location.href = '<?php echo Yii::app()->baseUrl; ?>/eleitores/index?cpf='+cpf;
            }
        });
    });      
</script>

<div class="col-xl-12">
<!-- Área de Erros  -->
<?php
$flashMessages = Yii::app()->user->getFlashes();
if ($flashMessages) {
    foreach ($flashMessages as $key => $message) {
        ?>
        <div class="alert alert-<?php echo ($key == 'error') ? 'danger' : $key; ?>">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <i class="fa fa-warning"></i>
            <h4><?php echo $message; ?></h4>
            <?php echo $form->errorSummary($model, null, null, null); ?>
        </div>
        <?php
    }
}
?>
</div>

<div class="container" style="min-height: 100%;font-">
    <div class="row text-center">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 Services-tab item" >
        <h2 class="ola-text" >Olá !</h2>
    </div>
</div>
</div>
    <div class="row text-center">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 Services-tab item text-center ">
            <div style="background:#0899A1;padding:15px;margin:15px;border-radius: 10px;display: inline-block;text-align:left" class=" home-card" >   
                <div style="display:inline-flex;" class=" row">
                    <div class="ico-new-eleitor" >
                    </div>
                    <div class="ico-new-eleitor-text">
                        Cadastrar Indivíduo
                    </div>
                </div>
                <br/>
                <div style="display:inline-flex;width: 100%;margin-top:10px" class="row">
                    <form style="width:100%">
                        <input type="text" class="form-control pesq-input" id="cpf" value="" placeholder="999.999.999-99"/>
                    </form>
                </div>
            </div>
            <br/>
            <div style="background:#0899A1;padding:15px;margin:15px;border-radius: 10px;display: inline-block;text-align:left" class=" home-card" >   
                <div style="display:inline-flex;" class=" row">
                    <div class="ico-lista-eleitor" >
                    </div>
                    <div class="ico-new-eleitor-text">
                        Consultar Cadastro
                    </div>
                </div>
                <br/>
                <div style="display:inline-flex;width: 100%;margin-top:10px" class="row">
                    <form style="width:100%">
                        <input type="text" class="form-control pesq-input" id="cpf_consulta" value="" placeholder="999.999.999-99"/>
                    </form>
                </div>
            </div>
            <br/>
            <div style="background:#0899A1;padding:15px;margin:15px;border-radius: 10px;display: inline-block;text-align:left" class=" home-card" >   
                <div style="display:inline-flex;" class=" row">
                    <div class="ico-lista-engajamento" >
                    </div>
                    <div class="ico-new-eleitor-text">
                        Atualizar Engajamento
                    </div>
                </div>
                <br/>
                <div style="display:inline-flex;width: 100%;margin-top:10px" class="row">
                    <form method="POST" enctype="multipart/form-data">
                        <input type="file" style="color:#FFF" name="csv_file" id="csv_file"/></td><td>
                            <input type="submit" class="btn-success" name="SubmitButton" value="Atualizar"/>
                    </form>
                </div>
            </div>
            <br/><br/><br/>
            <!-- <div style="background:#0899A1;padding:15px;margin:15px;border-radius: 10px;display: inline-block;" class="text-center home-card" >   
                <a href="<?php echo Yii::app()->urlManager->createUrl('eleitores/pesquisar'); ?>">
                    <i class="far fa-address-card fa-5x fa-icon-image"></i>
                    <h3 class="item-title">
                        Adicionar Cadastro
                    </h3>
                </a>
            </div> -->
        </div>
    </div>
</div>

<?php 
    if(isset($_POST['SubmitButton'])){ //check if form was submitted
        if ($_FILES["csv_file"]["error"] > 0)
        {
            echo "<script type='text/javascript'>alert('Selecione um arquivo válido para importação.');</script>";
        }
        else
        {
            if( $_FILES["csv_file"]["type"] === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                if ($xlsx = SimpleXLSX::parse($_FILES['csv_file']['tmp_name'])) {
                    foreach ($xlsx->readRows() as $k => $r) {
                        $cpf = $r[ 0 ];
                        $engaj = $r[ 1 ];
                        if ($engaj === 'Positivo'|| $engaj === 'POSITIVO') {
                            $engaj = 'POS';
                        }
                        if ($engaj === 'Negativo' || $engaj === 'NEGATIVO') {
                            $engaj = 'NEG';
                        }
                        if ($engaj === 'Neutro' || $engaj === 'NEUTRO') {
                            $engaj = 'NEU';
                        }
                        // $query = "UPDATE tb_eleitor SET engajamento = '". $engaj."' WHERE cpf = '".$cpf."';";
                        Yii::app()->db->createCommand("UPDATE tb_eleitor SET engajamento = '". $engaj."' WHERE cpf = '".$cpf."'")->execute();

                        // $sql = Yii::app()->db->createCommand($query)->execute();
                    }
                } else {
                    echo SimpleXLSX::parseError();
                }
                echo "<script type='text/javascript'>alert('Importação Realizada com sucesso.')</script>";
            } else {
                echo "<script type='text/javascript'>alert('Selecione um arquivo válido para importação.');</script>";
            }
        }
    }    
?>                                                                  