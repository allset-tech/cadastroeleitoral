
<div class="alert alert-danger" role="alert">
  <h4 class="alert-heading">Oops! Ocorreu um erro</h4>
  <p><?php echo CHtml::encode($message); ?></p>
</div>