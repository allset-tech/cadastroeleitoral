<?php
/**
 * The Header for our theme.
 *
 * @package Betheme
 * @author Muffin group
 * @link https://muffingroup.com
 */
?><!DOCTYPE html>

<form method="post" id="loginForm" name="loginForm" style="height:45px;background-color:#609451;width:100%;text-align:right; display: inline-flex;">
    <div style="width: 100%;background-color:#609451;text-align:right;font-size: 13pt;
        color: #FFF;
        vertical-align: middle;
        font-weight: bold; white-space:no-wrap;
        font-family: inherit; margin-right: 5px;
        line-height: 42px;background-color:#609451;">Área do Cliente:</div>
    <div style="margin-top: 6px;background-color:#609451;margin-right: 5px;">
        <input placeholder="CPF" id="docInput"name="docInput" type="text" maxlength="14" onkeydown="
        if (event.keyCode !== 8 && event.keyCode !== 46) {
            var cpf = this.value;
            if (cpf.match(/^\d{3}$/) !== null) {
            this.value = cpf + '.';
            } else if (cpf.match(/^\d{3}\.\d{3}$/) !== null) {
            this.value = cpf + '.';
            } else if (cpf.match(/^\d{3}\.\d{3}.\d{3}$/) !== null) {
            this.value = cpf + '-';
            }
        }" style="width:120px; height:30px; font-size: 11pt;font-weight: bold; border-radius: 25px;border: none;">
    </div>
   
    <div style="margin-top: 6px;background-color:#609451;margin-right: 5px;">
        <input placeholder="Senha" id="password"  name="password" type="password" maxlength="14"
            style="width:65px; height:30px; font-size: 11pt;font-weight: bold; border-radius: 25px;border: none;">
    </div>
    <div style="margin-right: 2px;margin-left: 5px;background-color:#609451;">
        <input type="submit" value="Login" id="loginBtn" name="loginBtn"
            style="margin-top:4px;padding:unset;color:#FFF;width:65px; font-weight: bold;height: 34px; border-radius: 25px;border: 1px solid #CCC; background-color:#cdc918;cursor: pointer;" onclick="return validate();"
            onmouseover="this.style.background='#d8d52c';" onmouseout="this.style.background='#cdc918';">
    </div>
</form>
<script type="text/javascript">
    function validate() {
       let pass = document.getElementById("password");
       let docInput =  document.getElementById("docInput");
		if (docInput.value === '') {
			docInput.style.border='1px solid red';
		} else {
			docInput.style.border='1px solid #CCC';
		}
		if (pass.value === '') {
			pass.style.border='1px solid red';
		} else {
			pass.style.border='1px solid #CCC';
		}
       return (pass.value !== '' && docInput.value != '')
    }
</script>
<?php
use Shuchkin\SimpleXLSX;

ini_set('error_reporting', E_ALL);
ini_set('display_errors', true);

require_once __DIR__.'/SimpleXLSX.php';

function isValid($user, $pass) {
	if ( $xlsx = SimpleXLSX::parse(__DIR__ .'/assets/login.xlsx') ) {
		list($cols,) = $xlsx->dimension();
		foreach( $xlsx->rows() as $k => $r) {
			if (strval( $r[0] ) === strval( $user ) && strval( $r[1] ) === strval( $pass )) {
				return true;	
			}
		}
	} else {
		echo SimpleXLSX::parseError();
	}
	return false;
}

function verifyAccess($user, $pass)
{
	//if (isValid($user, $pass) ) {
		$rep = array(".", "-");
		$retirado = str_replace($rep, "", $user);
		// $redirectURL = "https://patient.docway.com.br/appointment/basecorretoracompleto/create?documento=";
		// $redirectURL .= $retirado;
		$redirectURL = "https://autoatendimento.santaconsulta.com/?user=";
		$redirectURL .= $retirado . "&password=" . $pass;
		echo "<script type='text/javascript'>window.open('$redirectURL', '_self');</script>";
  	// } else {
	// 	echo "<script type='text/javascript'>alert('Dados Inválidos');</script>";
	// }
}

if ($_POST && key_exists('loginBtn', $_POST)){
   verifyAccess($_POST["docInput"], $_POST["password"]);
}
if ($_POST && key_exists('loginBtnLog', $_POST)){
   verifyAccess($_POST["docInputLog"], $_POST["passwordLog"]);
}

?>

<?php
	if ($_GET && key_exists('mfn-rtl', $_GET)):
		echo '<html class="no-js" lang="ar" dir="rtl">';
	else:
?>

<html <?php language_attributes(); ?> class="no-js <?php echo esc_attr(mfn_html_classes()); ?>"<?php mfn_tag_schema(); ?> >
<?php endif; ?>

<head>
	
	

<meta charset="<?php bloginfo('charset'); ?>" />
<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<?php do_action('mfn_hook_top'); ?>

	<?php get_template_part('includes/header', 'sliding-area'); ?>

	<?php
		if (mfn_header_style(true) == 'header-creative') {
			get_template_part('includes/header', 'creative');
		}
	?>

	<div id="Wrapper">

		<?php
			if (mfn_header_style(true) == 'header-below') {
				echo mfn_slider();
			}

			// $header_tmp_id = mfn_header_ID();
			$header_tmp_id = false;

			if( $header_tmp_id ){
				get_template_part( 'includes/header', 'template', array('id' => $header_tmp_id) );
			}else{
				get_template_part( 'includes/header', 'classic' );
			}

			if ( 'intro' == get_post_meta( mfn_ID(), 'mfn-post-template', true ) ) {
				get_template_part( 'includes/header', 'single-intro' );
			}
		?>

		<?php do_action( 'mfn_hook_content_before' );
