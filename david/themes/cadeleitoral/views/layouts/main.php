<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title><?php echo $this->pageTitle; ?></title>
  <!-- Custom fonts for this template-->
  <link href="<?php echo Yii::app()->theme->baseUrl;?>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="<?php echo Yii::app()->theme->baseUrl;?>/css/sb-admin-2.min.css" rel="stylesheet">
  <link href="<?php echo Yii::app()->theme->baseUrl;?>/css/sweetalert2.min.css" rel="stylesheet">
  <link href="<?php echo Yii::app()->theme->baseUrl;?>/css/default.css" rel="stylesheet">
  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo Yii::app()->theme->baseUrl;?>/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo Yii::app()->theme->baseUrl;?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- <link href="<?php //echo Yii::app()->theme->baseUrl;?>/css/dataTables.bootstrap.min.css" rel="stylesheet"> -->
  <link href="<?php echo Yii::app()->theme->baseUrl;?>/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  <link href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css" rel="stylesheet">
  <!-- Core plugin JavaScript-->
  <script src="<?php echo Yii::app()->theme->baseUrl;?>/vendor/jquery-easing/jquery.easing.min.js"></script>
  <!-- Custom scripts for all pages-->
  <script src="<?php echo Yii::app()->theme->baseUrl;?>/js/sb-admin-2.min.js"></script>
  <script src="<?php echo Yii::app()->theme->baseUrl;?>/js/demo.js"></script>
  <script src="<?php echo Yii::app()->theme->baseUrl;?>/js/default.js"></script>
  <!-- datatables plugin -->
  <script src="<?php echo Yii::app()->theme->baseUrl;?>/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?php echo Yii::app()->theme->baseUrl;?>/vendor/datatables/dataTables.bootstrap4.min.js"></script>
  <!-- export files -->
  <script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js" referrerpolicy="origin"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" referrerpolicy="origin"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" referrerpolicy="origin"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" referrerpolicy="origin"></script>
  <script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js" referrerpolicy="origin"></script>
  <script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js" referrerpolicy="origin"></script>
  <script src="<?php echo Yii::app()->theme->baseUrl;?>/js/sweetalert2.all.min.js"></script>
</head>
<body id="page-top" style="background-color: #0899A1;">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- End of Sidebar -->
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light text-white topbar mb-4 static-top shadow" style="position: sticky;background-color:#FFF">
          <img src="<?php echo Yii::app()->theme->baseUrl;?>/img/logo.png" style="height:120%;"/>
          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto" >
            <li class="nav-item dropdown no-arrow" >      
              <a class="navbar-brand text-white" style=" margin-top: 17px;margin-right: 27px;" href="<?php echo Yii::app()->urlManager->createUrl('eleitores/index'); ?>">
                <i class="fas fa-search" style="font-size: 22px;color:#082f67;"></i></a> 
              </a>
            </li>
            <li class="nav-item dropdown no-arrow" >      
              <a class="navbar-brand text-white" style="margin-top: 16px;" href="<?php echo Yii::app()->urlManager->createUrl('site/index'); ?>">
                <i class="fas fa-home" style="font-size: 22px;color:#082f67;"></i></a> 
              </a>
            </li>
            <?php 
              try{
                if(Yii::app()->user->perfil != "L"){   
                  echo '<li class="nav-item dropdown no-arrow">
                          <a class="nav-link" href="'.Yii::app()->urlManager->createUrl('usuarios/index').'">
                          <i style="font-size: 22px;" class="fas fa-users"></i>
                          </a>
                        </li>';    
                }
              }catch(Exception $e){
                $this->redirect(array('acesso/login')); 
              }?>
            <div class="topbar-divider d-none d-sm-block"></div>
            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">             
              <a class="nav-link" href="<?php echo Yii::app()->urlManager->createUrl('acesso/logout'); ?>">
              <div class="ico-sair">
            </div>
              <!-- <i style="font-size: 30px;color:rgba(222, 27, 27, 1)" class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-red-800"></i> -->
              </a>
            </li>
          </ul>
        </nav>
        <div class="container-fluid" style="background:#FFF;margin:25px">
          <?php echo $content; ?>
        </div>
      </div>
    </div>
  </div>
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  <div class="row" style="width:100%;background:#FFF;height:30px; position: absolute;bottom: 0px;">
</div>
</body>
</html>

