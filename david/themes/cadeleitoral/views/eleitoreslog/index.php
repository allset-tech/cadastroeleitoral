<script type="text/javascript">
    $().ready(function () {
        function listaEleitorlog(){
            var data = <?php echo $model; ?>;
            $("#total_linhas").html(<?php echo $num_rows; ?>);
            if ($.fn.dataTable.isDataTable('#listaEleitorlog')) {
            t.clear().draw();
            t = $('#listaEleitorlog').DataTable();
            } else {
                t = $('#listaEleitorlog').DataTable({  
                    dom: 'Bfrtip',                  
                    buttons: [
                    ],
                    responsive: true,
                    "paging": true,
                    "ordering": false,
                    "info": true,
                    "infoFiltered" : true,
                    "bFilter": false,
                    "columnDefs": [
                        { "class": "col-terco", "targets":[ 1,2,3 ] }
                    ],
                });
                var link = '<?php echo Yii::app()->request->baseUrl . '/' . Yii::app()->controller->id . '/'; ?>'
                    for (i = 0; i <= data.length - 1; i++) {
                        buttonView = ''
                        // urlView = link + 'visualizar/id/' + data[i]['id']
                        // buttonView = '<a href="' + urlView + '" title="Visualizar" class="text-center"><i class="fa fa-edit"></i></a>'
                        t.row.add([
                            data[i]['id'],
                            data[i]['cpf'],
                            data[i]['administrador'],
                            data[i]['data_exclusao'],
                        ]).draw(false);
                    }
                }
            }
            listaEleitorlog();
    });
</script>

<div class="container-fluid">
    <div class="title-bar">
        Cadastros Excluidos
    </div>  
    <div class="card shadow mb-4">
        <div class="card-body">
        <table class="table table-responsive table-bordered" id="listaEleitorlog" style="width:100%">
                <thead>
                    <tr>
                        <th class="text-center">ID</th>
                        <th class="text-center"style="width: 200px;">Eleitor Excluido</th>
                        <th class="text-center"style="width: 200px;">Responsável pela exclusão</th>
                        <th class="text-center" style="width: 200px;">Data exclusão</th>
                    </tr>
                </thead>
                <tbody></tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
        <div class="row" style="padding-bottom: 20px;padding-left: 20px;">    
            <a href="<?php echo Yii::app()->urlManager->createUrl('eleitores/index'); ?>" class="btn btn-info">
                Cancelar
            </a>
        </div>
    </div>
    
</div>

<div class="modal fade" id="modalResultado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">
                <div id="conteudo"></div>                
            </div>
        </div>
    </div>
</div> 