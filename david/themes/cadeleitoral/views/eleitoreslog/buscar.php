<div class="row">
    <div class="col-xl-12 col-md-12" style="background-color:#ededed;">
    Filtro de Busca:
    </div>
</div>

<form name="Usuario" id="filtra_usuarios" method="POST">
    <div class="form-row">
        <input type="hidden" id="file_name" value="" /> 

        <div class="form-group col-sm-2 col-md-2">
            <label for="cpf">CPF</label>
            <input type="cpf" class="form-control" id="e_cpf" placeholder="" value="">
        </div>

    </div>
    <div class="form-row">
        <button type="button" id="buscaLista" class="btn btn-fill btn-danger">Buscar</button>              
        &nbsp;<button type="button" class="btn btn-fill btn-info" id="limparFiltros">Limpar</button>
    </div>    
</form>  
    

<!--</div>    -->

<script type="text/javascript">
      $(document).ready(function() {

            $("#e_cpf").mask("999.999.999-99");
			//reset form
            $("#limparFiltros").click(function(){
              $('#filtra_usuarios')[0].reset();
            });

            function filtraLista(){

              var filtro = '';

              var cpf       = $("#e_cpf").val();

              if(cpf.length > 0 &&  cpf != " ")
                 filtro += '&cpf='+cpf


              $.ajax({
                'url': '<?php echo Yii::app()->baseUrl; ?>/eleitoreslog/filtrar',
                'data': filtro,
                'dataType': 'json',
                'type': 'post',
                'success': function (data) {
                    if (data.status == "Error") {
                        $.growl.error({message: 'Erro!!! ' + data.detalhes});
                    } else {
                        if (data.status == "Ok") {
                              t.clear().draw();

                             if(data.detalhes.length > 0)
                             {
                                  $("#total_linhas").html(data.total);

                                  var link = '<?php echo Yii::app()->request->baseUrl . '/' . Yii::app()->controller->id . '/'; ?>'

                                for (i = 0; i <= data.detalhes.length - 1; i++) {
                                    buttonView = ''
                                    urlView = link + 'visualizar/id/' + data.detalhes[i]['cpf']
                                    buttonView = '<a href="' + urlView + '" title="Visualizar" class="text-center"><i class="fa fa-edit"></i></a>'

                                    t.row.add([
                                        buttonView,
                                        data.detalhes[i]['cpf'],
                                        data.detalhes[i]['administrador'],
                                        data.detalhes[i]['data_cexclusao'],
                                    ]).draw(false);

                                }

                             }

                        }
                    }
                },
                'cache': false, });

            }


            $('#buscaLista').click(function() {
              filtraLista();
            })
      });
</script>
