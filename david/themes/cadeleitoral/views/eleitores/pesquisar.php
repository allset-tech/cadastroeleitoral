<script type="text/javascript">    
    $(document).ready(function(){
        $("#cpf").mask('999.999.999-99');
        $("#btnPesquisar").click(function(e){
            e.preventDefault();
            let cpf         = $("#cpf").val();
            let meu_cpf     = '<?php echo @Yii::app()->user->id;?>'; 
            if(cpf <= 0){
                sweetAlert('Oops...', 'Informe o campo CPF!', 'error');
                return false;
            }
            $.ajax({
                'url': '<?php echo Yii::app()->baseUrl; ?>/eleitores/pesquisarcpf',
                'data': 'cpf='+cpf,
                'type': 'post',
                'dataType': 'json',
                'success': function(data) {
                    if (data.status == 'Ok') {
                           //CPF encontrado
                           if(data.resultado == 'SIM'){
                                //Eu sou o lider
                                if(data.lider == meu_cpf){
                                    sweetAlert({
                                                title: 'Deseja realizar alterações no cadastro?',
                                                //showDenyButton: true,
                                                showCancelButton: true,
                                                confirmButtonText: 'Sim',
                                                //denyButtonText: `Não`,
                                            }).then((result) => {
                                            if (result.value == true) {
                                                //swal('Saved!', '', 'success')
                                                window.location.href = '<?php echo Yii::app()->baseUrl; ?>/eleitores/visualizar/id/' + data.eleitor
                                            } else if (result.dismiss == 'cancel') {
                                                //swal('Changes are not saved', '', 'info')
                                                $("#cpf").val('');
                                                $("#cpf").focus();
                                            }
                                            }); 
                                } else {
                                    sweetAlert('Oops...', 'Este CPF já foi cadastrado por : ' + data.lider_nome, 'error');
                                    $("#cpf").val('');
                                    $("#cpf").focus();
                                }
                           } else {
                                sweetAlert('Oops...', 'CPF não encontrado!', 'error');
                                window.location.href = '<?php echo Yii::app()->baseUrl; ?>/eleitores/novo'
                           }
                        } else {
                            sweetAlert('Oops...', 'Erro:'+ data.detalhes, 'error');
                        }
                },
                'cache': false,
            });
          });
    });                            
</script>

<div class="col-xl-12">
<!-- Área de Erros  -->
<?php
$flashMessages = Yii::app()->user->getFlashes();
if ($flashMessages) {
    foreach ($flashMessages as $key => $message) {
        ?>
        <div class="alert alert-<?php echo ($key == 'error') ? 'danger' : $key; ?>">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <i class="fa fa-warning"></i>
            <h4><?php echo $message; ?></h4>
            <?php echo $form->errorSummary($model, null, null, null); ?>
        </div>
        <?php
    }
}
?>
</div>
<div class="container-fluid">
  <!-- Page Heading -->
  <h2 class="h3 mb-2 text-gray-800"><i class="fas fa-search"></i> Pesquisar CPF</h2>
  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <div class="row">
      </div>
    </div>
    <div class="card-body">     
        <form>
            <div class="col-xl-12">
                <div class="form-row">
                    <div class="col-md-8 mb-8">
                        <input type="text" class="form-control" id="cpf" value="" placeholder="999.999.999-99"/>
                    </div>
                    <div class="col-md-2 mb-2 text-center">
                        <button class="btn btn-success" id="btnPesquisar">Pesquisar</button>
                    </div>
                </div>
                <div class="form-row">
                </div>    
            </div>
        </form>
    </div>
 </div>
</div>