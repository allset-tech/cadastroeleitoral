<script type="text/javascript" charset="UTF-8">

    function excluir(id){
        if(confirm("Deseja excluir este registro?") == true){
            $.ajax({
            'url': '<?php echo Yii::app()->baseUrl; ?>/eleitores/excluir',
            'data': {'id':id},
            'type': 'post',
            'dataType': 'json',
            'success': function(data) {
                if (data.resultado) {
                swal({
                        title: "Tudo Certo!",
                        text: "Cadastro excluído com sucesso",
                        type: "success"
                        }).then(function() {
                        window.location.reload();
                    });
                } else {
                    sweetAlert('Oops...', 'Erro:'+ data.detalhes, 'error');
                }
            },
            'cache': false,
            });
        }
    }

    $().ready(function () {
        function returnColumns(){
            return add_columns;
        }
        function buildDataTable(){
            
            if ($.fn.dataTable.isDataTable('#listaEleitores')) {
                t.clear().draw();
                t = $('#listaEleitores').DataTable();
            } else {
                setTimeout(() => {
                    $('#buscaLista').click();
                    
                }, 100);
            }
        }
        function defineCampos(){
            $("#campos_pdf select").each(function(index){
                if ($(this).has('option:selected')){
                    console.log('Select number ' + index + ': ' + $(this).val());
                }
            });
        }
        let colunas_pdf = [38, 9, 22, 4, 18, 13, 8, 21, 36];
        let add_columns = [38, 9, 22, 4, 18, 13, 8, 21, 36, 39, 5, 35, 31];
        buildDataTable();
    });
</script>

<div class="title-bar" style="line-height: 45px;">
    <div style="display:inline-flex;width:100%">
        <div style="margin-top:-2px;">
            <img src="<?php echo Yii::app()->theme->baseUrl;?>/img/ico_cadastro.png"/>
        </div>   
        <div style="margin-left:10px">
            Relação de Cadastros
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="py-3">
            <div class="col-xl-12">
                <?php
                    $this->renderPartial('buscar', array(
                        'model'         => $model,
                        'engajamento'   => $engajamento,
                        'qtdeDependetes'=> $qtdeDependetes,
                        'faixaEtaria'=> $faixaEtaria,
                        'codigos'       => $codigos,
                    ));
                ?>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-responsive table-bordered" id="listaEleitores" width="100%">
                <thead>
                    <tr>
                        <th class="text-center">AÇÕES</th>
                        <th class="text-center" style="text-align:left !important">LÍDER</th>
                        <th class="text-center" style="text-align:left !important">CPF</th> 
                        <th class="text-center" style="text-align:left !important">NOME</th>
                        <th class="text-center">CÓDIGO</th>
                        <th class="text-center">PROFISSÃO</th> 
                        <th class="text-center">DATA NASC</th>
                        <th class="text-center">WHATSAPP</th>
                        <th class="text-center">BAIRRO</th>
                        <th class="text-center">SEXO</th> 
                        <th class="text-center">RG</th>
                        <th class="text-center">NOME MÃE</th> 
                        <th class="text-center">TELEFONE</th> 
                        <th class="text-center">E-MAIL</th>
                        <th class="text-center">CPF LIDER</th> 
                        <th class="text-center">DATA VISITA</th> 
                        <th class="text-center">HORA VISITA</th> 
                        <th class="text-center">INDICADO POR</th> 
                        <th class="text-center">FACEBOOK</th> 
                        <th class="text-center">INSTAGRAM</th> 
                        <th class="text-center">ENGAJAMENTO</th> 
                        <th class="text-center">ENDEREÇO</th> 
                        <th class="text-center">NÚMERO</th> 
                        <th class="text-center">ESTADO</th> 
                        <th class="text-center">MUNICÍPIO</th>
                        <th class="text-center">CEP</th> 
                        <th class="text-center">ZONA</th> 
                        <th class="text-center">SEÇÃO</th> 
                        <th class="text-center">Nª PESSOAS FAMILIA</th>
                        <th class="text-center">LÍDER COMUNITÁRIO</th> 
                        <th class="text-center">TIPO RESIDÊNCIA</th> 
                        <th class="text-center">POSSUI VEÍCULO</th> 
                        <th class="text-center">TIPO VEÍCULO</th> 
                        <th class="text-center">TÍTULO DE ELEITOR</th> 
                        <th class="text-center">SITUAÇÃO TÍTULO</th> 
                        <th class="text-center">ESTIMATIVA VOTOS</th> 
                        <th class="text-center">DATA CADASTRO</th> 
                        <th class="text-center">ZONA MORADIA</th> 
                        <th class="text-center">LOCAL VOTAÇÃO</th> 
                        <th class="text-center">OBSERVAÇÃO</th>
                        <th class="text-center">DEPENDETES</th>
                        <th class="text-center">NOME DEPENDENTE 1</th>
                        <th class="text-center">FAIXA ETÁRIA</th>
                        <th class="text-center">NOME DEPENDENTE 2</th>
                        <th class="text-center">FAIXA ETÁRIA</th>
                        <th class="text-center">NOME DEPENDENTE 3</th>
                        <th class="text-center">FAIXA ETÁRIA</th>
                        <th class="text-center">NOME DEPENDENTE 4</th>
                        <th class="text-center">FAIXA ETÁRIA</th>
                        <th class="text-center">NOME PAI</th>
                        <th class="text-center">ENDEREÇO VOTAÇÃO</th>
                        <th class="text-center">BAIRRO VOTAÇÃO</th>
                        <th class="text-center">MUNICÍPIO VOTAÇÃO</th>
                        <th class="text-center">TWITTER</th>
                        <th class="text-center">TELEFONE RECADOS</th>
                        <th class="text-center">CELULAR RECADOS</th>
                    </tr>
                </thead>
                <tbody></tbody>
                <tfoot>
                    <tr>
                        <th colspan="2" style="text-align:right">TOTAL:</th>
                        <th><div id="total_linhas"></div></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="modalResultado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">
                <div id="conteudo"></div>                
            </div>
        </div>
    </div>
</div> 