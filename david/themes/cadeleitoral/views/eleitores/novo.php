<?php
Yii::app()->user->setState('titulo', 'Novo Cadastro');
$this->renderPartial('form', array(
    'model'             => $model,
    'sexo'               => $sexo,
    'estado_civil'       => $estado_civil,
    'engajamento'        => $engajamento,
    'lider_comunitario'  => $lider_comunitario,
    'tipo_residencia'    => $tipo_residencia,
    'possui_veiculo'     => $possui_veiculo,
    'situacao_titulo'    => $situacao_titulo,  
    'zona_moradia'       => $zona_moradia,
    'codigos'            => $codigos,   
    'falecido'           => $falecido,
    'tipo_veiculo'       => $tipo_veiculo,  
    'bairros'            => $bairros,  
    'qtdeDependetes'     => $qtdeDependetes,
    'faixaEtaria'     => $faixaEtaria
));
?>