<div class="form-row" style="margin: 10px 0px 20px 0px">
    <a target="__blank" href="<?php echo Yii::app()->urlManager->createUrl('eleitores/imprimeficha'); ?>" class="btn btn-primary" id="btnFicha"><i class="fas fa-file-download"></i> Nova Ficha</a>
    &nbsp;<?php if(Yii::app()->user->perfil == 'A'): ?>
        <a href="<?php echo Yii::app()->urlManager->createUrl('eleitoreslog/index'); ?>" class="btn btn-danger">
            <i class="fas fa-exchange-alt"></i> Log de Exclusão
        </a>
    <?php endif;?>
        
</div>
<div class="filtr-busca"> 
        Filtro de Busca
</div>
<form class="card-header" action="exportExcel" target="_blank" name="Eleitor" id="filtra_eleitores" method="POST">
    <div class="form-row">
        <div class="form-group col-sm-3 col-md-3">
            <label for="lider">Líder</label>
            <input type="lider" class="form-control" id="e_lider"name="e_lider" placeholder="" value="">
        </div>
        <div class="form-group col-sm-3 col-md-3"  >
            <label for="engajamento">Engajamento</label>
            <?php echo CHtml::dropDownList('e_engajamento', '', $engajamento, array('class' => 'form-control', 'id' => 'e_engajamento', 'empty' => '--')); ?>
        </div>
        <div class="form-group col-sm-2 col-md-2">
            <label for="codigo">Código</label>
            <?php echo CHtml::dropDownList('e_codigo', '', $codigos, array('class' => 'form-control', 'id' => 'e_codigo', 'empty' => '--')) ?>
        </div>
        <div class="form-group col-sm-2 col-md-2"  >
            <label for="municipio">Cidade</label>
            <input type="municipio" class="form-control" id="e_municipio"name="e_municipio" placeholder="" value="">
        </div>
        <div class="form-group col-sm-2 col-md-2"  >
            <label for="bairro">Bairro</label>
            <input type="bairro" class="form-control" name="e_bairro" id="e_bairro" placeholder="" value="">
        </div>
       
        <div class="form-group col-sm-3 col-md-3"  >
            <label for="observacao">Observação</label>
            <input type="observacao" class="form-control" id="e_observacao"name="e_observacao" placeholder="" value="">
        </div>
        <div class="form-group col-sm-3 col-md-3"  >
            <label for="qtdeDependetes">Quantidade de Dependetes</label>
            <?php echo CHtml::dropDownList('e_qtdeDependetes', '', $qtdeDependetes, array('class' => 'form-control', 'id' => 'e_qtdeDependetes', 'empty' => '--')); ?>
        </div>
        <div class="form-group col-sm-2 col-md-2"  >
            <label for="nomeDep">Nome Dependente</label>
            <input type="nomeDep" class="form-control" id="e_nomeDep"name="e_nomeDep" placeholder="" value="">
        </div>
        <div class="form-group col-sm-2 col-md-2"  >
            <label for="faixaEtaria">Faixa Etária</label>
            <?php echo CHtml::dropDownList('e_faixaEtaria', '', $faixaEtaria, array('class' => 'form-control', 'id' => 'e_faixaEtaria', 'empty' => '--')); ?>
        </div>
        <div class="form-group col-sm-2 col-md-2"  >
            <label for="nome_pai">Nome Pai</label>
            <input type="nome_pai" class="form-control" id="e_nome_pai"name="e_nome_pai" placeholder="" value="">
        </div>

        <div class="form-group col-sm-3 col-md-3"  >
            <label for="bairro_votacao">Bairro Votação</label>
            <input type="bairro_votacao" class="form-control" id="e_bairro_votacao"name="e_bairro_votacao" placeholder="" value="">
        </div>
        <div class="form-group col-sm-3 col-md-3"  >
            <label for="municipio_votacao">Município Votação</label>
            <input type="municipio_votacao" class="form-control" id="e_municipio_votacao"name="e_municipio_votacao" placeholder="" value="">
        </div>
        <div class="form-group col-sm-2 col-md-2"  >
            <label for="twitter">Twitter</label>
            <input type="twitter" class="form-control" id="e_twitter"name="e_twitter" placeholder="" value="">
        </div>
        <div class="form-group col-sm-2 col-md-2"  >
            <label for="telefone_recado">Telefone Recado</label>
            <input type="telefone_recado" class="form-control" id="e_telefone_recado"name="e_telefone_recado" placeholder="" value="">
        </div>
        <div class="form-group col-sm-2 col-md-2" >
            <label for="celRecado">Celular Recado</label>
            <input type="celRecado" class="form-control" id="e_cel_recado"name="e_cel_recado" placeholder="" value="">
        </div> 



        <div class="form-group col-sm-3 col-md-3" style="display:none;">
            <label for="nome">Nome</label>
            <input type="nome" class="form-control" id="e_nome"name="e_nome" placeholder="" value="">
        </div>
        <div class="form-group col-sm-3 col-md-3" style="display:none;">
            <label for="cpf">CPF</label>
            <input type="cpf" class="form-control" id="e_cpf"name="e_cpf" placeholder="" value="">
        </div>
        
        <div class="form-group col-sm-2" style="display:none;" >
            <label for="endereco_votacao">Endereço Votação</label>
            <input type="endereco_votacao" class="form-control" id="e_endereco_votacao"name="e_endereco_votacao" placeholder="" value="">
        </div>
        
       
    </div>

    <div class="form-row">
    &nbsp;&nbsp;<button type="button" id="buscaLista" class="btn btn-fill btn-danger">Buscar</button>              
        &nbsp;<button type="button" class="btn btn-fill btn-info" id="limparFiltros">Limpar</button>
        &nbsp;<button type="submit" id="buscaLista" class="btn btn-success btn-blue" ><i class="fas fa-file-download"></i>&nbsp;&nbsp;Excel</button>
    </div>
</form>  

<script type="text/javascript">
      $(document).ready(function() {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        $("#e_cpf").mask('999.999.999-99');
        $("#e_cel_recado").mask('(99) 9999-9999');
        $("#e_telefone_recado").mask('(99) 9999-9999');

        $("#e_cpf").val(urlParams.get('cpf'));
        $("#limparFiltros").click(function(){
            $('#filtra_eleitores')[0].reset();
        });
       
 		function returnColumns(){
              return add_columns;
          }
          let add_columns = [38, 9, 22, 4, 18, 13, 8, 21, 36, 39, 5, 35, 31];
            function filtraLista(){
                var filtro = '';
                var lider       = $("#e_lider").val();
                var cpf       = $("#e_cpf").val();
                var nome       = $("#e_nome").val();
                var bairro      = $("#e_bairro").val();
                var municipio      = $("#e_municipio").val();
                var observacao      = $("#e_observacao").val();
                var nomeDep     = $("#e_nomeDep").val();
                var nome_pai     = $("#e_nome_pai").val();
                var endereco_votacao     = $("#e_endereco_votacao").val();
                var bairro_votacao     = $("#e_bairro_votacao").val();
                var municipio_votacao     = $("#e_municipio_votacao").val();
                var twitter     = $("#e_twitter").val();
                var telefone_recado     = $("#e_telefone_recado").val();
                var cel_recado     = $("#e_cel_recado").val();
                var faixaEtaria =  ($("#e_faixaEtaria").val() != "") ? $("#e_faixaEtaria option:selected").val() : '';
                var engajamento = ($("#e_engajamento").val() != "") ? $("#e_engajamento option:selected").text() : '';
                var codigo      = ($("#e_codigo").val() != "") ? $('#e_codigo option:selected').toArray().map(item => item.text).join() : '';
                var qtdeDependetes = ($("#e_qtdeDependetes").val() != "") ? $("#e_qtdeDependetes option:selected").text() : '';
                if(lider.length > 0 &&  lider != " ")
                    filtro += '&lider='+lider
                if(cpf.length > 0 &&  cpf != " ")
                    filtro += '&cpf='+cpf
                if(nome.length > 0 &&  nome != " ")
                    filtro += '&nome='+nome
                if(bairro.length > 0 &&  bairro != " ")
                    filtro += '&bairro='+bairro
                if(municipio.length > 0 &&  municipio != " ")
                    filtro += '&municipio='+municipio
                if(engajamento.length > 0 &&  engajamento != " ")
                    filtro += '&engajamento='+engajamento
                if(qtdeDependetes.length > 0 &&  qtdeDependetes != " ")
                    filtro += '&qtdeDependetes='+qtdeDependetes
                if(codigo.length > 0 &&  codigo != " ")
                    filtro += '&codigo='+codigo
                if(observacao.length > 0 &&  observacao != " ")
                    filtro += '&observacao='+observacao
                if(faixaEtaria.length > 0 &&  faixaEtaria != " ")
                    filtro += '&faixaEtaria='+faixaEtaria
                if(nomeDep.length > 0 &&  nomeDep != " ")
                    filtro += '&nomeDep='+nomeDep
                if(nome_pai.length > 0 &&  nome_pai != " ")
                    filtro += '&nome_pai='+nome_pai
                if(endereco_votacao.length > 0 &&  endereco_votacao != " ")
                    filtro += '&endereco_votacao='+endereco_votacao
                if(bairro_votacao.length > 0 &&  bairro_votacao != " ")
                    filtro += '&bairro_votacao='+bairro_votacao
                if(municipio_votacao.length > 0 &&  municipio_votacao != " ")
                    filtro += '&municipio_votacao='+municipio_votacao
                if(twitter.length > 0 &&  twitter != " ")
                    filtro += '&twitter='+twitter
                if(telefone_recado.length > 0 &&  telefone_recado != " ")
                    filtro += '&telefone_recado='+telefone_recado
                if(cel_recado.length > 0 &&  cel_recado != " ")
                    filtro += '&cel_recado='+cel_recado

                if ($.fn.dataTable.isDataTable('#listaEleitores')) {
                    t.clear().draw();
                }
            t = $('#listaEleitores').DataTable( {
                    "bFilter": false,
                    "responsive": true,
                    "initComplete": function(settings, json) {
                        var api = this.api();
                        var numRows = api.rows( ).count();
                        $("#total_linhas").html(numRows);
                    },
                   "bDestroy": true,
                   "dom": 'Bfrtip',                  
                    buttons: [],
                    "columnDefs": [
                        { "class": "nowrap-column", "targets": [1,2,3,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55 ] }
                    ],
                    "processing": true,
                    "serverSide": false,
                    "ajax": {
                      'url'  : '<?php echo Yii::app()->baseUrl; ?>/eleitores/filtrar',
                      'data' : {
                          'coordenador' : '',
                          'lider' : lider,
                          'cpf' : cpf,
                          'nome' : nome,
                          'bairro' : bairro,
                          'municipio' : municipio,
                          'engajamento' : engajamento,
                          'codigo' : codigo,
                          'observacao' : observacao,
                          'qtdeDependetes' : qtdeDependetes,
                          'faixaEtaria' : faixaEtaria,
                          'nomeDep' : nomeDep,
                          'nome_pai': nome_pai,
                          'endereco_votacao': endereco_votacao,
                          'bairro_votacao': bairro_votacao,
                          'municipio_votacao': municipio_votacao,
                          'twitter': twitter,
                          'telefone_recado': telefone_recado,
                          'cel_recado': cel_recado
                      }
                  },
                  "columns": [
                    {
                        "data": 2,
                        "render": function (data) {
                            let perfil = "<?php echo Yii::app()->user->perfil == 'A'; ?>";
                            if(perfil != ""){
                                return '<a href="<?php echo Yii::app()->baseUrl; ?>/eleitores/visualizar/id/'+ data + '" title="Editar"><i class="fa fa-edit"></i></a> ' +  
                                ' <i class="fa fa-trash" title="Excluir" onclick="excluir(\''+data+'\')"></i>';    
                            }else{
                                return '<a href="<?php echo Yii::app()->baseUrl; ?>/eleitores/visualizar/id/'+ data + '" title="Editar"><i class="fa fa-edit"></i></a>';   
                            }
                        },
                    },
                    { "data":1},
                    { "data":2},
                    { "data":3},
                    { "data":4},
                    { "data":5},
                    { "data":6},
                    { "data":7},
                    { "data":8},
                    { "data":9},
                    { "data":10},
                    { "data":11},
                    { "data":12},
                    { "data":13},
                    { "data":14},
                    { "data":15},
                    { "data":16},
                    { "data":17},
                    { "data":18},
                    { "data":19},
                    { "data":20},
                    { "data":21},
                    { "data":22},
                    { "data":23},
                    { "data":24},
                    { "data":25},
                    { "data":26},
                    { "data":27},
                    { "data":28},
                    { "data":29},
                    { "data":30},
                    { "data":31},
                    { "data":32},
                    { "data":33},
                    { "data":34},
                    { "data":35},
                    { "data":36},
                    { "data":37},
                    { "data":38},
                    { "data":40},
                    { "data":41},
                    { "data":42},
                    { "data":43},
                    { "data":44},
                    { "data":45},
                    { "data":46},
                    { "data":47},
                    { "data":48},
                    { "data":49},
                    { "data":50},
                    { "data":51},
                    { "data":52},
                    { "data":53},
                    { "data":54},
                    { "data":55},
                    { "data":56},
                    ],
              } );
            }
            $('#buscaLista').click(function() {
              filtraLista();
              $("#e_cpf").val('');
            })
      });
</script>

