<?php
Yii::app()->user->setState('titulo', 'Visualizando Cadastro');
$this->renderPartial('form', array(
    'model'              => $model,
    'sexo'               => $sexo,
    'estado_civil'       => $estado_civil,
    'engajamento'        => $engajamento,
    'lider_comunitario'  => $lider_comunitario,
    'tipo_residencia'    => $tipo_residencia,
    'possui_veiculo'     => $possui_veiculo,
    'situacao_titulo'    => $situacao_titulo, 
    'zona_moradia'       => $zona_moradia, 
    'codigos'            => $codigos,
    'coordenador'        => $coordenador,
    'lider'              => $lider,
    'falecido'           => $falecido,
    'tipo_veiculo'       => $tipo_veiculo,
    'bairros'            => $bairros, 
    'lideres'            => $lideres,
    'coordenadores'      => $coordenadores,
    'qtdeDependetes'     =>$qtdeDependetes,
    'faixaEtaria'     => $faixaEtaria
));
?>