<script type="text/javascript">
    $().ready(function () {
        $("#Eleitor_bairro").change(function(){
            exibeZona($(this).val());
        });
        function exibeZona(value){
            let bairro = value;
            if(bairro){
                $.ajax({
                    'url': '<?php echo Yii::app()->baseUrl; ?>/eleitores/listazona',
                    'data': 'bairro='+bairro,
                    'type': 'post',
                    'dataType': 'json',
                    'success': function(data) {
                        if (data.status == 'OK') {
                           // $("#Eleitor_zona").val('');
                            $('#Eleitor_zona_moradia').prop('selectedIndex',0);
                            $("#Eleitor_zona_moradia").val(data.detalhes);
                        } else {
                            sweetAlert('Oops...', 'Erro:'+ data.detalhes, 'error');
                        }
                    },
                    'cache': false,
                });
            }
        }
        function validaFormulario(){
            let cpf = $("#Eleitor_cpf").val();
            if(cpf.length < 14){
                sweetAlert('Oops...', 'CPF inválido!!', 'error');
                $("#Eleitor_cpf").focus();
                return false;
            }
            return true;
        }
        <?php if (Yii::app()->controller->action->id != "novo") { ?>
            var codigos = '<?php echo $model->codigos_escolhidos; ?>'
            var lista   = codigos.split("#"); 
             $("#Eleitor_codigo").val(lista);
        <?php }?>

        $("#btnSalvarEleitor").click(function(e){
            e.preventDefault();
            if(validaFormulario() === true)
                $.ajax({
                    'url': $("#Eleitor-form").attr("action"),
                    'data': $("#Eleitor-form").serialize(),
                    'type': 'post',
                    'dataType': 'json',
                    'success': function(data) {
                        if (data.status == 'Ok') {
                            sweetAlert('Ok!', 'Dados registrados com sucesso!', 'success');
                            <?php if (Yii::app()->controller->action->id == "novo"):?>
                                window.setTimeout(function() {
                                    window.location.href = '<?php echo Yii::app()->urlManager->createUrl('eleitores/index'); ?>';                  
                                }, 2000);
                            <?php endif; ?>
                            <?php if (Yii::app()->controller->action->id == "visualizar"):?>
                                window.setTimeout(function() {
                                    window.location.href = '<?php echo Yii::app()->urlManager->createUrl('eleitores/index'); ?>';                  
                                }, 2000);
                            <?php endif; ?>
                        } else {
                            sweetAlert('Oops...', 'Erro:'+ data.detalhes, 'error');
                        }
                    },
                    'cache': false,
                });
        });    
    });
</script>
<?php
if (Yii::app()->controller->action->id == "novo") {
    $formAction = Yii::app()->createUrl('eleitores/AdicionaEleitor', array());
} else {
    $formAction = Yii::app()->createUrl('eleitores/salvar/id/'.$model->cpf, array());
}

$form = $this->beginWidget("CActiveForm", array(
    'id'          => 'Eleitor-form',
    'htmlOptions' => array('enctype' => 'multipart/form-data', 'class' => 'form-inline'),
    'action'      => $formAction,
    ));

?>

<div class="col-xl-12">
<!-- Área de Erros  -->
    <?php
    $flashMessages = Yii::app()->user->getFlashes();
    if ($flashMessages) {
        foreach ($flashMessages as $key => $message) {
            ?>
            <div class="alert alert-<?php echo ($key == 'error') ? 'danger' : $key; ?>">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <i class="fa fa-warning"></i>
                <h4><?php echo $message; ?></h4>
                <?php echo $form->errorSummary($model, null, null, null); ?>
            </div>
            <?php
        }
    }
    ?>
</div>

<div class="container-fluid">
    <!-- Page Heading -->
    <div class="title-bar">
        Cadastrar Indivíduo
    </div>       
    <div class="card-body">
        <div style="width:100%;display:inline-flex;">
            <div  style="width:75%;">
                <div class="form-row">
                    <div class="col-md-4  mb-2">
                        <label for="validationTooltip03">Indicado por</label>
                        <?php if (Yii::app()->controller->action->id == "novo") { ?>
                            <?php echo CHtml::textField('Eleitor[indicado_por]', '', array('class' => 'form-control', 'style' => 'text-transform: uppercase','id' => 'Eleitor_indicado_por')) ?>
                        <?php } else { ?>
                            <?php echo $form->textField($model, 'indicado_por', array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_indicado_por')) ?>
                        <?php } ?>
                    </div>
                    <div class="col-md-4  mb-2">
                        <label for="">Data Visita</label>
                        <?php if (Yii::app()->controller->action->id == "novo") { ?>
                            <?php
                                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                    'name' => 'Eleitor[data_visita]',
                                    'model' => $model,
                                    'attribute' => 'data_visita',
                                    'value' => '',
                                    'language' => 'pt',
                                    'options' => array(
                                        'dateFormat' => 'dd/mm/yy',
                                    ),
                                    'htmlOptions' => array(
                                        'class' => 'form-control',
                                        'id' => 'Eleitor_data_visita',
                                        'autocomplete' => 'on',
                                    )
                                ));
                            ?>
                        <?php } else { ?>
                            <?php echo $form->textField($model, 'data_visita', array('class' => 'form-control', 'id' => 'Eleitor_data_visita')) ?>
                        <?php } ?>
                    </div>
                    <div class="col-md-4 mb-2">
                        <label for="validationTooltip03">Hora visita</label>
                        <?php if (Yii::app()->controller->action->id == "novo") { ?>
                            <?php echo CHtml::textField('Eleitor[hora_visita]', '', array('class' => 'form-control', 'id' => 'Eleitor_hora_visita')) ?>
                        <?php } else { ?>
                            <?php echo $form->textField($model, 'hora_visita', array('class' => 'form-control', 'id' => 'Eleitor_hora_visita')) ?>
                        <?php } ?>
                    </div>
                    <div class="col-md-4 mb-2">
                        <label for="validationTooltip03">Engajamento</label>
                        <?php if (Yii::app()->controller->action->id == "novo") { ?>
                            <?php echo CHtml::dropDownList('Eleitor[engajamento]', '', $engajamento, array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_engajamento', 'empty' => '--selecione--')) ?>
                        <?php } else { ?>
                            <?php echo $form->dropDownList($model, 'engajamento', $engajamento, array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_engajamento', 'empty' => '--selecione--')) ?>
                        <?php } ?>
                    </div>  
                    <?php if (Yii::app()->controller->action->id != "novo") { ?>
                        <div class="col-md-4 mb-2">
                            <label for="validationTooltip03">Falecido?</label>
                            <?php echo $form->dropDownList($model, 'falecido', $falecido, array('class' => 'form-control','style' => 'text-transform: uppercase', 'id' => 'Eleitor_falecido', 'empty' => '--selecione--')) ?>
                        </div>
                    <?php } ?>
                    <?php if (Yii::app()->controller->action->id != "novo") { ?>
                        <div class="col-md-4  mb-2">
                            <label for="validationTooltip03">Lider</label>
                            <?php echo $form->dropDownList($model, 'lider', $lideres, array('class' => 'form-control','style' => 'text-transform: uppercase', 'id' => 'Eleitor_lider', 'empty' => '--selecione--')) ?>
                        </div>
                    <?php } ?>
                    
                </div>
            </div>
            <div  style="width:25%;height:100%">
                <div class="col-md-12 mb-2">
                    <label for="">Código*</label>
                    <?php //if (Yii::app()->controller->action->id == "novo") { ?>
                    <?php //} else { ?>
                        <?php //echo $form->dropDownList($model, 'codigo', $codigo, array('class' => 'form-control', 'id' => 'Eleitor_codigo', 'empty' => '--selecione--', 'multiple' => 'multiple')) ?>
                    <?php //} ?>
                    <?php echo CHtml::dropDownList('Eleitor[codigo]', '', $codigos, array('class' => 'form-control combo-mult', 'id' => 'Eleitor_codigo', 'multiple' => 'multiple')) ?>
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-xl-12 col-md-12">
            <div class="form-row title-divider">
                DADOS PESSOAIS
            </div> 
            <div class="form-row">
                <div class="col-md-3 mb-2">
                    <label for="">Nome*</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[nome]', '', array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_nome')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'nome', array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_nome')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-3 mb-2">
                    <label for="validationTooltip01">CPF*</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[cpf]', '', array('class' => 'form-control', 'id' => 'Eleitor_cpf')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'cpf', array('class' => 'form-control', 'id' => 'Eleitor_cpf')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-3 mb-2">
                    <label for="">Rg*</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[rg]', '', array('class' => 'form-control', 'id' => 'Eleitor_rg')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'rg', array('class' => 'form-control', 'id' => 'Eleitor_rg')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-3 mb-2">
                    <label for="">Data Nasc.*</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php
                            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                'name' => 'Eleitor[data_nasc]',
                                'model' => $model,
                                'attribute' => 'data_nasc',
                                'value' => '',
                                'language' => 'pt',
                                'options' => array(
                                    'dateFormat' => 'dd/mm/yy',
                                ),
                                'htmlOptions' => array(
                                    'class' => 'form-control',
                                    'id' => 'Eleitor_data_nasc',
                                    'autocomplete' => 'on',
                                )
                            ));
                        ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'data_nasc', array('class' => 'form-control', 'id' => 'Eleitor_data_nasc')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-3 mb-2">
                    <label for="validationTooltip03">Nome da mãe*</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[nome_mae]', '', array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_nome_mae')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'nome_mae', array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_nome_mae')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-3 mb-2">
                    <label for="validationTooltip03">Nome do pai</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[nome_pai]', '', array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_nome_pai')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'nome_pai', array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_nome_pai')) ?>
                    <?php } ?>
                </div> 
                <div class="col-md-3 mb-2">
                    <label for="">Sexo</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::dropDownList('Eleitor[sexo]', '', $sexo, array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_sexo', 'empty' => '--selecione o sexo--')) ?>
                    <?php } else { ?>
                        <?php echo $form->dropDownList($model, 'sexo', $sexo, array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_sexo', 'empty' => '--selecione o sexo--')) ?>
                    <?php } ?>
                </div>   
                <div class="col-md-3 mb-2">
                    <label for="">Estado Civil</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::dropDownList('Eleitor[estado_civil]', '', $estado_civil, array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_estado_civil', 'empty' => '--selecione o estado civil--')) ?>
                    <?php } else { ?>
                        <?php echo $form->dropDownList($model, 'estado_civil', $estado_civil, array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_estado_civil', 'empty' => '--selecione o estado civil--')) ?>
                    <?php } ?>
                </div>  
                <div class="col-md-2 mb-2">
                    <label for="">CEP</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                    <?php echo CHtml::textField('Eleitor[cep]', '', array('class' => 'form-control', 'id' => 'Eleitor_cep')) ?>
                    <?php } else { ?>
                    <?php echo $form->textField($model, 'cep', array('class' => 'form-control', 'id' => 'Eleitor_cep')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-5 mb-2">
                    <label for="">Endereço</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[endereco]', '', array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_endereco')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'endereco', array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_endereco')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-1 mb-2">
                    <label for="validationTooltip03">Número</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[numero]', '', array('class' => 'form-control', 'id' => 'Eleitor_numero')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'numero', array('class' => 'form-control', 'id' => 'Eleitor_numero')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-4 mb-2">
                    <label for="">Bairro/Conjunto</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[bairroconjunto]', '', array('class' => 'form-control','style' => 'text-transform: uppercase', 'id' => 'Eleitor_bairroconjunto')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'bairroconjunto', array('class' => 'form-control', 'style' => 'text-transform: uppercase','id' => 'Eleitor_bairroconjunto')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-3 mb-2">
                    <label for="">Bairro*</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::dropDownList('Eleitor[bairro]', '', $bairros, array('class' => 'form-control','style' => 'text-transform: uppercase', 'id' => 'Eleitor_bairro', 'empty' => '--selecione--')) ?>
                    <?php } else { ?>
                        <?php echo $form->dropDownList($model, 'bairro', $bairros, array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_bairro', 'empty' => '--selecione--')) ?>
                    <?php } ?>    
                </div>
                <div class="col-md-3 mb-2">
                    <label for="">Zona*</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::dropDownList('Eleitor[zona_moradia]', '', $zona_moradia, array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_zona_moradia', 'empty' => '--selecione--')) ?>
                    <?php } else { ?>
                        <?php echo $form->dropDownList($model, 'zona_moradia', $zona_moradia, array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_zona_moradia', 'empty' => '--selecione--')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-3 mb-2">
                    <label for="">Estado</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[estado]', '', array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_estado')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'estado', array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_estado')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-3 mb-2">
                    <label for="">Município</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[municipio]', '', array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_municipio')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'municipio', array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_municipio')) ?>
                    <?php } ?>
                </div>

            </div>   
            <div class="form-row title-divider">
                DADOS DE CONTATO \ REDES SOCIAIS
            </div> 
            <div class="form-row">
                <div class="col-md-4 mb-2">
                    <label for="">Telefone</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[telefone]', '', array('class' => 'form-control form-control-fone', 'id' => 'Eleitor_telefone')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'telefone', array('class' => 'form-control form-control-fone', 'id' => 'Eleitor_telefone')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-4 mb-2">
                    <label for="">Telefone de Recados</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[telefone_recado]', '', array('class' => 'form-control form-control-fone', 'id' => 'Eleitor_telefone_recado')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'telefone_recado', array('class' => 'form-control form-control-fone', 'id' => 'Eleitor_telefone_recado')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-4 mb-2">
                    <label for="">Celular de Recados</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[cel_recado]', '', array('class' => 'form-control form-control-fone', 'id' => 'Eleitor_cel_recado')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'cel_recado', array('class' => 'form-control form-control-fone', 'id' => 'Eleitor_cel_recado')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-4 mb-2">
                    <label for="">Whatsapp</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[wathsapp]', '', array('class' => 'form-control form-control-whats', 'id' => 'Eleitor_wathsapp')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'wathsapp', array('class' => 'form-control form-control-whats', 'id' => 'Eleitor_wathsapp')) ?>
                        <!-- <br /> 
                        <a href="https://api.whatsapp.com/send?phone=55'. <?php echo $model->wathsapp; ?>.'">
                            <i class="fas fa-search"></i>Entrar em contato</a> -->
                    <?php } ?>
                </div>
                <div class="col-md-4 mb-2">
                    <label for="validationTooltip04">Email</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[email]', '', array('class' => 'form-control form-control-email', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_email', 'placeHolder' => 'exfunclo@exemplo.com')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'email', array('class' => 'form-control form-control-email', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_email')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-4 mb-2">
                    </div>
                <div class="col-md-4 mb-2">
                    <label for="validationTooltip03">Instagram</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[instagram]', '', array('class' => 'form-control form-control-insta', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_instagram')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'instagram', array('class' => 'form-control form-control-insta', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_instagram')) ?>
                    <?php } ?>
                </div> 
                <div class="col-md-4 mb-2">
                    <label for="validationTooltip03">Facebook</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[facebook]', '', array('class' => 'form-control form-control-face', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_facebook')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'facebook', array('class' => 'form-control form-control-face', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_facebook')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-4 mb-2">
                    <label for="validationTooltip03">Twitter</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[twitter]', '', array('class' => 'form-control form-control-tw', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_twitter')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'twitter', array('class' => 'form-control form-control-tw', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_twitter')) ?>
                    <?php } ?>
                </div>
            </div> 
            <div class="form-row title-divider">
                DADOS DE DEPENDENTES
            </div>
            <div class="form-row">
                <div class="col-md-4 mb-2">
                    <label for="">Quantidade de Dependentes</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::dropDownList('Eleitor[qtdeDependetes]', '', $qtdeDependetes, array('class' => 'form-control', 'id' => 'Eleitor_tem_dependetes', 'empty' => '--selecione--')) ?>
                    <?php } else { ?>
                        <?php echo $form->dropDownList($model, 'qtdeDependetes', $qtdeDependetes, array('class' => 'form-control', 'id' => 'Eleitor_tem_dependetes', 'empty' => '--selecione--')) ?>
                    <?php } ?>
                </div>  
            </div>
            <div class="form-row" id="deps1" style="display:none;">
                <div class="col-md-4 mb-2">
                    <label for="validationTooltip03">Nome Dependente 1</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[nome_dep_1]', '', array('class' => 'form-control', 'id' => 'Nome_dep_1')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'nome_dep_1', array('class' => 'form-control', 'id' => 'Nome_dep_1')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-2 mb-2">
                    <label for="">Faixa Etária Dependente 1</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::dropDownList('Eleitor[faixa_dep_1]', '', $faixaEtaria, array('class' => 'form-control', 'id' => 'Faixa_dep_1', 'empty' => '--selecione--')) ?>
                    <?php } else { ?>
                        <?php echo $form->dropDownList($model, 'faixa_dep_1', $faixaEtaria, array('class' => 'form-control', 'id' => 'Faixa_dep_1', 'empty' => '--selecione--')) ?>
                    <?php } ?>
                </div>     
            </div>  
            <div class="form-row" id="deps2" style="display:none;">
                <div class="col-md-4 mb-2">
                    <label for="validationTooltip03">Nome Dependente 2</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[nome_dep_2]', '', array('class' => 'form-control', 'id' => 'Nome_dep_2')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'nome_dep_2', array('class' => 'form-control', 'id' => 'Nome_dep_2')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-2 mb-2">
                    <label for="">Faixa Etária Dependente 2</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::dropDownList('Eleitor[faixa_dep_2]', '', $faixaEtaria, array('class' => 'form-control', 'id' => 'Faixa_dep_2', 'empty' => '--selecione--')) ?>
                    <?php } else { ?>
                        <?php echo $form->dropDownList($model, 'faixa_dep_2', $faixaEtaria, array('class' => 'form-control', 'id' => 'Faixa_dep_2', 'empty' => '--selecione--')) ?>
                    <?php } ?>
                </div>     
            </div> 
            
            <div class="form-row" id="deps3" style="display:none;">
                <div class="col-md-4 mb-2">
                    <label for="validationTooltip03">Nome Dependente 3</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[nome_dep_3]', '', array('class' => 'form-control', 'id' => 'Nome_dep_3')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'nome_dep_3', array('class' => 'form-control', 'id' => 'Nome_dep_3')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-2 mb-2">
                    <label for="">Faixa Etária Dependente 3</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::dropDownList('Eleitor[faixa_dep_3]', '', $faixaEtaria, array('class' => 'form-control', 'id' => 'Faixa_dep_3', 'empty' => '--selecione--')) ?>
                    <?php } else { ?>
                        <?php echo $form->dropDownList($model, 'faixa_dep_3', $faixaEtaria, array('class' => 'form-control', 'id' => 'Faixa_dep_3', 'empty' => '--selecione--')) ?>
                    <?php } ?>
                </div>     
            </div> 

            <div class="form-row" id="deps4" style="display:none;">
                <div class="col-md-4 mb-2">
                    <label for="validationTooltip03">Nome Dependente 4</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[nome_dep_4]', '', array('class' => 'form-control', 'id' => 'Nome_dep_4')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'nome_dep_4', array('class' => 'form-control', 'id' => 'Nome_dep_4')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-2 mb-2">
                    <label for="">Faixa Etária Dependente 4</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::dropDownList('Eleitor[faixa_dep_4]', '', $faixaEtaria, array('class' => 'form-control', 'id' => 'Faixa_dep_4', 'empty' => '--selecione--')) ?>
                    <?php } else { ?>
                        <?php echo $form->dropDownList($model, 'faixa_dep_4', $faixaEtaria, array('class' => 'form-control', 'id' => 'Faixa_dep_4', 'empty' => '--selecione--')) ?>
                    <?php } ?>
                </div>     
            </div> 
            <div class="form-row title-divider">
                DADOS SOCIECONÔMICOS
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-2">
                    <label for="">Núm. Pessoas Família</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[numero_pesssoas_familia]', '', array('class' => 'form-control', 'id' => 'numero_pesssoas_familia')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'numero_pesssoas_familia', array('class' => 'form-control', 'id' => 'numero_pesssoas_familia')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-3 mb-2">
                    <label for="">Profissão</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[profissao]', '', array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_profissao')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'profissao', array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_profissao')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-3 mb-2">
                    <label for="">Tipo de residência</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::dropDownList('Eleitor[tipo_residencia]', '', $tipo_residencia, array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_tipo_residencia', 'empty' => '--selecione--')) ?>
                    <?php } else { ?>
                        <?php echo $form->dropDownList($model, 'tipo_residencia', $tipo_residencia, array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_tipo_residencia', 'empty' => '--selecione--')) ?>
                    <?php } ?>
                </div>  
                <div class="col-md-3 mb-2">
                    <label for="validationTooltip03">Cartão SUS</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[cartao_sus]', '', array('class' => 'form-control', 'id' => 'Eleitor_cartao_sus')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'cartao_sus', array('class' => 'form-control', 'id' => 'Eleitor_cartao_sus')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-3 mb-2">
                    <label for="">Líder Comunitário</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::dropDownList('Eleitor[lider_comunitario]', '', $lider_comunitario, array('class' => 'form-control', 'id' => 'Eleitor_lider_comunitario', 'empty' => '--selecione--')) ?>
                    <?php } else { ?>
                        <?php echo $form->dropDownList($model, 'lider_comunitario', $lider_comunitario, array('class' => 'form-control', 'id' => 'Eleitor_lider_comunitario', 'empty' => '--selecione--')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-3 mb-2">
                    <label for="">Possui veículo</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::dropDownList('Eleitor[possui_veiculo]', '', $possui_veiculo, array('class' => 'form-control', 'id' => 'Eleitor_possui_veiculo', 'empty' => '--selecione--')) ?>
                    <?php } else { ?>
                        <?php echo $form->dropDownList($model, 'possui_veiculo', $possui_veiculo, array('class' => 'form-control', 'id' => 'Eleitor_possui_veiculo', 'empty' => '--selecione--')) ?>
                    <?php } ?>
                </div>        

                <div class="col-md-3 mb-2" id="tipoVeiculo" style="display:none">
                    <label for="">Tipo do veículo</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::dropDownList('Eleitor[tipo_veiculo]', '', $tipo_veiculo, array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_tipo_veiculo', 'empty' => '--selecione--')) ?>
                    <?php } else { ?>
                        <?php echo $form->dropDownList($model, 'tipo_veiculo', $tipo_veiculo, array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_tipo_veiculo', 'empty' => '--selecione--')) ?>
                    <?php } ?>
                </div>  
            </div>



            <div class="form-row">
                
               
                <div class="col-md-2 mb-2" id="qualBeirros" style="display:none">
                    <label for="">Qual bairro?*</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[qualbairro]', '', array('class' => 'form-control','style' => 'text-transform: uppercase', 'id' => 'Qualbairro')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'qualbairro', array('class' => 'form-control','style' => 'text-transform: uppercase', 'id' => 'Qualbairro')) ?>
                    <?php } ?>
                </div>
                
                
                
                
                
            </div>    
            <div class="form-row title-divider">
                DADOS ELEITORAIS
            </div>
            <div class="form-row" >
                <div class="col-md-4 mb-2">
                    <label for="">Título de eleitor</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[titulo_eleitor]', '', array('class' => 'form-control', 'id' => 'Eleitor_titulo_eleitor')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'titulo_eleitor', array('class' => 'form-control', 'id' => 'Eleitor_titulo_eleitor')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-2 mb-2">
                    <label for="">Zona</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[zona]', '', array('class' => 'form-control', 'id' => 'Eleitor_zona')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'zona', array('class' => 'form-control', 'id' => 'Eleitor_zona')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-2 mb-2">
                    <label for="">Seção</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[secao]', '', array('class' => 'form-control', 'id' => 'Eleitor_secao')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'secao', array('class' => 'form-control', 'id' => 'Eleitor_secao')) ?>
                    <?php } ?>
                </div>
               
                <div class="col-md-4 mb-2">
                    <label for="">Local de Votação</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[local_votacao]', '', array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_local_votacao')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'local_votacao', array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_local_votacao')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-4 mb-2">
                    <label for="">Endereço Votação</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[endereco_votacao]', '', array('class' => 'form-control', 'id' => 'Eleitor_endereco_votacao')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'endereco_votacao', array('class' => 'form-control', 'id' => 'Eleitor_endereco_votacao')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-4 mb-2">
                    <label for="">Bairro Votação</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[bairro_votacao]', '', array('class' => 'form-control', 'id' => 'Eleitor_bairro_votacao')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'bairro_votacao', array('class' => 'form-control', 'id' => 'Eleitor_bairro_votacao')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-4 mb-2">
                    <label for="">Município Votação</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[municipio_votacao]', '', array('class' => 'form-control', 'id' => 'Eleitor_municipio_votacao')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'municipio_votacao', array('class' => 'form-control', 'id' => 'Eleitor_municipio_votacao')) ?>
                    <?php } ?>
                </div>
                <div class="col-md-4 mb-2">
                    <label for="validationTooltip03">Domicílio Eleitoral</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[domicilio_eleitoral]', '', array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_domicilio_eleitoral')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'domicilio_eleitoral', array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_domicilio_eleitoral')) ?>
                    <?php } ?>
                </div> 
                <div class="col-md-4 mb-2">
                    <label for="">Situação do título*</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::dropDownList('Eleitor[situacao_titulo]', '', $situacao_titulo, array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_situacao_titulo', 'empty' => '--selecione--')) ?>
                    <?php } else { ?>
                        <?php echo $form->dropDownList($model, 'situacao_titulo', $situacao_titulo, array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_situacao_titulo', 'empty' => '--selecione--')) ?>
                    <?php } ?>
                </div> 
                <div class="col-md-1 mb-2">
                    <label for="">Est. de votos</label>
                    <?php if (Yii::app()->controller->action->id == "novo") { ?>
                        <?php echo CHtml::textField('Eleitor[estimativa_votos]', '', array('class' => 'form-control', 'id' => 'Eleitor_estimativa_votos')) ?>
                    <?php } else { ?>
                        <?php echo $form->textField($model, 'estimativa_votos', array('class' => 'form-control', 'id' => 'Eleitor_estimativa_votos')) ?>
                    <?php } ?>
                </div>  
            </div>
        </div> 
        
        <div class="form-row">
            <div class="col-md-12 mb-2">
                <label for="">Observação</label>
                <?php if (Yii::app()->controller->action->id == "novo") { ?>
                    <?php echo CHtml::textarea('Eleitor[observacao]', '', array('class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_observacao', 'cols' => 240, 'rows' => '4', 'maxlength' => '256')) ?>
                <?php } else { ?>
                    <?php echo $form->textarea($model, 'observacao', array( 'class' => 'form-control', 'style' => 'text-transform: uppercase', 'id' => 'Eleitor_observacao', 'cols' => 240, 'rows' => '4', 'maxlength' => '256')) ?>
                <?php } ?>
            </div>
        </div>  
        <div style="margin-top:20px;">         
            <button type="button" class="btn btn-primary" id="btnSalvarEleitor">Salvar</button>
            <!--<button type="reset" class="btn btn-info">Cancelar</button> -->
            <a href="<?php echo Yii::app()->urlManager->createUrl('eleitores/index'); ?>" class="btn btn-info">
                Cancelar
            </a>

            <?php if (Yii::app()->controller->action->id != "novo") { ?>   
                <a target="__blank" href="<?php echo Yii::app()->createUrl('/eleitores/imprimefichap/',array("id"=>$model->cpf)); ?>" class="btn btn-success" id="btnFicha"><i class="fas fa-file-download"></i> Imprimir</a> 
            <?php } ?>
        </div>
    </div>
  </div>
</div>

<?php $this->endWidget(); ?>