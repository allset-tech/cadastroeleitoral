<?php
Yii::app()->user->setState('titulo', 'Novo Usuário');
$this->renderPartial('form', array(
    'model'         => $model,
    'sexo'          => $sexo,
    'perfil'        => $perfil,
    'status'        => $status,
    'coordenador'   => $coordenador,      
));
?>