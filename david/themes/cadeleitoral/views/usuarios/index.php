<script type="text/javascript">
    $().ready(function () {
        function listaUsuarios(){
            var data = <?php echo $model; ?>;
            $("#total_linhas").html(<?php echo $num_rows; ?>);
            if ($.fn.dataTable.isDataTable('#listaUsuario')) {
                t.clear().draw();
                t = $('#listaUsuario').DataTable();
            } else {
                t = $('#listaUsuario').DataTable({  
                    dom: 'Bfrtip',                  
                    buttons: [
                        'excel'
                    ],
                    responsive: true,
                    "paging": true,
                    "ordering": false,
                    "info": true,
                    "infoFiltered" : true,
                    "bFilter": true,
                    "columnDefs": [
                        { "class": "full-column", "targets": 1 },
                        { "class": "nowrap-column", "targets": 5 }
                        //{ className: "dt-right", "targets": [7,8,9] },
                        //{ className: "dt-nowrap", "targets": [0,1] }
                    ],
            });
            var link = '<?php echo Yii::app()->request->baseUrl . '/' . Yii::app()->controller->id . '/'; ?>'
                for (i = 0; i <= data.length - 1; i++) {
                    buttonView = ''
                    urlView = link + 'visualizar/id/' + data[i]['cpf']
                    buttonView = '<a href="' + urlView + '" title="Visualizar" class="text-center"><i class="fa fa-edit"></i></a>'
                    t.row.add([
                        buttonView,
                        data[i]['nome'],
                        data[i]['perfil'],
                        data[i]['status'],
                        data[i]['coordenador'],
                        data[i]['data_cadastro'],
                    ]).draw(false);
                }
            }
        }
        listaUsuarios();
    });
</script>
<div class="container-fluid">
    <div class="title-bar" style="display:inline-flex;line-height: 45px;">
        <div style="width:100%">
            <i style="font-size: 17px;" class="fas fa-users" style="margin-right: 10px;"></i>
            &nbsp;&nbsp;Relação de Usuários
        </div>
        <div style="margin-right: 30px;white-space: nowrap;">
            <a href="<?php echo Yii::app()->urlManager->createUrl('usuarios/novo'); ?>" class="btn-primary-blue"><i class="fas fa-plus-square"></i> ADICIONAR</a>
        </div>
    </div>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="col-xl-12">
                <?php
                    $this->renderPartial('buscar', array(
                        'model'         => $model,
                        'perfil'        => $perfil,
                        'status'        => $status,
                    ));
                ?>
            </div>
        </div>
        <div class="card-body">
        <table class="table table-responsive table-bordered" id="listaUsuario" style="width:100%">
                <thead>
                    <tr>
                        <th class="text-center"><i class="fa fa-edit"></i></th>
                        <th class="text-center" style="text-align:left !important">NOME</th>
                        <th class="text-center"  style="text-align:left !important">PERFIL</th>
                        <th class="text-center">STATUS</th>
                        <th class="text-center">COORDENADOR</th>
                        <th class="text-center">DATA CADASTRO</th>
                    </tr>
                </thead>
                <tbody></tbody>
                <tfoot>
                    <tr>
                        <th colspan="5" style="text-align:right">Total:</th>
                        <th style="text-align:right"><div id="total_linhas"></div></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="modalResultado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>

            <div class="modal-body">
                <div id="conteudo"></div>                
            </div>
        </div>
    </div>
</div> 