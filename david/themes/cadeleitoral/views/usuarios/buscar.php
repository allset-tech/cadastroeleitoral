<form name="Usuario" id="filtra_usuarios" method="POST">
    <div class="filtr-busca"> 
        Filtro de Busca
</div>
    <div class="form-row">
        <input type="hidden" id="file_name" value="" /> 
        <div class="form-group col-sm-3">
            <label for="nome">Nome</label>
            <input type="nome" class="form-control" id="e_nome" placeholder="" value="">
        </div>
        <div class="form-group col-sm-3">
            <label for="cpf">CPF</label>
            <input type="cpf" class="form-control" id="e_cpf" placeholder="" value="">
        </div>
        <div class="form-group col-sm-3">
            <label for="perfil">Perfil</label>
            <?php echo CHtml::dropDownList('e_perfil', '', $perfil, array('class' => 'form-control', 'id' => 'e_perfil', 'empty' => '--')); ?>
        </div>
        <div class="form-group col-sm-3">
            <label for="status">Status</label>
            <?php echo CHtml::dropDownList('e_status', '', $status, array('class' => 'form-control', 'id' => 'e_status', 'empty' => '--')) ?>
        </div>
    </div>
    <div class="form-row">
        <button type="button" id="buscaLista" class="btn btn-fill btn-danger">Buscar</button>              
        &nbsp;<button type="button" class="btn btn-fill btn-info" id="limparFiltros">Limpar</button>
    </div>    
</form>  

<script type="text/javascript">

      $(document).ready(function() {
            $("#e_cpf").mask("999.999.999-99");
			//reset form
            $("#limparFiltros").click(function(){
              $('#filtra_usuarios')[0].reset();
            });
            function filtraLista(){
              var filtro = '';
              var nome      = $("#e_nome").val();
              var cpf       = $("#e_cpf").val();
              var status    = ($("#e_status").val() != "") ? $("#e_status option:selected").text() : '';
              var perfil    = ($("#e_perfil").val() != "") ? $("#e_perfil option:selected").text() : '';
             
              if(nome.length > 0 && nome != " ")
                 filtro += '&nome='+nome
              if(cpf.length > 0 &&  cpf != " ")
                 filtro += '&cpf='+cpf
              if(status.length > 0 &&  status != " ")
                 filtro += '&status='+status
              if(perfil.length > 0 &&  perfil != " ")
                 filtro += '&perfil='+perfil

              $.ajax({
                'url': '<?php echo Yii::app()->baseUrl; ?>/usuarios/filtrar',
                'data': filtro,
                'dataType': 'json',
                'type': 'post',
                'success': function (data) {
                    if (data.status == "Error") {
                        $.growl.error({message: 'Erro!!! ' + data.detalhes});
                    } else {
                        if (data.status == "Ok") {
                              t.clear().draw();
                             if(data.detalhes.length > 0)
                             {
                                  $("#total_linhas").html(data.total);
                                  var link = '<?php echo Yii::app()->request->baseUrl . '/' . Yii::app()->controller->id . '/'; ?>'
                                for (i = 0; i <= data.detalhes.length - 1; i++) {
                                    buttonView = ''
                                    urlView = link + 'visualizar/id/' + data.detalhes[i]['cpf']
                                    buttonView = '<a href="' + urlView + '" title="Visualizar" class="text-center"><i class="fa fa-edit"></i></a>'
                                    t.row.add([
                                        buttonView,
                                        data.detalhes[i]['nome'],
                                        data.detalhes[i]['perfil'],
                                        data.detalhes[i]['status'],
                                        data.detalhes[i]['coordenador'],
                                        data.detalhes[i]['data_cadastro'],
                                    ]).draw(false);
                                }
                             }
                        }
                    }
                },
                'cache': false, });
            }
            $('#buscaLista').click(function() {
              filtraLista();
            })
      });
</script>