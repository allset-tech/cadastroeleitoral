<?php
Yii::app()->user->setState('titulo', 'Visualizando Usuário');
$this->renderPartial('form', array(
    'model'         => $model,
    'sexo'          => $sexo,
    'perfil'        => $perfil,
    'status'        => $status,
    'coordenador'   => $coordenador,
));
?>