<script type="text/javascript">    
    $(document).ready(function(){
        $("#btnTransferir").click(function(e){
            e.preventDefault();
            let num_eleitores   = $("#num_eleitores").val();
            let lider_origem    = '<?php echo @$model->cpf;?>';
            let lider_destino   = $("#lideres").val();           
            if(num_eleitores <= 0){
                sweetAlert('Oops...', 'Líder não possui cadastros à serem transferidos.', 'error');
                return false;
            }
            if(lider_destino == "") {
                sweetAlert('Oops...', 'Você precisa escolher um líder para onde serão transferidos os cadastros.', 'error');
                return false;
            }
            $.ajax({
                'url': '<?php echo Yii::app()->baseUrl; ?>/usuarios/finalizar',
                'data': 'origem=' + lider_origem +'&destino='+lider_destino,
                'type': 'post',
                'dataType': 'json',
                'success': function(data) {
                    if (data.status == 'Ok') {
                            sweetAlert('Oops...', 'Dados transferidos com sucesso!', 'success');
                            window.setTimeout(function() {
                                    window.location.reload();                
                                }, 2000);
                        } else {
                            sweetAlert('Oops...', 'Erro:'+ data.detalhes, 'error');
                        }
                },
                'cache': false,
            });
          });
    });                            

</script>
<div class="col-xl-12">
<!-- Área de Erros  -->
<?php
$flashMessages = Yii::app()->user->getFlashes();
if ($flashMessages) {
    foreach ($flashMessages as $key => $message) {
        ?>
        <div class="alert alert-<?php echo ($key == 'error') ? 'danger' : $key; ?>">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <i class="fa fa-warning"></i>
            <h4><?php echo $message; ?></h4>
            <?php echo $form->errorSummary($model, null, null, null); ?>
        </div>
        <?php
    }
}
?>
</div>
<div class="container-fluid">
<div class="title-bar">
                        Transfencia de Usuário
                    </div>  
    <div class="card-body">     
        <form>
            <div class="col-md-12">
                <div class="form-row">
                    <div class="col-md-2 mb-3">
                        <label for="">Número de eleitores do líder</label>
                        <input type="text" class="form-control" id="num_eleitores" value="<?php echo @$num_eleitores;?>" disabled="disabled" />
                    </div>
                    <div class="col-md-2 mb-3">
                        <label for="">Transferir p/ o líder</label>
                        <?php echo CHtml::dropDownList('lideres', '', $lideres_coordenador, array('class' => 'form-control', 'id' => 'lideres', 'empty' => '--escolha--')) ?>
                    </div>
                </div>
                <div class="form-row">
                <a href="<?php echo Yii::app()->urlManager->createUrl('usuarios/index'); ?>" 
                    class="btn btn-info">
                    Cancelar
                </a>
                    <button class="btn btn-success" id="btnTransferir" style="margin-left:5px">Finalizar</button>
</div>
            </div>    
        </form>
    </div>
 </div>
</div>