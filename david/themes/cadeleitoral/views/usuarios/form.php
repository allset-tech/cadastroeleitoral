<?php
    if (Yii::app()->controller->action->id == "novo") {
        $formAction = Yii::app()->createUrl('usuarios/novo', array());
    } else {
        $formAction = Yii::app()->createUrl('usuarios/salvar/id/'.$model->cpf, array());
    }
    $form = $this->beginWidget("CActiveForm", array(
        'id'          => 'Usuario-form',
        'htmlOptions' => array('enctype' => 'multipart/form-data', 'class' => 'form-inline'),
        'action'      => $formAction,
        ));
?>

<div class="col-xl-12">
<!-- Área de Erros  -->
<?php
$flashMessages = Yii::app()->user->getFlashes();
if ($flashMessages) {
    foreach ($flashMessages as $key => $message) {
        ?>
        <div class="alert alert-<?php echo ($key == 'error') ? 'danger' : $key; ?>">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <i class="fa fa-warning"></i>
            <h4><?php echo $message; ?></h4>
            <?php echo $form->errorSummary($model, null, null, null); ?>
        </div>
        <?php
    }
}
?>
</div>
<div class="container-fluid">
    <div class="card-body">
        <div class="form-row">
            <div class="col-md-12">
                <div class="form-row">
                    <div class="title-bar"  style="margin-bottom:10px">
                        Usuário
                    </div>
                    <div class="col-md-4 mb-4">
                        <label for="">Perfil*</label>
                        <input type="hidden" id="input_admin" value="<?php echo Yii::app()->user->perfil; ?>" />
                        <?php if (Yii::app()->controller->action->id == "novo") { ?>
                            <?php echo CHtml::dropDownList('Usuario[perfil]', '', $perfil, array('class' => 'form-control', 'id' => 'Usuario_perfil', 'empty' => '--selecione o perfil--')) ?>
                        <?php } else { ?>
                            <?php echo $form->dropDownList($model, 'perfil', $perfil, array('class' => 'form-control', 'id' => 'Usuario_perfil', 'empty' => '--selecione o perfil--')) ?>
                        <?php } ?>
                    </div>
                    <div class="col-md-3 mb-3" id="area_admin" style="display: none;">
                        <label for="validationTooltip03">Coordenador</label>
                        <?php echo CHtml::dropDownList('Usuario[coordenador]', '', $coordenador, array('class' => 'form-control', 'id' => 'Usuario_coordenador', 'empty' => '--selecione--')) ?>
                    </div>
                    <div class="col-md-4 mb-4">
                        <label for="">Nome*</label>
                        <?php if (Yii::app()->controller->action->id == "novo") { ?>
                            <?php echo CHtml::textField('Usuario[nome]', '', array('class' => 'form-control', 'id' => 'Usuario_nome')) ?>
                        <?php } else { ?>
                            <?php echo $form->textField($model, 'nome', array('class' => 'form-control', 'id' => 'Usuario_nome')) ?>
                        <?php } ?>
                    </div>
                    <div class="col-md-2 mb-3">
                        <label for="">Data Nasc.*</label>
                        <?php if (Yii::app()->controller->action->id == "novo") { ?>
                            <?php
                                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                    'name' => 'Usuario[data_nasc]',
                                    'model' => $model,
                                    'attribute' => 'data_nasc',
                                    'value' => '',
                                    'language' => 'pt',
                                    'options' => array(
                                        'dateFormat' => 'dd/mm/yy',
                                    ),
                                    'htmlOptions' => array(
                                        'class' => 'form-control',
                                        'id' => 'Usuario_data_nasc',
                                        'autocomplete' => 'on',
                                    )
                                ));
                                ?>
                        <?php } else { ?>
                            <?php echo $form->textField($model, 'data_nasc', array('class' => 'form-control', 'id' => 'Usuario_data_nasc')) ?>
                        <?php } ?>
                    </div>
                    <div class="col-md-2 mb-3">
                        <label for="">Sexo*</label>
                        <?php if (Yii::app()->controller->action->id == "novo") { ?>
                            <?php echo CHtml::dropDownList('Usuario[sexo]', '', $sexo, array('class' => 'form-control', 'id' => 'Usuario_sexo', 'empty' => '--selecione o sexo--')) ?>
                        <?php } else { ?>
                            <?php echo $form->dropDownList($model, 'sexo', $sexo, array('class' => 'form-control', 'id' => 'Usuario_sexo', 'empty' => '--selecione o sexo--')) ?>
                        <?php } ?>
                    </div>
                    <div class="col-md-2 mb-3">
                        <label for="">Telefone*</label>
                        <?php if (Yii::app()->controller->action->id == "novo") { ?>
                            <?php echo CHtml::textField('Usuario[telefone]', '', array('class' => 'form-control', 'id' => 'Usuario_telefone')) ?>
                        <?php } else { ?>
                            <?php echo $form->textField($model, 'telefone', array('class' => 'form-control', 'id' => 'Usuario_telefone')) ?>
                        <?php } ?>
                    </div>
                    <div class="col-md-2 mb-3">
                        <label for="">CEP*</label>
                        <?php if (Yii::app()->controller->action->id == "novo") { ?>
                            <?php echo CHtml::textField('Usuario[cep]', '', array('class' => 'form-control', 'id' => 'Usuario_cep')) ?>
                        <?php } else { ?>
                            <?php echo $form->textField($model, 'cep', array('class' => 'form-control', 'id' => 'Usuario_cep')) ?>
                        <?php } ?>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="">Endereço*</label>
                        <?php if (Yii::app()->controller->action->id == "novo") { ?>
                            <?php echo CHtml::textField('Usuario[endereco]', '', array('class' => 'form-control', 'id' => 'Usuario_endereco')) ?>
                        <?php } else { ?>
                            <?php echo $form->textField($model, 'endereco', array('class' => 'form-control', 'id' => 'Usuario_endereco')) ?>
                        <?php } ?>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="">Estado*</label>
                        <?php if (Yii::app()->controller->action->id == "novo") { ?>
                            <?php echo CHtml::textField('Usuario[estado]', '', array('class' => 'form-control', 'id' => 'Usuario_estado')) ?>
                        <?php } else { ?>
                            <?php echo $form->textField($model, 'estado', array('class' => 'form-control', 'id' => 'Usuario_estado')) ?>
                        <?php } ?>
                    </div>
                    <div class="col-md-4 mb-4">
                        <label for="">Município*</label>
                        <?php if (Yii::app()->controller->action->id == "novo") { ?>
                            <?php echo CHtml::textField('Usuario[municipio]', '', array('class' => 'form-control', 'id' => 'Usuario_municipio')) ?>
                        <?php } else { ?>
                            <?php echo $form->textField($model, 'municipio', array('class' => 'form-control', 'id' => 'Usuario_municipio')) ?>
                        <?php } ?>
                    </div>
                    <div class="col-md-4 mb-4">
                        <label for="">Bairro*</label>
                        <?php if (Yii::app()->controller->action->id == "novo") { ?>
                            <?php echo CHtml::textField('Usuario[bairro]', '', array('class' => 'form-control', 'id' => 'Usuario_bairro')) ?>
                        <?php } else { ?>
                            <?php echo $form->textField($model, 'bairro', array('class' => 'form-control', 'id' => 'Usuario_bairro')) ?>
                        <?php } ?>
                    </div>
                    <div class="col-md-4 mb-4">
                        <label for="validationTooltip03">Número*</label>
                        <?php if (Yii::app()->controller->action->id == "novo") { ?>
                            <?php echo CHtml::textField('Usuario[numero]', '', array('class' => 'form-control', 'id' => 'Usuario_numero')) ?>
                        <?php } else { ?>
                            <?php echo $form->textField($model, 'numero', array('class' => 'form-control', 'id' => 'Usuario_numero')) ?>
                        <?php } ?>
                    </div> 
                </div>  
                <div class="form-row">
                    <div class="title-bar" style="margin-bottom:10px">
                        Dados do Login
                    </div>                      
                    <div class="col-md-3 mb-3">
                        <label for="validationTooltip01">CPF*</label>
                        <?php if (Yii::app()->controller->action->id == "novo") { ?>
                            <?php echo CHtml::textField('Usuario[cpf]', '', array('class' => 'form-control', 'id' => 'Usuario_cpf')) ?>
                        <?php } else { ?>
                            <?php echo $form->textField($model, 'cpf', array('class' => 'form-control', 'id' => 'Usuario_cpf')) ?>
                        <?php } ?>
                    </div>
                    <div class="col-md-4 mb-4">
                        <label for="validationTooltip04">Email/Login*</label>
                        <?php if (Yii::app()->controller->action->id == "novo") { ?>
                            <?php echo CHtml::textField('Usuario[email]', '', array('class' => 'form-control', 'id' => 'Usuario_email', 'placeHolder' => 'exfunclo@exemplo.com')) ?>
                        <?php } else { ?>
                            <?php echo $form->textField($model, 'email', array('class' => 'form-control', 'id' => 'Usuario_email')) ?>
                        <?php } ?>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="">Senha*</label>
                        <?php if (Yii::app()->controller->action->id == "novo") { ?>
                            <?php echo CHtml::passwordField('Usuario[senha]', '', array('class' => 'form-control', 'id' => 'Usuario_senha')) ?>
                        <?php } else { ?>
                            <?php echo CHtml::passwordField('Usuario[senha]', $model->senha, array('class' => 'form-control', 'id' => 'Usuario_senha')) ?>
                        <?php } ?>
                        <span id = "message" style="color:red"> </span>
                    </div>
                    <?php if (Yii::app()->controller->action->id != "novo"): ?>        
                    <div class="col-md-2 mb-3">
                        <label for="">Status*</label>
                        <?php if (Yii::app()->controller->action->id == "novo") { ?>
                            <?php echo CHtml::dropDownList('Usuario[status]', '', $status, array('class' => 'form-control', 'id' => 'Usuario_status', 'empty' => '--selecione o status--')) ?>
                        <?php } else { ?>
                            <?php echo $form->dropDownList($model, 'status', $status, array('class' => 'form-control', 'id' => 'Usuario_status', 'empty' => '--selecione o status--')) ?>
                        <?php } ?>
                    </div>
                    <?php endif; ?>                     
                </div> 
            </div>
     </div>          
            <button type="button" class="btn btn-primary" id="btnSalvarUsuario">Salvar</button>
            <!--<button type="reset" class="btn btn-info">Cancelar</button> -->
            <a href="<?php echo Yii::app()->urlManager->createUrl('usuarios/index'); ?>" class="btn btn-info">
                Cancelar
            </a>
            <?php if(Yii::app()->user->perfil == "C" || Yii::app()->user->perfil == "A"):?>
                <?php if($model->perfil == 'L'):?> 
                    <a href="<?php echo Yii::app()->urlManager->createUrl('usuarios/transferir/id/'.base64_encode($model->cpf)); ?>" class="btn btn-danger">
                    <i class="fas fa-exchange-alt"></i> Transferir eleitores
                    </a>
                <?php endif;?>
            <?php endif;?>    
    </div>
  </div>
</div>

<?php $this->endWidget(); ?>
<script type="text/javascript">    
    $(document).ready(function(){
        $("#Usuario_perfil").change(function(){
            var perfil = $("#input_admin").val();
            if(perfil == "A" && $(this).val() == "L"){
                $("#area_admin").show();
            } else {
                $("#area_admin").hide();
            }
        });
        $("#Usuario_senha").change(function(){
            $("#message").html('');
            var numeros    = ['0','1','2','3','4','5','6','7','8','9'];
            var alfabetos  = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
            let senha      = $(this).val().toUpperCase();
            if(senha.length < 5){
                $("#message").html('A senha deve conter no mínimo 5 digitos.');
                return false;
            }
            let check = false;
            //verificando numeros
            for (const item of senha){
                if(numeros.indexOf(item) != -1){
                    check = true;
                    break;
                }
            }
           if(!check){
                $("#message").html('a senha deve conter no mínimo 1 número.');
                return false;
           }
           check = false;
           //verificando alfabetos
           for (const item of senha){
                if(alfabetos.indexOf(item) != -1){
                    check = true;
                    break;
                }
            }

            if(!check){
                $("#message").html('a senha deve conter no mínimo 1 letra.');
                return false;
           }
           $("#btnSalvarUsuario").removeAttr('disabled');
        });    
    });                            
</script>