
jQuery(document).ready(function() {
      $.extend(true, $.fn.dataTable.defaults, {
        "language": {
            "lengthMenu": "Veja _MENU_",
            "zeroRecords": "Nenhume item encontrado - desculpe",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "nenhum item",
            "infoFiltered": "(encontrado(s) de _MAX_ total de registros)",
            "search": "Pesquisar:",
            "zeroRecords": "nenhum registro foi encontrado",
            "paginate": {
                "first": "Primeiro",
                "last": "Último",
                "next": "Próximo",
                "previous": "Anterior"
            },
        }

    });

});

function atualiza() {
    location.reload();
}


