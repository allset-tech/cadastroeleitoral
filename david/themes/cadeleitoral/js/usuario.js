


$(document).ready(function(){
   

////////////formulario////////////////////////////

$("#Usuario_cpf").mask('999.999.999-99');
$("#Usuario_data_nasc").mask('99/99/9999');
$("#Usuario_telefone").mask('(99)99999-9999');
$("#Usuario_cep").mask('99999-999');


//////////Validacoes /////////////////////////////

function validaFormulario(){

    let cpf = $("#Usuario_cpf").val();

    if(cpf.length < 14){
        sweetAlert('Oops...', 'CPF inválido!!', 'error');
        $("#Usuario_cpf").focus();
        return false;
    }

    return true;
    
}

function ValidateEmail(mail) 
{
 if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
  {
    return (true)
  }

    return (false)
}


$("#Usuario_email").change(function(){
    if(!ValidateEmail($(this).val()))
        sweetAlert('Oops...', 'Endereço de email parece inválido!', 'error');
});



/// Busca dados do CEP ////////////////////////    


$("#Usuario_cep").change(function(){
    pesquisacep($(this).val());
});


function limpa_formulário_cep() {
    //Limpa valores do formulário de cep.
        $("#Usuario_endereco").val('');
        $("#Usuario_estado").val('');
        $("#Usuario_municipio").val('');
        $("#Usuario_bairro").val('');
}

    
function pesquisacep(valor) {
    //Nova variável "cep" somente com dígitos.
    var cep = valor.replace(/\D/g, '');

    //Verifica se campo cep possui valor informado.
    if (cep != "") {

        //Expressão regular para validar o CEP.
        var validacep = /^[0-9]{8}$/;

        //Valida o formato do CEP.
        if(validacep.test(cep)) {

           // limpa_formulário_cep();
            //Preenche os campos com "..." enquanto consulta webservice.
            $("#Usuario_endereco").val('...');
            $("#Usuario_estado").val('...');
            $("#Usuario_municipio").val('...');
            $("#Usuario_bairro").val('...');

            $.ajax({
                url : 'https://viacep.com.br/ws/'+ cep + '/json/?callback',
                success: function(data){
                    //console.log(data);
                    $("#Usuario_endereco").val(data.logradouro);
                    $("#Usuario_estado").val(data.uf);
                    $("#Usuario_municipio").val(data.localidade);
                    $("#Usuario_bairro").val(data.bairro);
                }
            });

        } //end if.
        else {
            //cep é inválido.
            limpa_formulário_cep();
            sweetAlert('Oops...', 'Formato de CEP inválido.', 'error');
        }
    } //end if.
    else {
        //cep sem valor, limpa formulário.
        limpa_formulário_cep();
    }
};


//enviar formulario
$("#btnSalvarUsuario").click(function(){
    if(validaFormulario() === true)
        $("#Usuario-form").submit();

});



});
