function alertaValidacao(titulo, mensagem, tipo) {

    tipo = (tipo == undefined) ? "erro" : tipo;
    var alerta = jQuery('#mensagemAlertaAjax');
    
    // Inicializa Alerta
    jQuery(alerta).show();
    jQuery(alerta).removeClass();    
    jQuery(alerta).find('h4').html(titulo);
    
    switch(tipo) {
	case "erro":
	    jQuery(alerta).addClass('alert alert-error');
	    jQuery(alerta).find('h4').prepend('<span class="icon-warning"></span>');
	    break;
	case "sucesso":
	    jQuery(alerta).addClass('alert alert-success');
	    jQuery(alerta).find('h4').prepend('<span class="icon-checkmark"></span>');
	    setTimeout(function(){ jQuery(alerta).fadeOut(); }, 10000);
	    break;
    }
    
    jQuery(alerta).find('p').html(mensagem);
    jQuery("#areaLogin").css({'margin-left':'-275px'})
}

jQuery(function() {
    $("#EsqueceuSenha_usuario_cpf").mask("999.999.999-99");
    $("#divEsqueceuSenha").hide();
    
    $("#btn_esqueceu_senha").click(function() {
        //$("#EsqueceuSenha_usuario_login").val('').focus()
        $("#EsqueceuSenha_usuario_cpf").val('').focus()        
        $("#areaLogin").css({'margin-left':'-375px'})
        $("#divEsqueceuSenha").css({'box-sizing':'content-box'})
        $("#divEsqueceuSenha").css({'width':'208px'})
        $("#divEsqueceuSenha").show( 'slide', {}, 300 )
    });
    $(".close").click(function() {
      $("#areaLogin").css({'margin-left':'-275px'})      
    });    
});